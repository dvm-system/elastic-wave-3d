This repository presents results of mapping of 3D elastic waves simulation method on high-performance heterogeneous clusters. A distinctive feature of the proposed method is the use of a curved three-dimensional grid, which is consistent with the geometry of free surface. Usage of curved grids considerably complicates both manual and automated parallelization. Technique to map curved grid on a structured grid has been presented to solve this problem. The sequential program based on the finite difference method on a structured grid, has been parallelized using Fortran-DVMH programming language.

The repository has the following structure:

* `SEQ_VER` contains sequential implementation of proposed algorithm,

* `MPI_VER` contains a parallel implementation of the algorithm based on MPI parallel programming technology,

* `DVMH_VER` contains parallel algorithm implemented in Fortran-DVMH programming language.

Note that DVMH version of a program can be run as program in MPI, MPI/OpenMP or MPI/OpenMP/CUDA models according to configuration of Fortran-DVMH program compilation process and hardware capabilities of available HPC system.
To run Fortran-DVMH program it is necessary to install [DVM System](http://dvm-system.org/) on personal computer. It is also possilbe to use clusters where the system has been already installed (the list of available clusters can be found [here (en)](http://dvm-system.org/en/use/) or [here (ru)](http://dvm-system.org/ru/use/)).
