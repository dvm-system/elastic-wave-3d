
      implicit double precision(A-H,R-Z)
      implicit character(O)
       
      
      include "mpif.h" ! MPI
    
    
      double precision, allocatable::                  
     &                                 X(:,:,:),Y(:,:,:),Z(:,:,:),
     &                     a1_10(:), a1_20(:), a1_11(:), a1_21(:),
     &                     a2_10(:), a2_20(:), a2_11(:), a2_21(:),
     &                     a3_10(:), a3_20(:), a3_11(:), a3_21(:),  
     &                        surf_Z(:,:),
     &                     surf2_X(:,:),surf2_Y(:,:),surf2_Z(:,:),
     &      dX_dq3_surf2(:,:),dY_dq3_surf2(:,:),dZ_dq3_surf2(:,:),
     & Y_x_0(:,:),Z_x_0(:,:),Y_x_aW(:,:),Z_x_aW(:,:),
     & X_y_0(:,:),Z_y_0(:,:),X_y_aL(:,:),Z_y_aL(:,:),
    
     & F1x_y_0(:,:),F1x_y_aL(:,:),
     & F1y_y_0(:,:),F1y_y_aL(:,:),
     & F1z_y_0(:,:),F1z_y_aL(:,:),
    
     & F2x_z_surf2(:,:),F2x_z_aH(:,:),
     & F2y_z_surf2(:,:),F2y_z_aH(:,:),
     & F2z_z_surf2(:,:),F2z_z_aH(:,:), 

     & X_y_0_z_surf2(:),X_y_aL_z_surf2(:),
     & Y_x_0_z_surf2(:),Y_x_aW_z_surf2(:),
     & Z_x_0_z_surf2(:),Z_x_aW_z_surf2(:),
     & Z_y_0_z_surf2(:),Z_y_aL_z_surf2(:),
     &                                     U(:,:,:),V(:,:,:),W(:,:,:),
     &                                  UU(:,:,:),VV(:,:,:),WW(:,:,:),
     &                                      alambda(:,:,:),amu(:,:,:),
     &                                      ttoRhoJ(:,:,:),Rho(:,:,:),
     &                                                           F(:),
     &                      dq1_dx(:,:,:),dq2_dx(:,:,:),dq3_dx(:,:,:),
     &                      dq1_dy(:,:,:),dq2_dy(:,:,:),dq3_dy(:,:,:),
     &                      dq1_dz(:,:,:),dq2_dz(:,:,:),dq3_dz(:,:,:),
     &                                        A1(:,:),A2(:,:),A3(:,:),
     &                                        B1(:,:),B2(:,:),B3(:,:),
     &                                        C1(:,:),C2(:,:),C3(:,:),
     &    R1_1(:,:),R1_2(:,:),R1_3(:,:),R1_4(:,:),R1_5(:,:),R1_6(:,:),
     &    R2_1(:,:),R2_2(:,:),R2_3(:,:),R2_4(:,:),R2_5(:,:),R2_6(:,:),
     &    R3_1(:,:),R3_2(:,:),R3_3(:,:),R3_4(:,:),R3_5(:,:),R3_6(:,:),
     &                              dz_dq1_surf(:,:),dz_dq2_surf(:,:),
     &                                                      amaxes(:),
     &                A_N_down_x(:),A_N_up_x(:),A_down_x(:),A_up_x(:),
     &                A_N_down_y(:),A_N_up_y(:),A_down_y(:),A_up_y(:),
     &                A_N_down_z(:),A_N_up_z(:),A_down_z(:),A_up_z(:),
     & U_down_x(:),U_up_x(:),U_down_y(:),U_up_y(:),
     & U_N_down_x(:),U_N_up_x(:),U_N_down_y(:),U_N_up_y(:), 
     & V_down_x(:),V_up_x(:),V_down_y(:),V_up_y(:),
     & V_N_down_x(:),V_N_up_x(:),V_N_down_y(:),V_N_up_y(:),
     & W_down_x(:),W_up_x(:),W_down_y(:),W_up_y(:),
     & W_N_down_x(:),W_N_up_x(:),W_N_down_y(:),W_N_up_y(:),

     &                                        tr_x(:),tr_y(:),tr_z(:),
     &                         trace_U(:,:),trace_V(:,:),trace_W(:,:)

      integer, allocatable::         i_tr(:),j_tr(:),k_tr(:),id_tr(:),
     & n_request(:),n_statuses(:,:)



      integer i, j, k, M, L, N, mult, N_rest, N_wide
      double precision pi, speed, d, aW, aL, aH, a_length, a_width, a_height,
     & coef, a_nx, a_ny, a_nz, rd, aH_level,
     & dZ_dq1, dZ_dq2, sqr,
     & F1x,F1y,F1z,
     & dF1x_dq2,dF1y_dq2,dF1z_dq2,
     & F2x,F2y,F2z,
     & dF1x_dq3,dF1y_dq3,dF1z_dq3,
     & dF1x_dq2dq3,dF1y_dq2dq3,dF1z_dq2dq3,
     & dF2x_dq3,dF2y_dq3,dF2z_dq3

      !-------------------------MPI parameters--------------------------- 
      
      integer, parameter :: ndim = 3
      integer N_bot_x,N_bot_y,N_bot_z
      integer N_top_x,N_top_y,N_top_z
      integer n_cartesian_comm
      integer n_cartesian_id
      integer, parameter :: n_cartesian_master = 0
      logical q_converged
      integer n_dim
      integer n_dims(ndim),n_newcoord(ndim)
      integer n_error
      integer it
      integer, parameter :: n_master = 0
      integer num_procs
      logical periodic(ndim)
      logical q_reorder
      integer n_shift
      integer n_world_id
      integer id, id_x, id_y, id_z, id_coords(ndim)
      integer n_status(MPI_STATUS_SIZE)
      integer N_down_x,N_down_y,N_down_z
      integer N_up_x,N_up_y,N_up_z
      
      
      external MPI_Comm_rank,MPI_Comm_size,MPI_Init,
     &MPI_Finalize,MPI_BARRIER,MPI_Cart_create,
     &MPI_Cart_shift,MPI_Cart_coords,MPI_Request

    !  integer n_request(1:132*num_procs)
      
      character(len=50)::o_filename1,o_filename2
      character(len=50)::o_numb_str
      character(len=50)::o_step 
      character(len=50)::o_i_tr     
      character(len=50)::o_j_tr
      character(len=50)::o_k_tr
      character(len=50)::o_id_x
      character(len=50)::o_id_y
      character(len=50)::o_id_z
      character(len=50)::o_l1
!------------------------------MPI---------------------------------	
      
!       Initialize MPI
       call MPI_Init(n_error)
!  Get the number of processes
       call MPI_Comm_size(MPI_COMM_WORLD,num_procs,n_error)
  
   
       allocate (n_request(1:12),
     &           n_statuses(MPI_STATUS_SIZE,1:12))

       n_request=MPI_REQUEST_NULL


!  Get the individual process ID.
       call MPI_Comm_rank(MPI_COMM_WORLD,n_world_id,n_error)

       if ( n_world_id==n_master ) then
       write(*,'(a,i3)') 'The number of MPI processes =',num_procs
       print *, ''
       print *, ''
       end if

!  Get a communicator for the Cartesian decomposition of the domain.

       open(unit=31,file='!_Decomposition.txt',form='formatted') 
       read(31,*) n_dims(1)
       read(31,*) n_dims(2)
       read(31,*) n_dims(3)
       close(31)

       periodic(1)=.false.
       periodic(2)=.false.
       periodic(3)=.false.
       q_reorder=.true.

      call MPI_Cart_create(MPI_COMM_WORLD,ndim,n_dims,periodic,
     &q_reorder,n_cartesian_comm,n_error)  
     
!  Get this processor's rank within the Cartesian communicator.

      call MPI_Comm_rank(n_cartesian_comm,n_cartesian_id,n_error)
      id=n_cartesian_id

!  Get from rank the cartesian coordinates of id

      call MPI_Cart_coords(n_cartesian_comm,n_cartesian_id,ndim,
     &id_coords,n_error)

      id_x=id_coords(1)
      id_y=id_coords(2)
      id_z=id_coords(3)


      call MPI_BARRIER(n_cartesian_comm,n_error) 
      
!  Note that MPI_Cart_shift counts dimensions using 0-based arithmetic!
!  We only have one dimension, but to inquire about a shift in dimension 1
!  we actually ask about dimension 0!

      n_dim = 0 ! Dimension directions are numbered starting with 0, so for 1D case we shifting in direction 0
      n_shift = 1 ! Displacement to +1, meaning from present process left one is bot, right one is top (-+shift)

      call MPI_Cart_shift(n_cartesian_comm,n_dim,n_shift,
     &N_bot_x,N_top_x,n_error)

   
      n_dim = 1 ! Dimension directions are numbered starting with 0, so for 1D case we shifting in direction 0
      n_shift = 1 ! Displacement to +1, meaning from present process left one is bot, right one is top (-+shift)

      call MPI_Cart_shift(n_cartesian_comm,n_dim,n_shift,
     &N_bot_y,N_top_y,n_error)
      
      
      n_dim = 2 ! Dimension directions are numbered starting with 0, so for 1D case we shifting in direction 0
      n_shift = 1 ! Displacement to +1, meaning from present process left one is bot, right one is top (-+shift)

      call MPI_Cart_shift(n_cartesian_comm,n_dim,n_shift,
     &N_bot_z,N_top_z,n_error)

      if(id_x==0) then
      N_bot_x=MPI_PROC_NULL
      end if
      if(id_x==n_dims(1)-1) then
      N_top_x=MPI_PROC_NULL
      end if
      if(id_y==0) then
      N_bot_y=MPI_PROC_NULL
      end if
      if(id_y==n_dims(2)-1) then
      N_top_y=MPI_PROC_NULL
      end if
      if(id_z==0) then
      N_bot_z=MPI_PROC_NULL
      end if
      if(id_z==n_dims(3)-1) then
      N_top_z=MPI_PROC_NULL
      end if

!      do i=0,num_procs-1
!      if(id==i) then
!      write(*,'(a,i3)') 'id= ',id
!      print *,''
!      write(*,'(a,i3)') 'id_x= ',id_x
!      write(*,'(a,i3)') 'id_y= ',id_y
!      write(*,'(a,i3)') 'id_z= ',id_z
!      print *,''
!      write(*,'(a,i3)') 'N_bot_x= ',N_bot_x
!      write(*,'(a,i3)') 'N_bot_y= ',N_bot_y
!      write(*,'(a,i3)') 'N_bot_z= ',N_bot_z
!      print *,''
!      write(*,'(a,i3)') 'N_top_x= ',N_top_x
!      write(*,'(a,i3)') 'N_top_y= ',N_top_y
!      write(*,'(a,i3)') 'N_top_z= ',N_top_z
!      print *, ''
!      print *, ''
!      end if
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      end do

      call MPI_BARRIER(n_cartesian_comm,n_error) 
      
!=============================================================================================== 
       open(unit=31,file='!_Speed.txt',form='formatted') 
       read(31,*) s
       close(31)

      speed=dble(s) ! Vp

      open(unit=31,file='!_d_coef.txt',form='formatted') 
      read(31,*) d_coef
      close(31)
      
      d=speed/dble(d_coef) ! Discrete step
      
      open(unit=31,file='!_Medium_size.txt',form='formatted') 
      read(31,*) a_w
      read(31,*) a_l
      read(31,*) a_h
      close(31)      
      
      a_width=dble(a_w)  ! width of area
      a_length=dble(a_l) ! length of area
      a_height=dble(a_h)  ! height of area

!====================== Time parameters ======================

      open(unit=31,file='!_dt_coef.txt',form='formatted') 
      read(31,*) dt_coef
      close(31)
     
      dt=dble(dt_coef)*d ! time discrete step

      t=2.0d0 ! time of source work

      
      open(unit=31,file='!_Explicite_time.txt',form='formatted') 
      read(31,*) explicite_time
      close(31)

      expl_time=dble(explicite_time) ! time of wave moving after source ended it's work

      iTT=floor(t/dt)+1
  !    iTime=floor((t+expl_time)/dt)+1
      iTime=1000
      open(unit=31,file='!_Snapshot_frequency.txt',form='formatted') 
      read(31,*) i_snap
      close(31)


      if(id==0) then
      write(*,'(a,f7.4)') 'dt= ',dt
      write(*,'(a,i6)') 'iTT= ',iTT
      write(*,'(a,i6)') 'iTime= ',iTime
      print *,''
      end if      

      call MPI_BARRIER(n_cartesian_comm,n_error)
!=============================================================
!====================== Source position ======================
      
      open(unit=31,file='!_Source.txt',form='formatted') 
      read(31,*) s_x
      read(31,*) s_y
      read(31,*) s_z
      close(31)

      x0=dble(s_x)
      y0=dble(s_y)
      z0=dble(s_z)
!=============================================================

      aH_level=-1.0d0

      M=ceiling(a_width/d)  ! Number of dots at X axis
      L=ceiling(a_length/d) ! Number of dots at Y axis 
      N=ceiling(a_height/d)  ! Number of dots at Z axis
      
      a_width=d*(M)
      a_length=d*(L)
      a_height=d*(N)
      
      aW=a_width
      aL=a_length
      aH=a_height

      pi=dacos(-1.0d0)

      rdd=1.0d0/(d*d)
      rd=1.0d0/d


      if(id==0) then
      write(*,'(a,f6.3,a,i6)') 'width(X)=  ',a_width,'  M=',M
      write(*,'(a,f6.3,a,i6)') 'length(Y)= ',a_length,'  L=',L
      write(*,'(a,f6.3,a,i6)') 'height(Z)= ',a_height,'  N=',N
      print *,''
      write(*,'(a,f7.4)') 'd= ',d
      print *,''
      end if    
 
      call MPI_BARRIER(n_cartesian_comm,n_error)


!====================== Traces ===============================
      num_tr=10
!=============================================================

!====================== domain decomposition =================      
      call domain_decomposition(n_dims(1),M,id_x,N_down_x,N_up_x) 
      call domain_decomposition(n_dims(2),L,id_y,N_down_y,N_up_y)
      call domain_decomposition(n_dims(3),N,id_z,N_down_z,N_up_z)  

      
      if(N_down_x==1) then
      N_down_x=2
      end if
      if(N_up_x==M) then
      N_up_x=M-1
      end if
      if(N_down_y==1) then
      N_down_y=2
      end if
      if(N_up_y==L) then
      N_up_y=L-1
      end if
      if(N_up_z==N) then
      N_up_z=N-1
      end if

!      do i=0,num_procs-1
!      if(id==i) then
!      print *, 'id=',id
!      print *, 'coords=',id_x,id_y,id_z
!      print *, 'down=  ',N_down_x,N_down_y,N_down_z
!      print *, 'up=    ',N_up_x,N_up_y,N_up_z
!      print *, ''   
!      end if
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      end do
 
      if(id==0) then
      print *,''
      print *,''
      end if

      call MPI_BARRIER(n_cartesian_comm,n_error)      
!=============================================================
















      
!=========================================================================================================================================================================
!======================================================================================GRID===============================================================================
!=========================================================================================================================================================================      
      allocate (
     & X(2*N_down_x-3:2*N_up_x+3,
     &   2*N_down_y-3:2*N_up_y+3,
     &   2*N_down_z-3:2*N_up_z+3),

     & Y(2*N_down_x-3:2*N_up_x+3,
     &   2*N_down_y-3:2*N_up_y+3,
     &   2*N_down_z-3:2*N_up_z+3),

     & Z(2*N_down_x-3:2*N_up_x+3,
     &   2*N_down_y-3:2*N_up_y+3,
     &   2*N_down_z-3:2*N_up_z+3),

     &   a1_10(2*M+1),a1_20(2*M+1),a1_11(2*M+1),a1_21(2*M+1),
     &   a2_10(2*L+1),a2_20(2*L+1),a2_11(2*L+1),a2_21(2*L+1),
     &   a3_10(3:2*N+1),a3_20(3:2*N+1),a3_11(3:2*N+1),a3_21(3:2*N+1),

     & surf_Z(0:2*M+2,0:2*L+2),
     & surf2_X(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & surf2_Y(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & surf2_Z(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),

     & dX_dq3_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & dY_dq3_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & dZ_dq3_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
    
     & Y_x_0(2*N_down_y-4:2*N_up_y+4,2*N_down_z-3:2*N_up_z+3),
     & Z_x_0(2*N_down_y-4:2*N_up_y+4,2*N_down_z-3:2*N_up_z+3),
     & Y_x_aW(2*N_down_y-4:2*N_up_y+4,2*N_down_z-3:2*N_up_z+3),
     & Z_x_aW(2*N_down_y-4:2*N_up_y+4,2*N_down_z-3:2*N_up_z+3),
  
     & X_y_0(2*N_down_x-4:2*N_up_x+4,2*N_down_z-3:2*N_up_z+3),
     & Z_y_0(2*N_down_x-4:2*N_up_x+4,2*N_down_z-3:2*N_up_z+3),
     & X_y_aL(2*N_down_x-4:2*N_up_x+4,2*N_down_z-3:2*N_up_z+3),
     & Z_y_aL(2*N_down_x-4:2*N_up_x+4,2*N_down_z-3:2*N_up_z+3),


     & F1x_y_0(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
     & F1x_y_aL(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
     & F1y_y_0(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
     & F1y_y_aL(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
     & F1z_y_0(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
     & F1z_y_aL(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
    
     & F2x_z_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & F2x_z_aH(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & F2y_z_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & F2y_z_aH(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & F2z_z_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & F2z_z_aH(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),

     & X_y_0_z_surf2(1:2*M+1),X_y_aL_z_surf2(1:2*M+1),
     & Y_x_0_z_surf2(1:2*L+1),Y_x_aW_z_surf2(1:2*L+1),
     & Z_x_0_z_surf2(1:2*L+1),Z_x_aW_z_surf2(1:2*L+1),
     & Z_y_0_z_surf2(1:2*M+1),Z_y_aL_z_surf2(1:2*M+1)) 

      X=0.0d0
      Y=0.0d0
      Z=0.0d0
      
      a1_10=0.0d0
      a1_20=0.0d0
      a1_11=0.0d0
      a1_21=0.0d0
      a2_10=0.0d0
      a2_20=0.0d0
      a2_11=0.0d0
      a2_21=0.0d0
      a3_10=0.0d0
      a3_20=0.0d0
      a3_11=0.0d0
      a3_21=0.0d0

!      surf_X=0.0d0
!      surf_Y=0.0d0
      surf_Z=0.0d0
      surf2_X=0.0d0
      surf2_Y=0.0d0
      surf2_Z=0.0d0

      dX_dq3_surf2=0.0d0
      dY_dq3_surf2=0.0d0
      dZ_dq3_surf2=0.0d0

      Y_x_0=0.0d0
      Z_x_0=0.0d0
      Y_x_aW=0.0d0
      Z_x_aW=0.0d0
      X_y_0=0.0d0
      Z_y_0=0.0d0
      X_y_aL=0.0d0
      Z_y_aL=0.0d0

      F1x_y_0=0.0d0
      F1x_y_aL=0.0d0
      F1y_y_0=0.0d0
      F1y_y_aL=0.0d0
      F1z_y_0=0.0d0
      F1z_y_aL=0.0d0
    
      F2x_z_surf2=0.0d0
      F2x_z_aH=0.0d0
      F2y_z_surf2=0.0d0
      F2y_z_aH=0.0d0
      F2z_z_surf2=0.0d0
      F2z_z_aH=0.0d0

      X_y_0_z_surf2=0.0d0
      X_y_aL_z_surf2=0.0d0
      Y_x_0_z_surf2=0.0d0
      Y_x_aW_z_surf2=0.0d0
      Z_x_0_z_surf2=0.0d0
      Z_x_aW_z_surf2=0.0d0
      Z_y_0_z_surf2=0.0d0
      Z_y_aL_z_surf2=0.0d0
     
 

      if(id==0) then
      print *,id,'allocated and zeroed'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)

!      print *,'x numbers',2*N_down_x-3,2*N_up_x+3
!      print *,'y numbers',2*N_down_y-3,2*N_up_y+3
!      print *,'z numbers',2*N_down_z-3,2*N_up_z+3

      open(unit=31,file='!_A_surf.txt',form='formatted') 
      read(31,*) a_s_1
      read(31,*) a_s_2
      close(31)

      A_surf1=-dble(a_s_1)
      A_surf2=-dble(a_s_2)

      open(unit=31,file='!_Surf_sign.txt',form='formatted') 
      read(31,*) surf_sign1
      close(31)

      surf_sign=dble(surf_sign1)

      open(unit=31,file='!_hx1_hx2_hx3_hx4.txt',form='formatted') 
      read(31,*) hx_1
      read(31,*) hx_2
      read(31,*) hx_3
      read(31,*) hx_4
      close(31)
      

      hx1=dble(hx_1)
      hx2=dble(hx_2)
      hx3=dble(hx_3)
      hx4=dble(hx_4)

      aaa1=hx1+0.5d0*(hx2-hx1)
      aaa2=hx3+0.5d0*(hx4-hx3)

      i2= floor((aaa1-0.5*(hx2-hx1))/(0.5*d))+1
      i3= floor((aaa1+0.5*(hx2-hx1))/(0.5*d))+1
      j2= floor((aaa2-0.5*(hx4-hx3))/(0.5*d))+1
      j3= floor((aaa2+0.5*(hx4-hx3))/(0.5*d))+1  
!      i2=1
!     i3=2*M+1
      
      hx1=0.5d0*d*(i2-1)
      hx2=0.5d0*d*(i3-1)
      aaa1=hx1+0.5d0*(hx2-hx1)

      hx3=0.5d0*d*(j2-1)
      hx4=0.5d0*d*(j3-1)
      aaa2=hx3+0.5d0*(hx4-hx3)

      if(id==0) then
      print *,hx1,hx2,hx3,hx4,'hx1-hx4'
      end if



      do j=j2,j3 ! CHANGE Z FOR REAL FORMULA
      do i=i2,i3
!      surf_X(i,j)=(i-1)*0.5*d
!      surf_Y(i,j)=(j-1)*0.5*d

!      surf_Z(i,j)=-0.25+0.25*(sin(0.5*pi+6.0*pi*real(i-1)*0.5*d/aW)+
!!     &                         sin(0.5*pi+pi*real(i-1)*0.5*d/aW)+
!     &                         sin(0.5*pi+3.0*pi*real(j-1)*0.5*d/aL))

!      surf_Z(i,j)=
!     & dsin(pi*(i-j2)/dble(i3-i2))*dsin(pi*(j-j2)/dble(j3-j2))*
!     &( A_surf1+A_surf2+   A_surf1*
!     &((3.0d0/4.0d0)*dcos((2.0d0*pi/(hx2-hx1))*((i-1)*0.5d0*d-aaa1))
!     &+(1.0d0/4.0d0)*
!     &dcos((2.0d0*3.0d0*pi/(hx2-hx1))*((i-1)*0.5d0*d-aaa1)))+
!     &                     A_surf2*
!     &((3.0d0/4.0d0)*dcos((2.0d0*pi/(hx4-hx3))*((j-1)*0.5d0*d-aaa2))
!     &+(1.0d0/4.0d0)*
!     &dcos((2.0d0*3.0d0*pi/(hx4-hx3))*((j-1)*0.5d0*d-aaa2)))) 


      surf_Z(i,j)=
!     &sin(pi*(i-j2)/real(i3-i2))*sin(pi*(j-j2)/real(j3-j2))*
     &surf_sign*( (A_surf1*
     &((3.0/4.0)*cos((pi/(hx2-hx1))*((i-1)*0.5*d-aaa1))
     &+(1.0/4.0)*cos((3.0*pi/(hx2-hx1))*((i-1)*0.5*d-aaa1))))*
     &   (A_surf2*
     &((3.0/4.0)*cos((pi/(hx4-hx3))*((j-1)*0.5*d-aaa2))
     &+(1.0/4.0)*cos((3.0*pi/(hx4-hx3))*((j-1)*0.5*d-aaa2)))))

      end do
      end do




      do j=1,2*L+1
      do i=1,i2-1
      surf_Z(i,j)=surf_Z(i2,j)
      end do

      do i=i3+1,2*M+1
      surf_Z(i,j)=surf_Z(i3,j)
      end do
      end do

      do i=1,2*M+1
      do j=1,j2-1
      surf_Z(i,j)=surf_Z(i,j2)
      end do

      do j=j3+1,2*L+1
      surf_Z(i,j)=surf_Z(i,j3)
      end do
      end do

      surf_Z(1,:)=surf_Z(2,:)
      surf_Z(2*M+1,:)=surf_Z(2*M,:)
      surf_Z(:,1)=surf_Z(:,2)
      surf_Z(:,2*L+1)=surf_Z(:,2*L)
      
      surf_Z(0,:)=surf_Z(1,:)
      surf_Z(2*M+2,:)=surf_Z(2*M+1,:)
      surf_Z(:,0)=surf_Z(:,1)
      surf_Z(:,2*L+2)=surf_Z(:,2*L+1)


      if(id==0) then
      open(35,file='Surface_Mountain.sct',form='binary',
     &status='unknown')
      do j=1,2*L+1
      do i=1,2*M+1
      write(35) real(surf_Z(i,j))
      end do
      end do
      close(35)
      end if


      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3
      
      dZ_dq1=rd*(surf_Z(i+1,j)-surf_Z(i-1,j))
      dZ_dq2=rd*(surf_Z(i,j+1)-surf_Z(i,j-1))
      
      sqr=sqrt(dZ_dq1*dZ_dq1+dZ_dq2*dZ_dq2+1.0d0)
      
      a_nx=-dZ_dq1/sqr
      a_ny=-dZ_dq2/sqr
      a_nz=1.0d0/sqr

      
      surf2_X(i,j)=(i-1)*0.5d0*d+d*a_nx
      surf2_Y(i,j)=(j-1)*0.5d0*d+d*a_ny
!      surf2_Z(i,j)=surf_Z(i,j)+d*a_nz*(aH-surf_Z(i,j))/aH
      surf2_Z(i,j)=surf_Z(i,j)+d*a_nz 
      end do
      end do

    

      if(id_z==0) then                ! CHANGE Z FOR REAL FORMULA
      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3
      X(i,j,1)=(i-1)*0.5d0*d
      Y(i,j,1)=(j-1)*0.5d0*d
      Z(i,j,1)=surf_Z(i,j)
      end do
      end do
      end if

!-- Surface ----------------------------------------------------------- 
     
      
!      open(35,file='(n_x,n_y,n_z).txt',form='formatted')

      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3
!      do j=2*N_down_y-2,2*N_up_y+2
!      do i=2*N_down_x-2,2*N_up_x+2
      
      dZ_dq1=rd*(surf_Z(i+1,j)-surf_Z(i-1,j))
      dZ_dq2=rd*(surf_Z(i,j+1)-surf_Z(i,j-1))
      
      sqr=sqrt(dZ_dq1*dZ_dq1+dZ_dq2*dZ_dq2+1.0d0)
      
      a_nx=-dZ_dq1/sqr
      a_ny=-dZ_dq2/sqr
      a_nz=1.0d0/sqr

      if(id_z==0) then
      X(i,j,2)=(i-1)*0.5d0*d+0.5d0*d*a_nx
      Y(i,j,2)=(j-1)*0.5d0*d+0.5d0*d*a_ny
      Z(i,j,2)=surf_Z(i,j)+0.5d0*d*a_nz

      X(i,j,0)=X(i,j,1)-0.5d0*d*a_nx
      Y(i,j,0)=Y(i,j,1)-0.5d0*d*a_ny
      Z(i,j,0)=Z(i,j,1)-0.5d0*d*a_nz 

      X(i,j,-1)=X(i,j,0)-0.5d0*d*a_nx
      Y(i,j,-1)=Y(i,j,0)-0.5d0*d*a_ny
      Z(i,j,-1)=Z(i,j,0)-0.5d0*d*a_nz 
      end if
 
      surf2_X(i,j)=(i-1)*0.5d0*d+d*a_nx
      surf2_Y(i,j)=(j-1)*0.5d0*d+d*a_ny
      surf2_Z(i,j)=surf_Z(i,j)+d*a_nz

! Derivatives on 3rd layer
      dX_dq3_surf2(i,j)=rd*(3.0d0*surf2_X(i,j)-
     &4.0d0*(0.5d0*(i-1)*0.5d0*d+0.5d0*surf2_X(i,j))+(i-1)*0.5d0*d)
      dY_dq3_surf2(i,j)=rd*(3.0d0*surf2_Y(i,j)-
     &4.0d0*(0.5d0*(j-1)*0.5d0*d+0.5d0*surf2_Y(i,j))+(j-1)*0.5d0*d)
      dZ_dq3_surf2(i,j)=rd*(3.0d0*surf2_Z(i,j)-
     &4.0d0*(0.5d0*surf_Z(i,j)+0.5d0*surf2_Z(i,j))+surf_Z(i,j))
     
      if(id_z==0) then
      X(i,j,3)=(i-1)*0.5d0*d+d*a_nx
      Y(i,j,3)=(j-1)*0.5d0*d+d*a_ny
      Z(i,j,3)=surf_Z(i,j)+d*a_nz
      end if

!      write(35,*) a_nx,a_ny,a_nz, i,j
      end do
!      write(35,*) ''
      end do

!      close(35)
    
      if(id==0) then
      print *,id,'surface and 2 layers'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)
     
    
!----------------------------------------------------------------------

!-- Edges of cube -----------------------------------------------------

! x=const
      if(id_x==0) then
      do k=2*N_down_z-3,2*N_up_z+3
      do j=2*N_down_y-3,2*N_up_y+3
      X(1,j,k)=0.0d0
      Y(1,j,k)=0.5d0*d*(j-1)
      if(k>3) then
      Z(1,j,k)=surf_Z(1,j)+0.5d0*d*(k-1)*(aH-surf_Z(1,j))/aH    ! change depanding on height
      end if
      end do
      end do
      end if

      if(id==0) then
      print *,id,'edges id_x==0'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)


      if(id_x==n_dims(1)-1) then
      do k=2*N_down_z-3,2*N_up_z+3
      do j=2*N_down_y-3,2*N_up_y+3
      X(2*M+1,j,k)=aW
      Y(2*M+1,j,k)=0.5d0*d*(j-1)
      if(k>3) then
      Z(2*M+1,j,k)=surf_Z(2*M+1,j)+0.5d0*d*(k-1)*(aH-surf_Z(2*M+1,j))/aH  ! change depanding on height
      end if
      end do
      end do
      end if

      if(id==0) then
      print *,id,'edges id_x==n_dims(1)-1'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)

!------
! y=const
      if(id_y==0) then
      do k=2*N_down_z-3,2*N_up_z+3
      do i=2*N_down_x-3,2*N_up_x+3
      X(i,1,k)=0.5d0*d*(i-1)
      Y(i,1,k)=0.0d0
      if(k>3) then
      Z(i,1,k)=surf_Z(i,1)+0.5d0*d*(k-1)*(aH-surf_Z(i,1))/aH     ! change depanding on height
      end if
      end do
      end do
      end if

      if(id==0) then
      print *,id,'edges id_y==0'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)

      if(id_y==n_dims(2)-1) then
      do k=2*N_down_z-3,2*N_up_z+3
      do i=2*N_down_x-3,2*N_up_x+3
      X(i,2*L+1,k)=0.5d0*d*(i-1)
      Y(i,2*L+1,k)=aL
      if(k>3) then
      Z(i,2*L+1,k)=surf_Z(i,2*L+1)+0.5d0*d*(k-1)*(aH-surf_Z(i,2*L+1))/aH     ! change depanding on height
      end if
      end do
      end do
      end if

      if(id==0) then
      print *,id,'edges id_y==n_dims(2)-1'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)

!-------
! z=aH
      if(id_z==n_dims(3)-1) then
      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3
      X(i,j,2*N+1)=(i-1)*0.5d0*d
      Y(i,j,2*N+1)=(j-1)*0.5d0*d
      Z(i,j,2*N+1)=aH
      end do 
      end do
      end if

      if(id==0) then
      print *,id,'edges id_z==n_dims(3)-1'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)
      
      if(id==0) then
      print *,id,'edges of cube'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)

!-------
!----------------------------------------------------------------------
      coef=aH-d

      open(unit=31,file='!_a3_11_a3_21_degree.txt',form='formatted') 
      read(31,*) i_deg
      close(31)

      do i=1,2*M+1
      a1_10(i)=(1.0d0+2.0d0*dble(i-1)/dble(2*M))*
     &             (dble(i-1)/dble(2*M)-1.0d0)**2
      a1_20(i)=1.0d0-a1_10(i)
      a1_11(i)=dble(i-1)/dble(2*M)*
     &             (1.0d0-dble(i-1)/dble(2*M))**2
      a1_21(i)=(dble(i-1)/dble(2*M)-1.0d0)*
     &                   (dble(i-1)/dble(2*M))**2
      end do

      do j=1,2*L+1
      a2_10(j)=(1.0d0+2.0d0*dble(j-1)/dble(2*L))*
     &             (dble(j-1)/dble(2*L)-1.0d0)**2
      a2_20(j)=1.0d0-a2_10(j)
      a2_11(j)=dble(j-1)/dble(2*L)*
     &             (1.0d0-dble(j-1)/dble(2*L))**2
      a2_21(j)=(dble(j-1)/dble(2*L)-1.0d0)*
     &                   (dble(j-1)/dble(2*L))**2
      end do

      do k=3,2*N+1
      a3_10(k)=(1.0d0+2.0d0*dble(k-3)/dble(2*N-2))*
     &             (dble(k-3)/dble(2*N-2)-1.0d0)**2
      a3_20(k)=1.0d0-a3_10(k)
      a3_11(k)=dble(k-3)/dble(2*N-2)*
     &         (1.0d0-dble(k-3)/dble(2*N-2))**i_deg*coef
      a3_21(k)=(dble(k-3)/dble(2*N-2)-1.0d0)*
     &         (dble(k-3)/dble(2*N-2))**i_deg*coef
      end do

      if(id==0) then
      print *,id,'a1_10 - a3_21'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)

!- Essential formula ---------------------------------------------------

      do k=2*N_down_z-3,2*N_up_z+3
      do j=2*N_down_y-4,2*N_up_y+4
      Y_x_0(j,k)=0.5d0*d*(j-1)
      Z_x_0(j,k)=surf_Z(1,j)+0.5d0*d*(k-1)*(aH-surf_Z(1,j))/aH
      end do
      end do
      
      do k=2*N_down_z-3,2*N_up_z+3
      do j=2*N_down_y-4,2*N_up_y+4
      Y_x_aW(j,k)=0.5d0*d*(j-1)
      Z_x_aW(j,k)=surf_Z(2*M+1,j)+0.5d0*d*(k-1)*(aH-surf_Z(2*M+1,j))/aH
      end do
      end do

      do j=1,2*L+1
      Y_x_0_z_surf2(j)=0.5d0*d*(j-1)
      Y_x_aW_z_surf2(j)=0.5d0*d*(j-1)
      Z_x_0_z_surf2(j)=surf_Z(1,j)+0.5d0*d*(3-1)*(aH-surf_Z(1,j))/aH
      Z_x_aW_z_surf2(j)=surf_Z(2*M+1,j)+0.5d0*d*(3-1)*
     &                  (aH-surf_Z(2*M+1,j))/aH
      end do

      do k=2*N_down_z-3,2*N_up_z+3
      do i=2*N_down_x-4,2*N_up_x+4
      X_y_0(i,k)=0.5d0*d*(i-1)
      Z_y_0(i,k)=surf_Z(i,1)+0.5d0*d*(k-1)*(aH-surf_Z(i,1))/aH
      end do
      end do

      do k=2*N_down_z-3,2*N_up_z+3
      do i=2*N_down_x-4,2*N_up_x+4
      X_y_aL(i,k)=0.5d0*d*(i-1)
      Z_y_aL(i,k)=surf_Z(i,2*L+1)+0.5d0*d*(k-1)*(aH-surf_Z(i,2*L+1))/aH
      end do
      end do

      do i=1,2*M+1
      X_y_0_z_surf2(i)=0.5d0*d*(i-1)
      X_y_aL_z_surf2(i)=0.5d0*d*(i-1)
      Z_y_0_z_surf2(i)=surf_Z(i,1)+0.5d0*d*(3-1)*(aH-surf_Z(i,1))/aH
      Z_y_aL_z_surf2(i)=surf_Z(i,2*L+1)+0.5d0*d*(3-1)*
     &                  (aH-surf_Z(i,2*L+1))/aH
      end do

      do k=2*N_down_z-3,2*N_up_z+3
      do i=2*N_down_x-3,2*N_up_x+3
      F1x_y_0(i,k)=a1_10(i)*0.0d0+a1_11(i)*1.0d0+
     &             a1_20(i)*aW+a1_21(i)*1.0d0
      F1x_y_aL(i,k)=a1_10(i)*0.0d0+a1_11(i)*1.0d0+
     &             a1_20(i)*aW+a1_21(i)*1.0d0

      F1y_y_0(i,k)=a1_10(i)*0.0d0+a1_11(i)*0.0d0+
     &             a1_20(i)*0.0d0+a1_21(i)*0.0d0
      F1y_y_aL(i,k)=a1_10(i)*aL+a1_11(i)*0.0d0+
     &              a1_20(i)*aL+a1_21(i)*0.0d0

      F1z_y_0(i,k)=a1_10(i)*
     &            (surf_Z(1,1)+0.5d0*d*(k-1)*(aH-surf_Z(1,1))/aH)+
     &             a1_11(i)*0.0d0+                ! Change depanding on height
     &             a1_20(i)*
     &          (surf_Z(2*M+1,1)+0.5d0*d*(k-1)*(aH-surf_Z(2*M+1,1))/aH)+
     &             a1_21(i)*0.0d0
      F1z_y_aL(i,k)=a1_10(i)*
     &          (surf_Z(1,2*L+1)+0.5d0*d*(k-1)*(aH-surf_Z(1,2*L+1))/aH)+
     &              a1_11(i)*0.0d0+                ! Change depanding on height
     &              a1_20(i)*
     &  (surf_Z(2*M+1,2*L+1)+0.5d0*d*(k-1)*(aH-surf_Z(2*M+1,2*L+1))/aH)+
     &              a1_21(i)*0.0d0                 ! Change depanding on height
      end do
      end do

      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3
      
      F2x_z_surf2(i,j)=(a1_11(i)+a1_20(i)*aW+a1_21(i))+           !F1x surf2_Z
     & a2_10(j)*(X_y_0_z_surf2(i)-(a1_11(i)+a1_20(i)*aW+a1_21(i)))+
     & a2_11(j)*0.0d0+
     & a2_20(j)*(X_y_aL_z_surf2(i)-(a1_11(i)+a1_20(i)*aW+a1_21(i)))+
     & a2_21(j)*0.0d0
      
      F2x_z_aH(i,j)=(a1_11(i)+a1_20(i)*aW+a1_21(i))+           !F1x aH
     & a2_10(j)*((i-1)*0.5d0*d-(a1_11(i)+a1_20(i)*aW+a1_21(i)))+
     & a2_11(j)*0.0d0+
     & a2_20(j)*((i-1)*0.5d0*d-(a1_11(i)+a1_20(i)*aW+a1_21(i)))+
     & a2_21(j)*0.0d0

      F2y_z_surf2(i,j)=(a1_10(i)*Y_x_0_z_surf2(j)+
     &                  a1_20(i)*Y_x_aW_z_surf2(j)) !F1y surf2_Z
     &+a2_10(j)*(0.0d0-(a1_10(i)*Y_x_0_z_surf2(1)+
     &                a1_20(i)*Y_x_aW_z_surf2(1)))+
     & a2_11(j)*0.0d0+
     & a2_20(j)*
     & (aL-(a1_10(i)*Y_x_0_z_surf2(2*L+1)+
     &      a1_20(i)*Y_x_aW_z_surf2(2*L+1)))+
     & a2_21(j)*0.0d0

      F2y_z_aH(i,j)=(a1_10(i)*0.5d0*d*(j-1)+a1_20(i)*0.5d0*d*(j-1))          !F1y aH
     &+a2_10(j)*(0.0d0-(a1_10(i)*0.5d0*d*(1-1)+a1_20(i)*0.5d0*d*(1-1)))+
     & a2_11(j)*0.0d0+
     & a2_20(j)*
     & (aL-(a1_10(i)*0.5d0*d*(2*L+1-1)+a1_20(i)*0.5d0*d*(2*L+1-1)))+
     & a2_21(j)*0.0d0

      F2z_z_surf2(i,j)=(a1_10(i)*Z_x_0_z_surf2(j)+
     &                  a1_20(i)*Z_x_aW_z_surf2(j))
     &+a2_10(j)*
     & (Z_y_0_z_surf2(i)-(a1_10(i)*Z_x_0_z_surf2(1)+
     &                    a1_20(i)*Z_x_aW_z_surf2(1)))+
     & a2_11(j)*0.0d0+                ! Change depanding on height
     & a2_20(j)*
     & (Z_y_aL_z_surf2(i)-
     & (a1_10(i)*Z_x_0_z_surf2(2*L+1)+a1_20(i)*Z_x_aW_z_surf2(2*L+1)))+
     & a2_21(j)*0.0d0 

      F2z_z_aH(i,j)=(a1_10(i)*aH+a1_20(i)*aH)
     &+a2_10(j)*
     & (aH-(a1_10(i)*aH+a1_20(i)*aH))+
     & a2_11(j)*0.0d0+                ! Change depanding on height
     & a2_20(j)*
     & (aH-(a1_10(i)*aH+a1_20(i)*aH))+
     & a2_21(j)*0.0d0                 ! Change depanding on height
      end do
      end do

!=======================================================================

      if(id_z==0) then
      do k=4,2*N_up_z+3
      do i=2*N_down_x-3,2*N_up_x+3
      do j=2*N_down_y-3,2*N_up_y+3

      F1x=a1_10(i)*0.0d0+a1_11(i)*1.0d0+a1_20(i)*aW+a1_21(i)*1.0d0
      F1y=a1_10(i)*Y_x_0(j,k)+a1_11(i)*0.0d0+
     &    a1_20(i)*Y_x_aW(j,k)+a1_21(i)*0.0d0 
      F1z=a1_10(i)*Z_x_0(j,k)+a1_11(i)*0.0d0+                ! Change depanding on height
     &    a1_20(i)*Z_x_aW(j,k)+a1_21(i)*0.0d0                 ! Change depanding on height
 
      dF1x_dq2=0.0d0
      dF1y_dq2=a1_10(i)*rd*(Y_x_0(j+1,k)-Y_x_0(j-1,k))+
     &         a1_11(i)*0.0d0+
     &         a1_20(i)*rd*(Y_x_aW(j+1,k)-Y_x_aW(j-1,k))+
     &         a1_21(i)*0.0d0
      dF1z_dq2=a1_10(i)*rd*(Z_x_0(j+1,k)-Z_x_0(j-1,k))+
     &         a1_11(i)*0.0d0+                                          ! Change depanding on height
     &         a1_20(i)*rd*(Z_x_aW(j+1,k)-Z_x_aW(j-1,k))+
     &         a1_21(i)*0.0d0
  
      F2x=F1x+a2_10(j)*(X_y_0(i,k)-F1x_y_0(i,k))+a2_11(j)*0.0d0+
     &        a2_20(j)*(X_y_aL(i,k)-F1x_y_aL(i,k))+a2_21(j)*0.0d0

      F2y=F1y+a2_10(j)*(0.0d0-F1y_y_0(i,k))+a2_11(j)*(1.0d0-1.0d0)+
     &        a2_20(j)*(aL-F1y_y_aL(i,k))+a2_21(j)*(1.0d0-1.0d0)

      F2z=F1z+a2_10(j)*(Z_y_0(i,k)-F1z_y_0(i,k))+a2_11(j)*(0.0d0-0.0d0)+  ! Change depanding on height
     &       a2_20(j)*(Z_y_aL(i,k)-F1z_y_aL(i,k))+a2_21(j)*(0.0d0-0.0d0) ! Change depanding on height

      dF1x_dq3=0.0d0
      dF1y_dq3=0.0d0
      dF1z_dq3=a1_10(i)*(aH-surf_Z(1,j))/aH+a1_11(i)*0.0d0+
     &         a1_20(i)*(aH-surf_Z(2*M+1,j))/aH+a1_21(i)*0.0d0            ! Change depanding on height   
 
      dF1x_dq2dq3=0.0d0
      dF1y_dq2dq3=0.0d0
      dF1z_dq2dq3=a1_10(i)*(-(surf_Z(1,j+1)-surf_Z(1,j-1))/aH)+
     &            a1_20(i)*(-(surf_Z(2*M+1,j+1)-surf_Z(2*M+1,j-1))/aH)       ! Change depanding on height

      dF2x_dq3=dF1x_dq3+a2_10(j)*(0.0d0-dF1x_dq3)+a2_11(j)*0.0d0+
     &                  a2_20(j)*(0.0d0-dF1x_dq3)+a2_21(j)*0.0d0
      dF2y_dq3=dF1y_dq3+a2_10(j)*(0.0d0-dF1y_dq3)+a2_11(j)*0.0d0+
     &                  a2_20(j)*(0.0d0-dF1y_dq3)+a2_21(j)*0.0d0
      dF2z_dq3=dF1z_dq3+a2_10(j)*((aH-surf_Z(i,1))/aH-dF1z_dq3)+
     &                  a2_11(j)*0.0d0+         ! Change depanding on height
     &                  a2_20(j)*((aH-surf_Z(i,2*L+1))/aH-1.0d0)+
     &                  a2_21(j)*0.0d0


      X(i,j,k)=F2x+a3_10(k)*(surf2_X(i,j)-F2x)+
     &             a3_11(k)*(dX_dq3_surf2(i,j)-dF2x_dq3)+
     &            a3_20(k)*((i-1)*0.5d0*d-F2x)+a3_21(k)*(0.0d0-dF2x_dq3)   !X(i,j,2*N+1)
      Y(i,j,k)=F2y+a3_10(k)*(surf2_Y(i,j)-F2y)+
     &             a3_11(k)*(dY_dq3_surf2(i,j)-dF2y_dq3)+
     &            a3_20(k)*((j-1)*0.5d0*d-F2y)+a3_21(k)*(0.0d0-dF2y_dq3)   !Y(i,j,2*N+1)
      Z(i,j,k)=F2z+a3_10(k)*(surf2_Z(i,j)-F2z_z_surf2(i,j))+                 ! !!!!!!!!!!!!!!!!!!!!!!!!
     &             a3_11(k)*(dZ_dq3_surf2(i,j)-dF2z_dq3)+
     &             a3_20(k)*(aH-F2z_z_aH(i,j))+a3_21(k)*(1.0d0-1.0d0)            !Z(i,j,2*N+1) 
      end do
      end do
      end do
 
      else

      do k=2*N_down_z-3,2*N_up_z+3
      do i=2*N_down_x-3,2*N_up_x+3
      do j=2*N_down_y-3,2*N_up_y+3

      F1x=a1_10(i)*0.0d0+a1_11(i)*1.0d0+a1_20(i)*aW+a1_21(i)*1.0d0
      F1y=a1_10(i)*Y_x_0(j,k)+a1_11(i)*0.0d0+
     &    a1_20(i)*Y_x_aW(j,k)+a1_21(i)*0.0d0 
      F1z=a1_10(i)*Z_x_0(j,k)+a1_11(i)*0.0d0+                ! Change depanding on height
     &    a1_20(i)*Z_x_aW(j,k)+a1_21(i)*0.0d0                 ! Change depanding on height
 
      dF1x_dq2=0.0d0
      dF1y_dq2=a1_10(i)*rd*(Y_x_0(j+1,k)-Y_x_0(j-1,k))+
     &         a1_11(i)*0.0d0+
     &         a1_20(i)*rd*(Y_x_aW(j+1,k)-Y_x_aW(j-1,k))+
     &         a1_21(i)*0.0d0
      dF1z_dq2=a1_10(i)*rd*(Z_x_0(j+1,k)-Z_x_0(j-1,k))+
     &         a1_11(i)*0.0d0+                                          ! Change depanding on height
     &         a1_20(i)*rd*(Z_x_aW(j+1,k)-Z_x_aW(j-1,k))+
     &         a1_21(i)*0.0d0
  
      F2x=F1x+a2_10(j)*(X_y_0(i,k)-F1x_y_0(i,k))+a2_11(j)*0.0d0+
     &        a2_20(j)*(X_y_aL(i,k)-F1x_y_aL(i,k))+a2_21(j)*0.0d0

      F2y=F1y+a2_10(j)*(0.0d0-F1y_y_0(i,k))+a2_11(j)*(1.0d0-1.0d0)+
     &        a2_20(j)*(aL-F1y_y_aL(i,k))+a2_21(j)*(1.0d0-1.0d0)

      F2z=F1z+a2_10(j)*(Z_y_0(i,k)-F1z_y_0(i,k))+a2_11(j)*(0.0d0-0.0d0)+  ! Change depanding on height
     &       a2_20(j)*(Z_y_aL(i,k)-F1z_y_aL(i,k))+a2_21(j)*(0.0d0-0.0d0) ! Change depanding on height

      dF1x_dq3=0.0d0
      dF1y_dq3=0.0d0
      dF1z_dq3=a1_10(i)*(aH-surf_Z(1,j))/aH+a1_11(i)*0.0d0+
     &         a1_20(i)*(aH-surf_Z(2*M+1,j))/aH+a1_21(i)*0.0d0            ! Change depanding on height   
 
      dF1x_dq2dq3=0.0d0
      dF1y_dq2dq3=0.0d0
      dF1z_dq2dq3=a1_10(i)*(-(surf_Z(1,j+1)-surf_Z(1,j-1))/aH)+
     &            a1_20(i)*(-(surf_Z(2*M+1,j+1)-surf_Z(2*M+1,j-1))/aH)       ! Change depanding on height

      dF2x_dq3=dF1x_dq3+a2_10(j)*(0.0d0-dF1x_dq3)+a2_11(j)*0.0d0+
     &                  a2_20(j)*(0.0d0-dF1x_dq3)+a2_21(j)*0.0d0
      dF2y_dq3=dF1y_dq3+a2_10(j)*(0.0d0-dF1y_dq3)+a2_11(j)*0.0d0+
     &                  a2_20(j)*(0.0d0-dF1y_dq3)+a2_21(j)*0.0d0
      dF2z_dq3=dF1z_dq3+a2_10(j)*((aH-surf_Z(i,1))/aH-dF1z_dq3)+
     &                  a2_11(j)*0.0d0+         ! Change depanding on height
     &                  a2_20(j)*((aH-surf_Z(i,2*L+1))/aH-1.0d0)+
     &                  a2_21(j)*0.0d0


      X(i,j,k)=F2x+a3_10(k)*(surf2_X(i,j)-F2x)+
     &             a3_11(k)*(dX_dq3_surf2(i,j)-dF2x_dq3)+
     &            a3_20(k)*((i-1)*0.5d0*d-F2x)+a3_21(k)*(0.0d0-dF2x_dq3)   !X(i,j,2*N+1)
      Y(i,j,k)=F2y+a3_10(k)*(surf2_Y(i,j)-F2y)+
     &             a3_11(k)*(dY_dq3_surf2(i,j)-dF2y_dq3)+
     &            a3_20(k)*((j-1)*0.5d0*d-F2y)+a3_21(k)*(0.0d0-dF2y_dq3)   !Y(i,j,2*N+1)
      Z(i,j,k)=F2z+a3_10(k)*(surf2_Z(i,j)-F2z_z_surf2(i,j))+                 ! !!!!!!!!!!!!!!!!!!!!!!!!
     &             a3_11(k)*(dZ_dq3_surf2(i,j)-dF2z_dq3)+
     &             a3_20(k)*(aH-F2z_z_aH(i,j))+a3_21(k)*(1.0d0-1.0d0)            !Z(i,j,2*N+1) 
      end do
      end do
      end do

      end if

!=======================================================================

      if(id==0) then
      print *,id,'Grid is calculated'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)

      deallocate(a1_10,a1_20,a1_11,a1_21,
     &a2_10,a2_20,a2_11,a2_21,a3_10,a3_20,a3_11,a3_21,  
     &surf_Z,surf2_X,surf2_Y,surf2_Z,
     &dX_dq3_surf2,dY_dq3_surf2,dZ_dq3_surf2,
     &Y_x_0,Z_x_0,Y_x_aW,Z_x_aW,X_y_0,Z_y_0,X_y_aL,Z_y_aL,    
     &F1x_y_0,F1x_y_aL,F1y_y_0,F1y_y_aL,F1z_y_0,F1z_y_aL,    
     &F2x_z_surf2,F2x_z_aH,F2y_z_surf2,F2y_z_aH,
     &F2z_z_surf2,F2z_z_aH,
     &X_y_0_z_surf2,X_y_aL_z_surf2,Y_x_0_z_surf2,Y_x_aW_z_surf2,
     &Z_x_0_z_surf2,Z_x_aW_z_surf2,Z_y_0_z_surf2,Z_y_aL_z_surf2)

!===========================================================================================================
!===========================================================================================================

























!===========================================================================================================

      allocate(
     & alambda
     & (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & amu
     & (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & Rho
     & (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & ttoRhoJ
     & (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),     

     & dq1_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),

     & A1(N_down_x:N_up_x,N_down_y:N_up_y),
     & A2(N_down_x:N_up_x,N_down_y:N_up_y),
     & A3(N_down_x:N_up_x,N_down_y:N_up_y),
     & B1(N_down_x:N_up_x,N_down_y:N_up_y),
     & B2(N_down_x:N_up_x,N_down_y:N_up_y),
     & B3(N_down_x:N_up_x,N_down_y:N_up_y),
     & C1(N_down_x:N_up_x,N_down_y:N_up_y),
     & C2(N_down_x:N_up_x,N_down_y:N_up_y),
     & C3(N_down_x:N_up_x,N_down_y:N_up_y),
    
     & R1_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_6(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_6(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_6(N_down_x:N_up_x,N_down_y:N_up_y),

     & dz_dq1_surf(N_down_x:N_up_x,N_down_y:N_up_y),
     & dz_dq2_surf(N_down_x:N_up_x,N_down_y:N_up_y) ) 


      alambda=0.0d0 ! lambda
      amu=0.0d0     ! mu
      Rho=0.0d0     ! density

      ttoRhoJ=0.0d0 ! Jacobian

      dq1_dx=1.0d0  ! Partial direvatives
      dq2_dx=0.0d0
      dq3_dx=0.0d0
      dq1_dy=0.0d0
      dq2_dy=1.0d0
      dq3_dy=0.0d0
      dq1_dz=0.0d0
      dq2_dz=0.0d0
      dq3_dz=1.0d0

      A1=0.0d0
      A2=0.0d0
      A3=0.0d0
      B1=0.0d0
      B2=0.0d0
      B3=0.0d0
      C1=0.0d0
      C2=0.0d0
      C3=0.0d0

      R1_1=0.0d0
      R1_2=0.0d0
      R1_3=0.0d0
      R1_4=0.0d0
      R1_5=0.0d0
      R1_6=0.0d0
      R2_1=0.0d0
      R2_2=0.0d0
      R2_3=0.0d0
      R2_4=0.0d0
      R2_5=0.0d0
      R2_6=0.0d0
      R3_1=0.0d0
      R3_2=0.0d0
      R3_3=0.0d0
      R3_4=0.0d0
      R3_5=0.0d0
      R3_6=0.0d0

      dz_dq1_surf=0.0d0
      dz_dq2_surf=0.0d0
      
!================================================================================
         ! reading from file X,Y,Z

         !Cartesian grid
!         do k=2*N_down_z-3,2*N_up_z+3
!         do j=2*N_down_y-3,2*N_up_y+3
!         do i=2*N_down_x-3,2*N_up_x+3
!         X(i,j,k)=(i-1)*d*0.5d0
!         Y(i,j,k)=(j-1)*d*0.5d0
!         Z(i,j,k)=(k-1)*d*0.5d0
!         end do
!         end do
!         end do

! Recording cordinates for snapshots --------------------------------------------
         o_filename2='X'
      write(unit=o_id_x,fmt=*) id_x
      write(unit=o_id_y,fmt=*) id_y
      write(unit=o_id_z,fmt=*) id_z
      
      o_filename1=trim(adjustl(o_filename2))//'_'//trim(adjustl(o_id_x))
     &//'_'//trim(adjustl(o_id_y))//'_'//trim(adjustl(o_id_z))//'.txt'c      
      
!      open(unit=31,file=o_filename1,form='formatted')  
!         do k=N_down_z,N_up_z
!         do j=N_down_y,N_up_y
!         do i=N_down_x,N_up_x
!         write(31,*) X(2*i,2*j,2*k), i,j,k
!         end do
!         write(31,*) ''
!         end do
!         write(31,*) ''
!         write(31,*) ''
!         end do
!         close(31)


         o_filename2='Y'
      
      o_filename1=trim(adjustl(o_filename2))//'_'//trim(adjustl(o_id_x))
     &//'_'//trim(adjustl(o_id_y))//'_'//trim(adjustl(o_id_z))//'.txt'c      
      
!      open(unit=31,file=o_filename1,form='formatted')  
!         do k=N_down_z,N_up_z
!         do j=N_down_y,N_up_y
!         do i=N_down_x,N_up_x
!         write(31,*) Y(2*i,2*j,2*k), i,j,k
!         end do
!         write(31,*) ''
!         end do
!         write(31,*) ''
!         write(31,*) ''
!         end do
!         close(31)


         o_filename2='Z'

      o_filename1=trim(adjustl(o_filename2))//'_'//trim(adjustl(o_id_x))
     &//'_'//trim(adjustl(o_id_y))//'_'//trim(adjustl(o_id_z))//'.txt'c      
      
!      open(unit=31,file=o_filename1,form='formatted')  
!         do k=N_down_z,N_up_z
!         do j=N_down_y,N_up_y
!         do i=N_down_x,N_up_x
!         write(31,*) Z(2*i,2*j,2*k), i,j,k
!         end do
!         write(31,*) ''
!         end do
!         write(31,*) ''
!         write(31,*) ''
!         end do
!         close(31)
!-------------------------------------------------------------------------------------------

      call MPI_BARRIER(n_cartesian_comm,n_error)  
      if(id==0) then
      print *,id,'X,Y,Z are done'
      print *,''
      end if 
      call MPI_BARRIER(n_cartesian_comm,n_error) 
!=========================================================================================================================================================================












!================= Source position ====================                         
      
      if(id==0) then

      allocate(amaxes(0:num_procs-1))

      amaxes=0.0d0

      print *,id,'finding x0,y0,z0'
      write(*,'(a,f6.3)') 'x0= ',x0
      write(*,'(a,f6.3)') 'y0= ',y0
      write(*,'(a,f6.3)') 'z0= ',z0
      print *,''
      end if

      call MPI_BARRIER(n_cartesian_comm,n_error)
      

      amax=1.0d0

      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      do j=N_down_y,N_up_y

      if(((X(2*i,2*j,2*k)-x0)**2 +
     &    (Y(2*i,2*j,2*k)-y0)**2 +
     &    (Z(2*i,2*j,2*k)-z0)**2 < amax)) then

      amax = (X(2*i,2*j,2*k)-x0)**2 +
     &       (Y(2*i,2*j,2*k)-y0)**2 +
     &       (Z(2*i,2*j,2*k)-z0)**2

      i_source=i
      j_source=j
      k_source=k

      end if

      end do
      end do
      end do

      
!      MPI_Send all amaxes to amaxes(:) at 0 process to find the smallest one
      if(id==0) then
      amaxes(0)=amax
      end if
   
      do i=1,num_procs-1

      if(id==i) then
      call MPI_Send(amax,1,MPI_DOUBLE_PRECISION,0,i,
     &n_cartesian_comm,n_error)     
      end if

      if(id==0) then
      call MPI_Recv(amaxes(i),1,MPI_DOUBLE_PRECISION,i,i,
     &n_cartesian_comm,n_status,n_error)      
      end if

      end do

      if(id==0) then

      amax2=1.0d0

      do i=0,num_procs-1
      if(amaxes(i)<amax2) then
      amax2=amaxes(i)
      id_source=i
      end if
      end do

      end if   
      

!     MPI_Send to all processes the number of the process having source location
      do i=1,num_procs-1

      if(id==0) then
      call MPI_Send(id_source,1,MPI_INTEGER,i,i,
     &n_cartesian_comm,n_error)     
      end if

      if(id==i) then
      call MPI_Recv(id_source,1,MPI_INTEGER,0,i,
     &n_cartesian_comm,n_status,n_error)      
      !print *, id, id_source
      end if

      end do

      if(id==0) then
      print *,'every process learns id_source'
      print *,''
      end if
      
      call MPI_BARRIER(n_cartesian_comm,n_error)
      do i=0,num_procs-1
      if(id==i) then
      print *, id,'id_source',id_source
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error) 
      end do
      call MPI_BARRIER(n_cartesian_comm,n_error)
      
      if(id==0) then
      print *,''
      end if

      if(id==id_source) then

      if(i_source==N_down_x) then
      i_source=i_source+2
      end if
      if(i_source==N_down_x+1) then
      i_source=i_source+1
      end if
      if(i_source==N_up_x) then
      i_source=i_source-2
      end if
      if(i_source==N_up_x-1) then
      i_source=i_source-1
      end if
      if(j_source==N_down_y) then
      j_source=j_source+2
      end if
      if(j_source==N_down_y+1) then
      j_source=j_source+1
      end if
      if(j_source==N_up_y) then
      j_source=j_source-2
      end if
      if(j_source==N_up_y-1) then
      j_source=j_source-1
      end if 
      if(k_source==N_down_z) then
      k_source=k_source+2
      end if
      if(k_source==N_down_z+1) then
      k_source=k_source+1
      end if
      if(k_source==N_up_z) then
      k_source=k_source-2
      end if
      if(k_source==N_up_z-1) then
      k_source=k_source-1
      end if

      print *,id,'new source coordinates' 
      write(*,'(a,f7.4)') 'x0= ', X(2*i_source,2*j_source,2*k_source)
      write(*,'(a,f7.4)') 'y0= ', Y(2*i_source,2*j_source,2*k_source)
      write(*,'(a,f7.4)') 'z0= ', Z(2*i_source,2*j_source,2*k_source)
      print *,''
      print *,'i,j,k source=',i_source,j_source,k_source
  
      end if
      
      if(id==0) then
      print *,''
      print *,''
      end if
    
      call MPI_BARRIER(n_cartesian_comm,n_error)
!======================================================


















!=========================================================================================================================================================================
!=========================================================================================================================================================================
!================= Medium parameters ==================
      
      do k=N_down_z-1,N_up_z+1
      do j=N_down_y-1,N_up_y+1
      do i=N_down_x-1,N_up_x+1

      if(k>=N/3) then
      alambda(i,j,k)=0.5d0
      amu(i,j,k)=0.25d0
      Rho(i,j,k)=1.0d0
      else
      alambda(i,j,k)=0.5d0
      amu(i,j,k)=0.25d0
      Rho(i,j,k)=1.0d0
      end if

      end do
      end do
      end do

      call MPI_BARRIER(n_cartesian_comm,n_error)
  
      if(id==0) then
      print *,id,'lambda, mu, rho are done'
      print *,''
      end if 
      call MPI_BARRIER(n_cartesian_comm,n_error) 
!======================================================
!================= Metrical coefficients ==============


!--------------------------- Jacobian -----------------
      if(id_z==0 .and. id==id_source) then
      open(35,file='Jacobian_1.txt',form='formatted')
      open(36,file='dz_dq3_1.txt',form='formatted')
      open(37,file='Jacobian_1-dz_dq3_1.txt',form='formatted')
      end if      

      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      do j=N_down_y,N_up_y
      dx_dq1=rd*(X(2*i+1,2*j,2*k)-X(2*i-1,2*j,2*k))
      dx_dq2=rd*(X(2*i,2*j+1,2*k)-X(2*i,2*j-1,2*k))
      dx_dq3=rd*(X(2*i,2*j,2*k+1)-X(2*i,2*j,2*k-1))

      dy_dq1=rd*(Y(2*i+1,2*j,2*k)-Y(2*i-1,2*j,2*k))
      dy_dq2=rd*(Y(2*i,2*j+1,2*k)-Y(2*i,2*j-1,2*k))
      dy_dq3=rd*(Y(2*i,2*j,2*k+1)-Y(2*i,2*j,2*k-1))

      dz_dq1=rd*(Z(2*i+1,2*j,2*k)-Z(2*i-1,2*j,2*k))
      dz_dq2=rd*(Z(2*i,2*j+1,2*k)-Z(2*i,2*j-1,2*k))
      dz_dq3=rd*(Z(2*i,2*j,2*k+1)-Z(2*i,2*j,2*k-1))

      aJac=dx_dq1*dy_dq2*dz_dq3+
     &     dx_dq2*dy_dq3*dz_dq1+
     &     dx_dq3*dy_dq1*dz_dq2-
     &     dx_dq3*dy_dq2*dz_dq1-
     &     dx_dq2*dy_dq1*dz_dq3-
     &     dx_dq1*dy_dq3*dz_dq2

      ttoRhoJ(i,j,k)=dt*dt/(Rho(i,j,k))

      if(k==1 .and. id==id_source) then                  ! WRITE SURFACE JACOBIAN
      write(35,*) aJac, i,j,k
      write(36,*) dz_dq3, i,j,k
      write(37,*) aJac-dz_dq3, i,j,k
      end if
      end do
      if(k==1 .and. id==id_source) then
      write(35,*) ''
      write(36,*) ''
      write(37,*) ''
      end if
      end do
      end do

      close(35)
      close(36)
      close(37)
 
      call MPI_BARRIER(n_cartesian_comm,n_error)  
      if(id==0) then
      print *,id,'ttoRhoJ is done'
      print *,''
      print *,id,'ttoRhoJ',abs(maxval(real(ttoRhoJ))),
     &maxloc(real(ttoRhoJ))
      print *,''
      end if 
      call MPI_BARRIER(n_cartesian_comm,n_error) 
!------------------------------------------------------
!-------------------------- Partials ------------------
      if(id_z==0 .and. id==id_source) then
      open(35,file='Jacobian_surf.txt',form='formatted')
      open(36,file='dz_dq3_surf.txt',form='formatted')
      open(46,file='Jacobian_surf-dz_dq3_surf.txt',form='formatted')
      open(47,file='Jacobian_surf-dz_dq3_surf_r.txt',form='formatted')
         

      open(37,file='dx_dq1_surf.txt',form='formatted')
      open(38,file='dx_dq2_surf.txt',form='formatted')
      open(39,file='dx_dq3_surf.txt',form='formatted')
      open(40,file='dy_dq1_surf.txt',form='formatted')
      open(41,file='dy_dq2_surf.txt',form='formatted')
      open(42,file='dy_dq3_surf.txt',form='formatted')
      open(43,file='dz_dq1_surf.txt',form='formatted')
      open(44,file='dz_dq2_surf.txt',form='formatted')
      open(45,file='dz_dq3_surf.txt',form='formatted')
      
      end if

      do k=2*N_down_z-2,2*N_up_z+2
      do i=2*N_down_x-2,2*N_up_x+2
      do j=2*N_down_y-2,2*N_up_y+2
      dx_dq1=rd*(X(i+1,j,k)-X(i-1,j,k))
      dx_dq2=rd*(X(i,j+1,k)-X(i,j-1,k))
      dx_dq3=rd*(X(i,j,k+1)-X(i,j,k-1))

      dy_dq1=rd*(Y(i+1,j,k)-Y(i-1,j,k))
      dy_dq2=rd*(Y(i,j+1,k)-Y(i,j-1,k))
      dy_dq3=rd*(Y(i,j,k+1)-Y(i,j,k-1))

      dz_dq1=rd*(Z(i+1,j,k)-Z(i-1,j,k))
      dz_dq2=rd*(Z(i,j+1,k)-Z(i,j-1,k))
      dz_dq3=rd*(Z(i,j,k+1)-Z(i,j,k-1))

      aJac=dx_dq1*dy_dq2*dz_dq3+
     &     dx_dq2*dy_dq3*dz_dq1+
     &     dx_dq3*dy_dq1*dz_dq2-
     &     dx_dq3*dy_dq2*dz_dq1-
     &     dx_dq2*dy_dq1*dz_dq3-
     &     dx_dq1*dy_dq3*dz_dq2

      if(k==1 .and. id==id_source) then
      write(35,*) aJac, i,j,k
      write(36,*) dz_dq3, i,j,k
      write(46,*) aJac-dz_dq3, i,j,k
      write(47,*) aJac-dz_dq3+dx_dq3*dz_dq1+dy_dq3*dz_dq2, i,j,k

      write(37,*) dx_dq1, i,j,k
      write(38,*) dx_dq2, i,j,k
      write(39,*) dx_dq3, i,j,k
      write(40,*) dy_dq1, i,j,k
      write(41,*) dy_dq2, i,j,k
      write(42,*) dy_dq3, i,j,k
      write(43,*) dz_dq1, i,j,k
      write(44,*) dz_dq2, i,j,k
      write(45,*) dz_dq3, i,j,k
      end if    

      dq1_dx(i,j,k)=(1.0d0/aJac)*(dy_dq2*dz_dq3-dy_dq3*dz_dq2)
      dq2_dx(i,j,k)=(1.0d0/aJac)*(dy_dq3*dz_dq1-dy_dq1*dz_dq3)
      dq3_dx(i,j,k)=(1.0d0/aJac)*(dy_dq1*dz_dq2-dy_dq2*dz_dq1)
      
      dq1_dy(i,j,k)=(1.0d0/aJac)*(dz_dq2*dx_dq3-dz_dq3*dx_dq2)
      dq2_dy(i,j,k)=(1.0d0/aJac)*(dz_dq3*dx_dq1-dz_dq1*dx_dq3)
      dq3_dy(i,j,k)=(1.0d0/aJac)*(dz_dq1*dx_dq2-dz_dq2*dx_dq1)

      dq1_dz(i,j,k)=(1.0d0/aJac)*(dx_dq2*dy_dq3-dx_dq3*dy_dq2)
      dq2_dz(i,j,k)=(1.0d0/aJac)*(dx_dq3*dy_dq1-dx_dq1*dy_dq3)
      dq3_dz(i,j,k)=(1.0d0/aJac)*(dx_dq1*dy_dq2-dx_dq2*dy_dq1)
      end do
      if(k==1 .and. id==id_source) then
      write(35,*) ''
      write(36,*) ''
      write(46,*) ''
      write(47,*) ''      

      write(37,*) ''
      write(38,*) ''
      write(39,*) ''
      write(40,*) ''
      write(41,*) ''
      write(42,*) ''
      write(43,*) '' 
      write(44,*) ''
      write(45,*) ''
      end if
      end do
      end do
      
      if(id_z==0 .and. id==id_source) then
      close(35)
      close(36)
      close(46)
      close(47)

      close(37)
      close(38)
      close(39)
      close(40)
      close(41)
      close(42)
      close(43)
      close(44)
      close(45)
      
      end if
 !     open(unit=31,file='dq1_dx.txt',form='formatted')
 !        do k1=2*N_down_z-2,2*N_up_z+2  
 !        do j1=2*N_down_y-2,2*N_up_y+2
 !        do i1=2*N_down_x-2,2*N_up_x+2
 !        write(31,*) dq1_dx(i1,j1,k1),i1,j1,k1
 !        end do
 !        write(31,*) ''
 !        end do         
 !        write(31,*) ''
 !        end do
 !     close(31)
 !     open(unit=31,file='dq2_dx.txt',form='formatted')
 !        do k1=2*N_down_z-2,2*N_up_z+2  
 !        do j1=2*N_down_y-2,2*N_up_y+2
 !        do i1=2*N_down_x-2,2*N_up_x+2
 !        write(31,*) dq2_dx(i1,j1,k1),i1,j1,k1
 !        end do
 !        write(31,*) ''
 !        end do         
 !        write(31,*) ''
 !        end do
 !     close(31)
 !     open(unit=31,file='dq3_dx.txt',form='formatted')
 !        do k1=2*N_down_z-2,2*N_up_z+2  
 !        do j1=2*N_down_y-2,2*N_up_y+2
 !        do i1=2*N_down_x-2,2*N_up_x+2
 !        write(31,*) dq3_dx(i1,j1,k1),i1,j1,k1
 !        end do
 !        write(31,*) ''
 !        end do         
 !        write(31,*) ''
 !        end do
 !     close(31)
 !     open(unit=31,file='dq1_dy.txt',form='formatted')
 !        do k1=2*N_down_z-2,2*N_up_z+2  
 !        do j1=2*N_down_y-2,2*N_up_y+2
 !        do i1=2*N_down_x-2,2*N_up_x+2
 !        write(31,*) dq1_dy(i1,j1,k1),i1,j1,k1
 !        end do
 !        write(31,*) ''
 !        end do         
 !        write(31,*) ''
 !        end do
 !     close(31)
 !     open(unit=31,file='dq2_dy.txt',form='formatted')
 !        do k1=2*N_down_z-2,2*N_up_z+2  
 !        do j1=2*N_down_y-2,2*N_up_y+2
 !        do i1=2*N_down_x-2,2*N_up_x+2
 !        write(31,*) dq2_dy(i1,j1,k1),i1,j1,k1
 !        end do
 !        write(31,*) ''
 !        end do         
 !        write(31,*) ''
 !        end do
 !     close(31)
 !     open(unit=31,file='dq3_dy.txt',form='formatted')
 !        do k1=2*N_down_z-2,2*N_up_z+2  
 !        do j1=2*N_down_y-2,2*N_up_y+2
 !        do i1=2*N_down_x-2,2*N_up_x+2
 !        write(31,*) dq3_dy(i1,j1,k1),i1,j1,k1
 !        end do
 !        write(31,*) ''
 !        end do         
 !        write(31,*) ''
 !        end do
 !     close(31)
 !     open(unit=31,file='dq1_dz.txt',form='formatted')
 !        do k1=2*N_down_z-2,2*N_up_z+2  
 !        do j1=2*N_down_y-2,2*N_up_y+2
 !        do i1=2*N_down_x-2,2*N_up_x+2
 !        write(31,*) dq1_dz(i1,j1,k1),i1,j1,k1
 !        end do
 !        write(31,*) ''
 !        end do         
 !        write(31,*) ''
 !        end do
 !     close(31)
 !     open(unit=31,file='dq2_dz.txt',form='formatted')
 !        do k1=2*N_down_z-2,2*N_up_z+2  
 !        do j1=2*N_down_y-2,2*N_up_y+2
 !        do i1=2*N_down_x-2,2*N_up_x+2
 !        write(31,*) dq2_dz(i1,j1,k1),i1,j1,k1
 !        end do
 !        write(31,*) ''
 !        end do         
 !        write(31,*) ''
 !        end do
 !     close(31)
 !     open(unit=31,file='dq3_dz.txt',form='formatted')
 !        do k1=2*N_down_z-2,2*N_up_z+2  
 !        do j1=2*N_down_y-2,2*N_up_y+2
 !        do i1=2*N_down_x-2,2*N_up_x+2
 !        write(31,*) dq3_dz(i1,j1,k1),i1,j1,k1
 !        end do
 !        write(31,*) ''
 !        end do         
 !        write(31,*) ''
 !        end do
 !     close(31)

      call MPI_BARRIER(n_cartesian_comm,n_error)
      do i=0,num_procs-1
      if(id==i) then
      print *,id,'dq1_dx',abs(maxval(real(dq1_dx))),maxloc(real(dq1_dx))
      print *,id,'dq2_dx',abs(maxval(real(dq2_dx))),maxloc(real(dq2_dx))
      print *,id,'dq3_dx',abs(maxval(real(dq3_dx))),maxloc(real(dq3_dx))
      print *,id,'dq1_dy',abs(maxval(real(dq1_dy))),maxloc(real(dq2_dy))
      print *,id,'dq2_dy',abs(maxval(real(dq2_dy))),maxloc(real(dq2_dy))
      print *,id,'dq3_dy',abs(maxval(real(dq3_dy))),maxloc(real(dq3_dy))
      print *,id,'dq1_dz',abs(maxval(real(dq1_dz))),maxloc(real(dq1_dz))
      print *,id,'dq2_dz',abs(maxval(real(dq2_dz))),maxloc(real(dq2_dz))
      print *,id,'dq3_dz',abs(maxval(real(dq3_dz))),maxloc(real(dq3_dz))
      print *,''      
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)
      end do
      call MPI_BARRIER(n_cartesian_comm,n_error)  

      if(id_z==0 .and. id==id_source) then
      open(unit=31,file='dq1_dx.txt',form='formatted')  
         do j=2*N_down_y-2,2*N_up_y+2
         do i=2*N_down_x-2,2*N_up_x+2
         write(31,*) dq1_dx(i,j,1), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='dq2_dx.txt',form='formatted')  
         do j=2*N_down_y-2,2*N_up_y+2
         do i=2*N_down_x-2,2*N_up_x+2
         write(31,*) dq2_dx(i,j,1), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='dq3_dx.txt',form='formatted')  
         do j=2*N_down_y-2,2*N_up_y+2
         do i=2*N_down_x-2,2*N_up_x+2
         write(31,*) dq3_dx(i,j,1), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='dq1_dy.txt',form='formatted')  
         do j=2*N_down_y-2,2*N_up_y+2
         do i=2*N_down_x-2,2*N_up_x+2
         write(31,*) dq1_dy(i,j,1), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='dq2_dy.txt',form='formatted')  
         do j=2*N_down_y-2,2*N_up_y+2
         do i=2*N_down_x-2,2*N_up_x+2
         write(31,*) dq2_dy(i,j,1), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='dq3_dy.txt',form='formatted')  
         do j=2*N_down_y-2,2*N_up_y+2
         do i=2*N_down_x-2,2*N_up_x+2
         write(31,*) dq3_dy(i,j,1), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='dq1_dz.txt',form='formatted')  
         do j=2*N_down_y-2,2*N_up_y+2
         do i=2*N_down_x-2,2*N_up_x+2
         write(31,*) dq1_dz(i,j,1), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='dq2_dz.txt',form='formatted')  
         do j=2*N_down_y-2,2*N_up_y+2
         do i=2*N_down_x-2,2*N_up_x+2
         write(31,*) dq2_dz(i,j,1), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='dq3_dz.txt',form='formatted')  
         do j=2*N_down_y-2,2*N_up_y+2
         do i=2*N_down_x-2,2*N_up_x+2
         write(31,*) dq3_dz(i,j,1), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      end if


      if(id==0) then
      print *,id,'partials are done'
      print *,''
      print *,''
      end if 
      call MPI_BARRIER(n_cartesian_comm,n_error) 
!---------------------------------------------------------------

!------------------------- Free surface coefficients -----------

      if(id_z==0) then

      do i=N_down_x,N_up_x
      do j=N_down_y,N_up_y
!      dx_dq1=1.0d0
!      dx_dq2=0.0d0
!      dx_dq3=rd*(X(2*i,2*j,2)-X(2*i,2*j,0))

!      dy_dq1=0.0d0
!      dy_dq2=1.0d0
!      dy_dq3=rd*(X(2*i,2*j,2)-X(2*i,2*j,0))

!      dz_dq1=rd*(Z(2*i+1,2*j,1)-Z(2*i-1,2*j,1))
!      dz_dq2=rd*(Z(2*i,2*j+1,1)-Z(2*i,2*j-1,1))
!      dz_dq3=rd*(Z(2*i,2*j,2)-Z(2*i,2*j,0)) 

      a_nx=-rd*(Z(2*i+1,2*j,1)-Z(2*i-1,2*j,1))
      a_ny=-rd*(Z(2*i,2*j+1,1)-Z(2*i,2*j-1,1))
      a_nz=1.0d0
      


      A1(i,j)=a_nx*(alambda(i,j,1)+2.0d0*amu(i,j,1))*dq3_dx(2*i,2*j,1)+    
     &        a_ny*amu(i,j,1)*dq3_dy(2*i,2*j,1)+
     &        a_nz*amu(i,j,1)*dq3_dz(2*i,2*j,1)

      A2(i,j)=a_nx*alambda(i,j,1)*dq3_dy(2*i,2*j,1)+
     &        a_ny*amu(i,j,1)*dq3_dx(2*i,2*j,1) 

      A3(i,j)=a_nx*alambda(i,j,1)*dq3_dz(2*i,2*j,1)+
     &        a_nz*amu(i,j,1)*dq3_dx(2*i,2*j,1)


      B1(i,j)=a_nx*amu(i,j,1)*dq3_dy(2*i,2*j,1)+
     &        a_ny*alambda(i,j,1)*dq3_dx(2*i,2*j,1)

      B2(i,j)=a_nx*amu(i,j,1)*dq3_dx(2*i,2*j,1)+    
     &        a_ny*(alambda(i,j,1)+2.0d0*amu(i,j,1))*dq3_dy(2*i,2*j,1)+
     &        a_nz*amu(i,j,1)*dq3_dz(2*i,2*j,1)

      B3(i,j)=a_ny*alambda(i,j,1)*dq3_dz(2*i,2*j,1)+
     &        a_nz*amu(i,j,1)*dq3_dy(2*i,2*j,1)


      C1(i,j)=a_nx*amu(i,j,1)*dq3_dz(2*i,2*j,1)+
     &        a_ny*alambda(i,j,1)*dq3_dx(2*i,2*j,1)

      C2(i,j)=a_ny*amu(i,j,1)*dq3_dz(2*i,2*j,1)+
     &        a_nz*alambda(i,j,1)*dq3_dy(2*i,2*j,1)

      C3(i,j)=a_nx*amu(i,j,1)*dq3_dx(2*i,2*j,1)+    
     &        a_ny*amu(i,j,1)*dq3_dy(2*i,2*j,1)+
     &        a_nz*(alambda(i,j,1)+2.0d0*amu(i,j,1))*dq3_dz(2*i,2*j,1)




      R1_1(i,j)=a_nx*(alambda(i,j,1)+2.0d0*amu(i,j,1))*dq1_dx(2*i,2*j,1)
     &         +a_ny*amu(i,j,1)*dq1_dy(2*i,2*j,1)+
     &          a_nz*amu(i,j,1)*dq1_dz(2*i,2*j,1)

      R1_2(i,j)=a_nx*(alambda(i,j,1)+2.0d0*amu(i,j,1))*dq2_dx(2*i,2*j,1)
     &         +a_ny*amu(i,j,1)*dq2_dy(2*i,2*j,1)+
     &          a_nz*amu(i,j,1)*dq2_dz(2*i,2*j,1)

      R1_3(i,j)=a_nx*alambda(i,j,1)*dq1_dy(2*i,2*j,1)+
     &          a_ny*amu(i,j,1)*dq1_dx(2*i,2*j,1)

      R1_4(i,j)=a_nx*alambda(i,j,1)*dq2_dy(2*i,2*j,1)+
     &          a_ny*amu(i,j,1)*dq2_dx(2*i,2*j,1)

      R1_5(i,j)=a_nx*alambda(i,j,1)*dq1_dz(2*i,2*j,1)+
     &          a_nz*amu(i,j,1)*dq1_dx(2*i,2*j,1)

      R1_6(i,j)=a_nx*alambda(i,j,1)*dq2_dz(2*i,2*j,1)+
     &          a_nz*amu(i,j,1)*dq2_dx(2*i,2*j,1)


      R2_1(i,j)=a_nx*amu(i,j,1)*dq1_dy(2*i,2*j,1)+
     &          a_ny*alambda(i,j,1)*dq1_dx(2*i,2*j,1)

      R2_2(i,j)=a_nx*amu(i,j,1)*dq2_dy(2*i,2*j,1)+
     &          a_ny*alambda(i,j,1)*dq2_dx(2*i,2*j,1)

      R2_3(i,j)=a_nx*amu(i,j,1)*dq1_dx(2*i,2*j,1)+
     &          a_ny*(alambda(i,j,1)+2.0d0*amu(i,j,1))*dq1_dy(2*i,2*j,1)
     &         +a_nz*amu(i,j,1)*dq1_dz(2*i,2*j,1)

      R2_4(i,j)=a_nx*amu(i,j,1)*dq2_dx(2*i,2*j,1)+
     &          a_ny*(alambda(i,j,1)+2.0d0*amu(i,j,1))*dq2_dy(2*i,2*j,1)
     &         +a_nz*amu(i,j,1)*dq2_dz(2*i,2*j,1)

      R2_5(i,j)=a_ny*alambda(i,j,1)*dq1_dz(2*i,2*j,1)+
     &          a_nz*amu(i,j,1)*dq1_dy(2*i,2*j,1)

      R2_6(i,j)=a_ny*alambda(i,j,1)*dq2_dz(2*i,2*j,1)+
     &          a_nz*amu(i,j,1)*dq2_dy(2*i,2*j,1)


      R3_1(i,j)=a_nx*amu(i,j,1)*dq1_dz(2*i,2*j,1)+
     &          a_nz*alambda(i,j,1)*dq1_dx(2*i,2*j,1)

      R3_2(i,j)=a_nx*amu(i,j,1)*dq2_dz(2*i,2*j,1)+
     &          a_nz*alambda(i,j,1)*dq2_dx(2*i,2*j,1)

      R3_3(i,j)=a_ny*amu(i,j,1)*dq1_dz(2*i,2*j,1)+
     &          a_nz*alambda(i,j,1)*dq1_dy(2*i,2*j,1)

      R3_4(i,j)=a_ny*amu(i,j,1)*dq2_dz(2*i,2*j,1)+
     &          a_nz*alambda(i,j,1)*dq2_dy(2*i,2*j,1)

      R3_5(i,j)=a_nx*amu(i,j,1)*dq1_dx(2*i,2*j,1)+
     &          a_ny*amu(i,j,1)*dq1_dy(2*i,2*j,1)+
     &          a_nz*(alambda(i,j,1)+2.0d0*amu(i,j,1))*dq1_dz(2*i,2*j,1)

      R3_6(i,j)=a_nx*amu(i,j,1)*dq2_dx(2*i,2*j,1)+
     &          a_ny*amu(i,j,1)*dq2_dy(2*i,2*j,1)+
     &          a_nz*(alambda(i,j,1)+2.0d0*amu(i,j,1))*dq2_dz(2*i,2*j,1)

      
      end do
      end do

      if(id_z==0 .and. id==id_source) then
      open(unit=31,file='dz_dq1_surf.txt',form='formatted')              ! PRINT ALL SURFACE DATA
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) dz_dq1_surf(i,j), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='dz_dq2_surf.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) dz_dq2_surf(i,j), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='A1.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) A1(i,j), i,j
         end do
         write(31,*) ''
         end do         
      close(31)
      open(unit=31,file='A2.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) A2(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='A3.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) A3(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='B1.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) B1(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='B2.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) B2(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='B3.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) B3(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='C1.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) C1(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='C2.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) C2(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='C3.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) C3(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      
      open(unit=31,file='R1_1.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R1_1(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R1_2.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R1_2(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R1_3.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R1_3(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R1_4.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R1_4(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R1_5.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R1_5(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R1_6.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R1_6(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R2_1.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R2_1(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R2_2.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R2_2(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R2_3.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R2_3(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R2_4.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R2_4(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R2_5.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R2_5(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R2_6.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R2_6(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R3_1.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R3_1(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R3_2.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R3_2(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R3_3.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R3_3(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R3_4.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R3_4(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R3_5.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R3_5(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
      open(unit=31,file='R3_6.txt',form='formatted')  
         do j=N_down_y,N_up_y
         do i=N_down_x,N_up_x
         write(31,*) R3_6(i,j), i,j
         end do
         write(31,*) ''
         end do
      close(31)
  
      end if
!      print *,id,'A1',abs(maxval(real(A1))),maxloc(real(A1))
!      print *,id,'A2',abs(maxval(real(A2))),maxloc(real(A2))
!      print *,id,'A3',abs(maxval(real(A3))),maxloc(real(A3))
!      print *,id,'B1',abs(maxval(real(B1))),maxloc(real(B1))
!      print *,id,'B2',abs(maxval(real(B2))),maxloc(real(B2))
!      print *,id,'B3',abs(maxval(real(B3))),maxloc(real(B3))
!      print *,id,'C1',abs(maxval(real(C1))),maxloc(real(C1))
!      print *,id,'C2',abs(maxval(real(C2))),maxloc(real(C2))
!      print *,id,'C3',abs(maxval(real(C3))),maxloc(real(C3))
!      print *,''
!      print *,id,'R1_1',abs(maxval(real(R1_1))),maxloc(real(R1_1))
!      print *,id,'R1_2',abs(maxval(real(R1_2))),maxloc(real(R1_2))
!      print *,id,'R1_3',abs(maxval(real(R1_3))),maxloc(real(R1_3))
!      print *,id,'R1_4',abs(maxval(real(R1_4))),maxloc(real(R1_4))
!      print *,id,'R1_5',abs(maxval(real(R1_5))),maxloc(real(R1_5))
!      print *,id,'R2_1',abs(maxval(real(R2_1))),maxloc(real(R2_1))
!      print *,id,'R2_2',abs(maxval(real(R2_2))),maxloc(real(R2_2))
!      print *,id,'R2_3',abs(maxval(real(R2_3))),maxloc(real(R2_3))
!      print *,id,'R2_4',abs(maxval(real(R2_4))),maxloc(real(R2_4))
!      print *,id,'R2_6',abs(maxval(real(R2_6))),maxloc(real(R2_6))
!      print *,id,'R3_1',abs(maxval(real(R3_1))),maxloc(real(R3_1))
!      print *,id,'R3_4',abs(maxval(real(R3_4))),maxloc(real(R3_4))
!      print *,id,'R3_5',abs(maxval(real(R3_5))),maxloc(real(R3_5))
!      print *,id,'R3_6',abs(maxval(real(R3_6))),maxloc(real(R3_6))
!      print *,id,'dz_dq1_surf',abs(maxval(real(dz_dq1_surf))),
!     &                             maxloc(real(dz_dq1_surf))
!      print *,id,'dz_dq2_surf',abs(maxval(real(dz_dq2_surf))),
!     &                             maxloc(real(dz_dq2_surf))
!      print *,''

      end if

      
!---------------------------------------------------------------
      
      call MPI_BARRIER(n_cartesian_comm,n_error)  
      if(id==0) then
      print *,id,'free surface coefficients are done'
      print *,''
      end if 
      call MPI_BARRIER(n_cartesian_comm,n_error) 




!======================== Traces ======================

      allocate(tr_x(num_tr),tr_y(num_tr),tr_z(num_tr),
     &         i_tr(num_tr),j_tr(num_tr),k_tr(num_tr),
     & id_tr(num_tr),
     & trace_U(num_tr,0:iTime),
     & trace_V(num_tr,0:iTime),
     & trace_W(num_tr,0:iTime))

      tr_x=0.0d0
      tr_y=0.0d0
      tr_z=0.0d0
   
      i_tr=0
      j_tr=0
      k_tr=0
      
      id_tr=0

      trace_U=0.0d0
      trace_V=0.0d0
      trace_W=0.0d0


      
      do i=1,num_tr
      tr_x(i)=1.0d0*i
      tr_y(i)=1.0d0*i
      tr_z(i)=1.0d0*i
      end do
      

      do l1=1,num_tr
      
      xtr=tr_x(l1)
      ytr=tr_y(l1)
      ztr=tr_z(l1)

      amax=1.0d0

      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      do j=N_down_y,N_up_y

      if(((X(2*i,2*j,2*k)-xtr)**2 +
     &    (Y(2*i,2*j,2*k)-ytr)**2 +
     &    (Z(2*i,2*j,2*k)-ztr)**2 < amax)) then

      amax = (X(2*i,2*j,2*k)-xtr)**2 +
     &       (Y(2*i,2*j,2*k)-ytr)**2 +
     &       (Z(2*i,2*j,2*k)-ztr)**2

      i_tr(l1)=i
      j_tr(l1)=j
      k_tr(l1)=k

      end if

      end do
      end do
      end do

      
!      MPI_Send all amaxes to amaxes(:) at 0 process to find the smallest one
      
      if(id==0) then
      amaxes(0)=amax
      end if

      do i=1,num_procs-1

      if(id==i) then
      call MPI_Send(amax,1,MPI_DOUBLE_PRECISION,0,i,
     &n_cartesian_comm,n_error)     
      end if

      if(id==0) then
      call MPI_Recv(amaxes(i),1,MPI_DOUBLE_PRECISION,i,i,
     &n_cartesian_comm,n_status,n_error)      
      end if

      end do

      if(id==0) then
      amax2=1.0d0
      do i=0,num_procs-1
      if(amaxes(i)<amax2) then
      amax2=amaxes(i)
      id_tr(l1)=i
      end if
      end do
      end if
   
      
!     MPI_Send to all processes the number of the process having trace location
      do i=1,num_procs-1

      if(id==0) then
      call MPI_Send(id_tr(l1),1,MPI_INTEGER,i,i,
     &n_cartesian_comm,n_error)     
      end if

      if(id==i) then
      call MPI_Recv(id_tr(l1),1,MPI_INTEGER,0,i,
     &n_cartesian_comm,n_status,n_error)      
      
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)
      end do
     
      call MPI_BARRIER(n_cartesian_comm,n_error)
      
      do i=0,num_procs-1
      if(id==i) then
      write(*,'(a,i3,a,i3,a,i3)') 'id= ',id, '   number of trace= ', l1,
     &'   id of trace= ', id_tr(l1)
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)
      end do
         

      end do !l1

      if(id==0) then
      print *,''
      print *,''
      end if

      call MPI_BARRIER(n_cartesian_comm,n_error)  

      do i=1,num_tr
      if(id==id_tr(i)) then
      write(*,'(a,i3,a,i3)') 'id= ', id,'   new X,Y,Z for trace ',i
      write(*,'(a,f6.3)') 'X= ', X(2*i_tr(i),2*j_tr(i),2*k_tr(i))
      write(*,'(a,f6.3)') 'Y= ', Y(2*i_tr(i),2*j_tr(i),2*k_tr(i))
      write(*,'(a,f6.3)') 'Z= ', Z(2*i_tr(i),2*j_tr(i),2*k_tr(i))
      print *,''
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)
      end do
      
      call MPI_BARRIER(n_cartesian_comm,n_error)
!======================================================

      deallocate(X,Y,Z)
!------------------------------------------------------

      

!======================================================



      allocate(
     &  U(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  V(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  W(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & UU(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & VV(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & WW(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1), 
     & F(0:iTT),
     &       A_N_down_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),
     &         A_N_up_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),
     &         A_down_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),
     &           A_up_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),

     &       A_N_down_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),
     &         A_N_up_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),
     &         A_down_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),
     &           A_up_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),

     &       A_N_down_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)),
     &         A_N_up_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)),
     &         A_down_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)),
     &           A_up_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)),

     & U_down_x(N_down_y:N_up_y),U_up_x(N_down_y:N_up_y),
     & U_down_y(N_down_x:N_up_x),U_up_y(N_down_x:N_up_x),
     & U_N_down_x(N_down_y:N_up_y),U_N_up_x(N_down_y:N_up_y),
     & U_N_down_y(N_down_x:N_up_x),U_N_up_y(N_down_x:N_up_x),
 
     & V_down_x(N_down_y:N_up_y),V_up_x(N_down_y:N_up_y),
     & V_down_y(N_down_x:N_up_x),V_up_y(N_down_x:N_up_x),
     & V_N_down_x(N_down_y:N_up_y),V_N_up_x(N_down_y:N_up_y),
     & V_N_down_y(N_down_x:N_up_x),V_N_up_y(N_down_x:N_up_x),

     & W_down_x(N_down_y:N_up_y),W_up_x(N_down_y:N_up_y),
     & W_down_y(N_down_x:N_up_x),W_up_y(N_down_x:N_up_x),
     & W_N_down_x(N_down_y:N_up_y),W_N_up_x(N_down_y:N_up_y),
     & W_N_down_y(N_down_x:N_up_x),W_N_up_y(N_down_x:N_up_x)

     &         )
    
     
      U=0.0d0
      UU=0.0d0
      V=0.0d0
      VV=0.0d0
      W=0.0d0
      WW=0.0d0
      F=0.0d0
 
      A_N_down_x=0.0d0
      A_N_up_x=0.0d0
      A_down_x=0.0d0
      A_up_x=0.0d0
      A_N_down_y=0.0d0
      A_N_up_y=0.0d0
      A_down_y=0.0d0
      A_up_y=0.0d0
      A_N_down_z=0.0d0
      A_N_up_z=0.0d0
      A_down_z=0.0d0
      A_up_z=0.0d0

      U_down_x=0.0d0
      U_up_x=0.0d0
      U_down_y=0.0d0
      U_up_y=0.0d0
      U_N_down_x=0.0d0
      U_N_up_x=0.0d0
      U_N_down_y=0.0d0
      U_N_up_y=0.0d0
 
      V_down_x=0.0d0
      V_up_x=0.0d0
      V_down_y=0.0d0
      V_up_y=0.0d0
      V_N_down_x=0.0d0
      V_N_up_x=0.0d0
      V_N_down_y=0.0d0
      V_N_up_y=0.0d0

      W_down_x=0.0d0
      W_up_x=0.0d0
      W_down_y=0.0d0
      W_up_y=0.0d0
      W_N_down_x=0.0d0
      W_N_up_x=0.0d0
      W_N_down_y=0.0d0
      W_N_up_y=0.0d0

!================= F =================================================     
      p_s=0.0d0 
       
      if(id==id_source) then
      p_s = 0.125d0*ttoRhoJ(i_source,j_source,k_source)/(d*d*d*d)   ! dt*dt/(Rho*J*S_pyramid)
      end if

      call MPI_BARRIER(n_cartesian_comm,n_error)
      print *,id,'p_s=',p_s
      call MPI_BARRIER(n_cartesian_comm,n_error)
     
      
      do k=1,iTT-2                                    ! f(t)
      F(k)=p_s*(dsin(pi*(k*dt-1.0D0)) + 0.8D0*
     & dsin(2.0D0*pi*(k*dt-1.0D0)) + 0.2D0*dsin(3.0D0*pi*(k*dt-1.0D0)))
      end do

      call MPI_BARRIER(n_cartesian_comm,n_error)
      if(id==id_source) then
      open(35,file='F.txt',form='formatted')
      write(35,*) real(F)
      close(35)
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)
!=====================================================================

      call MPI_BARRIER(n_cartesian_comm,n_error)
      if(id==0) then
      print *,id,''
      print *,id,'Ready'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)
!==================================================================
!========================= Explicit ainside ========================
!==================================================================
     
      if(id==0) then
      print *,''
      print *,'Start'
      print *,''
      
      call cpu_time(start)
      
      end if
 
      do i_time=0,iTT,2


      if(id==id_source) then

       U(i_source-1,j_source,k_source) = 
     & U(i_source-1,j_source,k_source) + F(i_time)*
     & dq1_dx(2*i_source-2,2*j_source,2*k_source)

       U(i_source+1,j_source,k_source) =
     & U(i_source+1,j_source,k_source) - F(i_time)*
     & dq1_dx(2*i_source+2,2*j_source,2*k_source)

       U(i_source,j_source-1,k_source) = 
     & U(i_source,j_source-1,k_source) + F(i_time)*
     & dq2_dx(2*i_source,2*j_source-2,2*k_source)

       U(i_source,j_source+1,k_source) =
     & U(i_source,j_source+1,k_source) - F(i_time)*
     & dq2_dx(2*i_source,2*j_source+2,2*k_source)

       U(i_source,j_source,k_source-1) = 
     & U(i_source,j_source,k_source-1) + F(i_time)*
     & dq3_dx(2*i_source,2*j_source,2*k_source-2)

       U(i_source,j_source,k_source+1) =
     & U(i_source,j_source,k_source+1) - F(i_time)*
     & dq3_dx(2*i_source,2*j_source,2*k_source+2)      


       V(i_source-1,j_source,k_source) = 
     & V(i_source-1,j_source,k_source) + F(i_time)*
     & dq1_dy(2*i_source-2,2*j_source,2*k_source)

       V(i_source+1,j_source,k_source) =
     & V(i_source+1,j_source,k_source) - F(i_time)*
     & dq1_dy(2*i_source+2,2*j_source,2*k_source)

       V(i_source,j_source-1,k_source) = 
     & V(i_source,j_source-1,k_source) + F(i_time)*
     & dq2_dy(2*i_source,2*j_source-2,2*k_source)

       V(i_source,j_source+1,k_source) =
     & V(i_source,j_source+1,k_source) - F(i_time)*
     & dq2_dy(2*i_source,2*j_source+2,2*k_source)

       V(i_source,j_source,k_source-1) = 
     & V(i_source,j_source,k_source-1) + F(i_time)*
     & dq3_dy(2*i_source,2*j_source,2*k_source-2)

       V(i_source,j_source,k_source+1) =
     & V(i_source,j_source,k_source+1) - F(i_time)*
     & dq3_dy(2*i_source,2*j_source,2*k_source+2)


       W(i_source-1,j_source,k_source) = 
     & W(i_source-1,j_source,k_source) + F(i_time)*
     & dq1_dz(2*i_source-2,2*j_source,2*k_source)

       W(i_source+1,j_source,k_source) =
     & W(i_source+1,j_source,k_source) - F(i_time)*
     & dq1_dz(2*i_source+2,2*j_source,2*k_source)

       W(i_source,j_source-1,k_source) = 
     & W(i_source,j_source-1,k_source) + F(i_time)*
     & dq2_dz(2*i_source,2*j_source-2,2*k_source)

       W(i_source,j_source+1,k_source) =
     & W(i_source,j_source+1,k_source) - F(i_time)*
     & dq2_dz(2*i_source,2*j_source+2,2*k_source)

       W(i_source,j_source,k_source-1) = 
     & W(i_source,j_source,k_source-1) + F(i_time)*
     & dq3_dz(2*i_source,2*j_source,2*k_source-2)

       W(i_source,j_source,k_source+1) =
     & W(i_source,j_source,k_source+1) - F(i_time)*
     & dq3_dz(2*i_source,2*j_source,2*k_source+2)

       end if

      call edges(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d,n_cartesian_comm,num_procs,id,id_x,id_y,id_z,
     & N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     & U_down_x,U_up_x,U_down_y,U_up_y,
     & U_N_down_x,U_N_up_x,U_N_down_y,U_N_up_y, 
     & V_down_x,V_up_x,V_down_y,V_up_y,
     & V_N_down_x,V_N_up_x,V_N_down_y,V_N_up_y,
     & W_down_x,W_up_x,W_down_y,W_up_y,
     & W_N_down_x,W_N_up_x,W_N_down_y,W_N_up_y,n_dims)

      call exchange1(UU,VV,WW,
     &N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     &id,id_x,id_y,id_z,num_procs,n_cartesian_comm,n_dims,
     &A_N_down_x,A_N_up_x,A_down_x,A_up_x,
     &A_N_down_y,A_N_up_y,A_down_y,A_up_y,
     &A_N_down_z,A_N_up_z,A_down_z,A_up_z,n_request,n_statuses)

      call ainside(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)

      call MPI_Waitall(12,n_request(1:12),n_statuses(:,1:12),n_error)

      call MPI_BARRIER(n_cartesian_comm,n_error)

      call exchange2(UU,VV,WW,
     &N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     &id,id_x,id_y,id_z,num_procs,n_cartesian_comm,n_dims,
     &A_N_down_x,A_N_up_x,A_down_x,A_up_x,
     &A_N_down_y,A_N_up_y,A_down_y,A_up_y,
     &A_N_down_z,A_N_up_z,A_down_z,A_up_z)
 

      call MPI_BARRIER(n_cartesian_comm,n_error)      
      if(id==0) then
      print *,''
      print *,id,'i_time= ',i_time
      print *,''
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)

      
      
      if(id==id_source) then
       
       UU(i_source-1,j_source,k_source) = 
     & UU(i_source-1,j_source,k_source) + F(i_time+1)*
     & dq1_dx(2*i_source-2,2*j_source,2*k_source)

       UU(i_source+1,j_source,k_source) =
     & UU(i_source+1,j_source,k_source) - F(i_time+1)*
     & dq1_dx(2*i_source+2,2*j_source,2*k_source)

       UU(i_source,j_source-1,k_source) = 
     & UU(i_source,j_source-1,k_source) + F(i_time+1)*
     & dq2_dx(2*i_source,2*j_source-2,2*k_source)

       UU(i_source,j_source+1,k_source) =
     & UU(i_source,j_source+1,k_source) - F(i_time+1)*
     & dq2_dx(2*i_source,2*j_source+2,2*k_source)

       UU(i_source,j_source,k_source-1) = 
     & UU(i_source,j_source,k_source-1) + F(i_time+1)*
     & dq3_dx(2*i_source,2*j_source,2*k_source-2)

       UU(i_source,j_source,k_source+1) =
     & UU(i_source,j_source,k_source+1) - F(i_time+1)*
     & dq3_dx(2*i_source,2*j_source,2*k_source+2)      


       VV(i_source-1,j_source,k_source) = 
     & VV(i_source-1,j_source,k_source) + F(i_time+1)*
     & dq1_dy(2*i_source-2,2*j_source,2*k_source)

       VV(i_source+1,j_source,k_source) =
     & VV(i_source+1,j_source,k_source) - F(i_time+1)*
     & dq1_dy(2*i_source+2,2*j_source,2*k_source)

       VV(i_source,j_source-1,k_source) = 
     & VV(i_source,j_source-1,k_source) + F(i_time+1)*
     & dq2_dy(2*i_source,2*j_source-2,2*k_source)

       VV(i_source,j_source+1,k_source) =
     & VV(i_source,j_source+1,k_source) - F(i_time+1)*
     & dq2_dy(2*i_source,2*j_source+2,2*k_source)

       VV(i_source,j_source,k_source-1) = 
     & VV(i_source,j_source,k_source-1) + F(i_time+1)*
     & dq3_dy(2*i_source,2*j_source,2*k_source-2)

       VV(i_source,j_source,k_source+1) =
     & VV(i_source,j_source,k_source+1) - F(i_time+1)*
     & dq3_dy(2*i_source,2*j_source,2*k_source+2)


       WW(i_source-1,j_source,k_source) = 
     & WW(i_source-1,j_source,k_source) + F(i_time+1)*
     & dq1_dz(2*i_source-2,2*j_source,2*k_source)

       WW(i_source+1,j_source,k_source) =
     & WW(i_source+1,j_source,k_source) - F(i_time+1)*
     & dq1_dz(2*i_source+2,2*j_source,2*k_source)

       WW(i_source,j_source-1,k_source) = 
     & WW(i_source,j_source-1,k_source) + F(i_time+1)*
     & dq2_dz(2*i_source,2*j_source-2,2*k_source)

       WW(i_source,j_source+1,k_source) =
     & WW(i_source,j_source+1,k_source) - F(i_time+1)*
     & dq2_dz(2*i_source,2*j_source+2,2*k_source)

       WW(i_source,j_source,k_source-1) = 
     & WW(i_source,j_source,k_source-1) + F(i_time+1)*
     & dq3_dz(2*i_source,2*j_source,2*k_source-2)

       WW(i_source,j_source,k_source+1) =
     & WW(i_source,j_source,k_source+1) - F(i_time+1)*
     & dq3_dz(2*i_source,2*j_source,2*k_source+2)

      end if

      call edges(U,V,W,UU,VV,WW,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d,n_cartesian_comm,num_procs,id,id_x,id_y,id_z,
     & N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     & U_down_x,U_up_x,U_down_y,U_up_y,
     & U_N_down_x,U_N_up_x,U_N_down_y,U_N_up_y, 
     & V_down_x,V_up_x,V_down_y,V_up_y,
     & V_N_down_x,V_N_up_x,V_N_down_y,V_N_up_y,
     & W_down_x,W_up_x,W_down_y,W_up_y,
     & W_N_down_x,W_N_up_x,W_N_down_y,W_N_up_y,n_dims)

      call exchange1(U,V,W,
     &N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     &id,id_x,id_y,id_z,num_procs,n_cartesian_comm,n_dims,
     &A_N_down_x,A_N_up_x,A_down_x,A_up_x,
     &A_N_down_y,A_N_up_y,A_down_y,A_up_y,
     &A_N_down_z,A_N_up_z,A_down_z,A_up_z,n_request,n_statuses)

      call ainside(U,V,W,UU,VV,WW,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)
 
      call MPI_Waitall(12,n_request(1:12),n_statuses(:,1:12),n_error)

      call MPI_BARRIER(n_cartesian_comm,n_error)

      call exchange2(U,V,W,
     &N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     &id,id_x,id_y,id_z,num_procs,n_cartesian_comm,n_dims,
     &A_N_down_x,A_N_up_x,A_down_x,A_up_x,
     &A_N_down_y,A_N_up_y,A_down_y,A_up_y,
     &A_N_down_z,A_N_up_z,A_down_z,A_up_z)


      call MPI_BARRIER(n_cartesian_comm,n_error)
      if(id==0) then
      print *,''
      print *,id,'i_time= ',i_time+1
      print *,''
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error) 

!=================================== Snapshots =================
!===============================================================    

      end do !i_time   
      
!---------------------------------------------------------------

      do i_time=i_time,iTime,2

      call edges(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d,n_cartesian_comm,num_procs,id,id_x,id_y,id_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     & U_down_x,U_up_x,U_down_y,U_up_y,
     & U_N_down_x,U_N_up_x,U_N_down_y,U_N_up_y, 
     & V_down_x,V_up_x,V_down_y,V_up_y,
     & V_N_down_x,V_N_up_x,V_N_down_y,V_N_up_y,
     & W_down_x,W_up_x,W_down_y,W_up_y,
     & W_N_down_x,W_N_up_x,W_N_down_y,W_N_up_y,n_dims)

      call exchange1(UU,VV,WW,
     &N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     &id,id_x,id_y,id_z,num_procs,n_cartesian_comm,n_dims,
     &A_N_down_x,A_N_up_x,A_down_x,A_up_x,
     &A_N_down_y,A_N_up_y,A_down_y,A_up_y,
     &A_N_down_z,A_N_up_z,A_down_z,A_up_z,n_request,n_statuses)

      call ainside(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)

      call MPI_Waitall(12,n_request(1:12),n_statuses(:,1:12),n_error)

      call MPI_BARRIER(n_cartesian_comm,n_error) 

      call exchange2(UU,VV,WW,
     &N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     &id,id_x,id_y,id_z,num_procs,n_cartesian_comm,n_dims,
     &A_N_down_x,A_N_up_x,A_down_x,A_up_x,
     &A_N_down_y,A_N_up_y,A_down_y,A_up_y,
     &A_N_down_z,A_N_up_z,A_down_z,A_up_z)


      if(id==0) then
      print *,''
      print *,id,'i_time=',i_time
      print *,''
      end if

!      call MPI_BARRIER(n_cartesian_comm,n_error)
 

      call edges(U,V,W,UU,VV,WW,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d,n_cartesian_comm,num_procs,id,id_x,id_y,id_z,
     & N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     & U_down_x,U_up_x,U_down_y,U_up_y,
     & U_N_down_x,U_N_up_x,U_N_down_y,U_N_up_y, 
     & V_down_x,V_up_x,V_down_y,V_up_y,
     & V_N_down_x,V_N_up_x,V_N_down_y,V_N_up_y,
     & W_down_x,W_up_x,W_down_y,W_up_y,
     & W_N_down_x,W_N_up_x,W_N_down_y,W_N_up_y,n_dims)

      call exchange1(U,V,W,
     &N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     &id,id_x,id_y,id_z,num_procs,n_cartesian_comm,n_dims,
     &A_N_down_x,A_N_up_x,A_down_x,A_up_x,
     &A_N_down_y,A_N_up_y,A_down_y,A_up_y,
     &A_N_down_z,A_N_up_z,A_down_z,A_up_z,n_request,n_statuses)

      call ainside(U,V,W,UU,VV,WW,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)
      
      call MPI_Waitall(12,n_request(1:12),n_statuses(:,1:12),n_error)

      call MPI_BARRIER(n_cartesian_comm,n_error) 

      call exchange2(U,V,W,
     &N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     &id,id_x,id_y,id_z,num_procs,n_cartesian_comm,n_dims,
     &A_N_down_x,A_N_up_x,A_down_x,A_up_x,
     &A_N_down_y,A_N_up_y,A_down_y,A_up_y,
     &A_N_down_z,A_N_up_z,A_down_z,A_up_z)

!      call MPI_BARRIER(n_cartesian_comm,n_error) 


      if(id==0) then
      print *,''
      print *,id,'i_time=',i_time+1
      print *,''
      end if

      call MPI_BARRIER(n_cartesian_comm,n_error) 

!=================================== Snapshots =================
!===============================================================    

      end do !i_time   

!------------------------ Sending and writing traces -----------------------
!--------------------------------------------------------------

!===============================================================
!===============================================================
!===============================================================

      call MPI_BARRIER(n_cartesian_comm,n_error)

      if(id==0) then

      call cpu_time(finish)
      print '("Time =",f8.1," seconds")',finish-start

      print *,id,'finish'
      end if
      call MPI_BARRIER(n_cartesian_comm,n_error)      

      deallocate(U,UU,V,VV,W,WW,alambda,amu,Rho,
     &dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     &trace_U,trace_V,trace_W)

!________________________________________________________________________
!============================================================================================================================
      
!============================================================================================================================      

      call MPI_BARRIER(n_cartesian_comm,n_error)
      
      call MPI_Comm_free(n_cartesian_comm,n_error)
      call MPI_Finalize(n_error)
      
      !pause
      stop
      end 
 

     

      !Essential
      subroutine ainside(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)

      include 'mpif.h'

      integer i,j,k,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & N_ud_x, N_ud_y

      double precision rd,rdd,d 
      double precision dSxx_dx,dSxy_dy,dSxz_dz,
     &                 dSxy_dx,dSyy_dy,dSyz_dz,
     &                 dSxz_dx,dSyz_dy,dSzz_dz,
     & dc1Ux_dq1,dc1Ux_dq2,dc1Ux_dq3,dc1Vy_dq1,dc1Vy_dq2,dc1Vy_dq3,
     & dc1Wz_dq1,dc1Wz_dq2,dc1Wz_dq3,
     & dcUy_dq1,dcUy_dq2,dcUy_dq3,dcVx_dq1,dcVx_dq2,dcVx_dq3,
     & dcUz_dq1,dcUz_dq2,dcUz_dq3,dcWx_dq1,dcWx_dq2,dcWx_dq3,
     & dc2Ux_dq1,dc2Ux_dq2,dc2Ux_dq3,dc2Vy_dq1,dc2Vy_dq2,dc2Vy_dq3,
     & dc2Wz_dq1,dc2Wz_dq2,dc2Wz_dq3,
     & dcVz_dq1,dcVz_dq2,dcVz_dq3,dcWy_dq1,dcWy_dq2,dcWy_dq3,
     & dc3Ux_dq1,dc3Ux_dq2,dc3Ux_dq3,dc3Vy_dq1,dcVy_dq2,dcVy_q3,
     & dc3Wz_dq1,dc3Wz_dq2,dc3Wz_dq3

      double precision
     & UU(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & VV(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & WW(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  U(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  V(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  W(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     
 
     & alambda
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & amu
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & ttoRhoJ
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),     

     & dq1_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),

     & A1(N_down_x:N_up_x,N_down_y:N_up_y),
     & A2(N_down_x:N_up_x,N_down_y:N_up_y),
     & A3(N_down_x:N_up_x,N_down_y:N_up_y),
     & B1(N_down_x:N_up_x,N_down_y:N_up_y),
     & B2(N_down_x:N_up_x,N_down_y:N_up_y),
     & B3(N_down_x:N_up_x,N_down_y:N_up_y),
     & C1(N_down_x:N_up_x,N_down_y:N_up_y),
     & C2(N_down_x:N_up_x,N_down_y:N_up_y),
     & C3(N_down_x:N_up_x,N_down_y:N_up_y),
     
     & R1,R2,R3,
     & R1_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_6(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_6(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_6(N_down_x:N_up_x,N_down_y:N_up_y),
     
     &dz_dq1_surf(N_down_x:N_up_x,N_down_y:N_up_y),
     &dz_dq2_surf(N_down_x:N_up_x,N_down_y:N_up_y), 

     & Znam1,Znam2,Znam3,Znam4,
     & dU_dq1,dU_dq2,dV_dq1,dV_dq2,dW_dq1,dW_dq2,
     & dU_dq3,dV_dq3,dW_dq3

!--- For surface sending -----------------------------------
     & U_down_x(N_down_y:N_up_y),U_up_x(N_down_y:N_up_y),
     & U_down_y(N_down_x:N_up_x),U_up_y(N_down_x:N_up_x),
     & U_N_down_x(N_down_y:N_up_y),U_N_up_x(N_down_y:N_up_y),
     & U_N_down_y(N_down_x:N_up_x),U_N_up_y(N_down_x:N_up_x),
 
     & V_down_x(N_down_y:N_up_y),V_up_x(N_down_y:N_up_y),
     & V_down_y(N_down_x:N_up_x),V_up_y(N_down_x:N_up_x),
     & V_N_down_x(N_down_y:N_up_y),V_N_up_x(N_down_y:N_up_y),
     & V_N_down_y(N_down_x:N_up_x),V_N_up_y(N_down_x:N_up_x),

     & W_down_x(N_down_y:N_up_y),W_up_x(N_down_y:N_up_y),
     & W_down_y(N_down_x:N_up_x),W_up_y(N_down_x:N_up_x),
     & W_N_down_x(N_down_y:N_up_y),W_N_up_x(N_down_y:N_up_y),
     & W_N_down_y(N_down_x:N_up_x),W_N_up_y(N_down_x:N_up_x)



!=================================== Inside medium =====================================
      do k=N_down_z+1,N_up_z-1
      do j=N_down_y+1,N_up_y-1
      do i=N_down_x+1,N_up_x-1


! dSxx_dx ------------------------------------------------------------
      dc1Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+                        !
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +             
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*               
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*               
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc1Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*               
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+                        
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*               
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc1Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*               
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+                        
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc1Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+                                     
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*                                  !
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -              
     &              (alambda(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc1Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*                                  
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+                                     
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*                                  
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc1Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*                                  
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*                                  !!!                                  
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+                                     
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc1Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+                                     
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*                                  
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*                                  !
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc1Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*                                  
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+                                     
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*                                  !!
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc1Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*									 
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*                                  
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSxx_dx=
     & dq1_dx(2*i,2*j,2*k)*(dc1Ux_dq1+dc1Vy_dq1+dc1Wz_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dc1Ux_dq2+dc1Vy_dq2+dc1Wz_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dc1Ux_dq3+dc1Vy_dq3+dc1Wz_dq3)
!-------------------------------------------------------------
            
! dSxy_dy ----------------------------------------------------
      dcUy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+                                      
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*                                      !!
     &  dq2_dy(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUy_dq2 =                                                         
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+                                         !
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcVx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+                                         !!
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*                                      !
     &  dq1_dx(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+                                         
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )
 
   
      dSxy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSxz_dz ----------------------------------------------------
      dcUz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*                                      !!!
     &  dq3_dz(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+                                         !
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcWx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+                                         !!!
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*                                      !
     &  dq1_dx(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSxz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------
!-------------------------------------------------------------

! dSxy_dx ----------------------------------------------------
      dSxy_dx=
     & dq1_dx(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSyy_dy ----------------------------------------------------
      dc2Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+                                     !!!
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc2Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*                                  !!
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc2Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc2Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc2Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+                        !!
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc2Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc2Wz_dq1 = dc1Wz_dq1 
     
      dc2Wz_dq2 = dc1Wz_dq2

      dc2Wz_dq3 = dc1Wz_dq3


      dSyy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dc2Ux_dq1+dc2Vy_dq1+dc2Wz_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dc2Ux_dq2+dc2Vy_dq2+dc2Wz_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dc2Ux_dq3+dc2Vy_dq3+dc2Wz_dq3) 
!------------------------------------------------------------- 

! dSyz_dz ----------------------------------------------------
      dcVz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*                                      !!!
     &  dq3_dz(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+                                         !!
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )


      dcWy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+                                         !!!
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -              
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*                                      !!
     &  dq2_dy(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSyz_dz =
     & dq1_dz(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!------------------------------------------------------------- 
!-------------------------------------------------------------

! dSxz_dx ----------------------------------------------------
      dSxz_dx = 
     & dq1_dx(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------

! dSyz_dy ----------------------------------------------------
      dSyz_dy =
     & dq1_dy(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!-------------------------------------------------------------

! dSzz_dz ----------------------------------------------------
      dc3Ux_dq1 = dc2Ux_dq1

      dc3Ux_dq2 = dc2Ux_dq2

      dc3Ux_dq3 = dc2Ux_dq3


      dc3Vy_dq1 = dc1Vy_dq1

      dc3Vy_dq1 = dc1Vy_dq2

      dc3Vy_dq3 = dc1Vy_dq3 


      dc3Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc3Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc3Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+                        !!!
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSzz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dc3Ux_dq1+dc3Vy_dq1+dc3Wz_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dc3Ux_dq2+dc3Vy_dq2+dc3Wz_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dc3Ux_dq3+dc3Vy_dq3+dc3Wz_dq3) 
!-------------------------------------------------------------

!-------------------------------------------------------------
   
      UU(i,j,k)=2.0d0*U(i,j,k)+ttoRhoJ(i,j,k)*(dSxx_dx+dSxy_dy+dSxz_dz)-
     &               UU(i,j,k)
      VV(i,j,k)=2.0d0*V(i,j,k)+ttoRhoJ(i,j,k)*(dSxy_dx+dSyy_dy+dSyz_dz)-
     &               VV(i,j,k)
      WW(i,j,k)=2.0d0*W(i,j,k)+ttoRhoJ(i,j,k)*(dSxz_dx+dSyz_dy+dSzz_dz)-
     &               WW(i,j,k)

!-------------------------------------------------------------
      end do
      end do
      end do
     
      
     

!=====================================================================================================================      
         
      return
      end subroutine 


      subroutine edges(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d,n_cartesian_comm,num_procs,id,id_x,id_y,id_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     & U_down_x,U_up_x,U_down_y,U_up_y,
     & U_N_down_x,U_N_up_x,U_N_down_y,U_N_up_y, 
     & V_down_x,V_up_x,V_down_y,V_up_y,
     & V_N_down_x,V_N_up_x,V_N_down_y,V_N_up_y,
     & W_down_x,W_up_x,W_down_y,W_up_y,
     & W_N_down_x,W_N_up_x,W_N_down_y,W_N_up_y,n_dims)

      include 'mpif.h'

      EXTERNAL MPI_BARRIER,MPI_Send,MPI_Recv

      integer i,j,k, n_dims(3),id,id_x,id_y,id_z 
     & n_cartesian_comm,num_procs,n_error,n_status(MPI_STATUS_SIZE),
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & N_ud_x, N_ud_y
 
      integer N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z

      double precision rd,rdd,d 
      double precision dSxx_dx,dSxy_dy,dSxz_dz,
     &                 dSxy_dx,dSyy_dy,dSyz_dz,
     &                 dSxz_dx,dSyz_dy,dSzz_dz,
     & dc1Ux_dq1,dc1Ux_dq2,dc1Ux_dq3,dc1Vy_dq1,dc1Vy_dq2,dc1Vy_dq3,
     & dc1Wz_dq1,dc1Wz_dq2,dc1Wz_dq3,
     & dcUy_dq1,dcUy_dq2,dcUy_dq3,dcVx_dq1,dcVx_dq2,dcVx_dq3,
     & dcUz_dq1,dcUz_dq2,dcUz_dq3,dcWx_dq1,dcWx_dq2,dcWx_dq3,
     & dc2Ux_dq1,dc2Ux_dq2,dc2Ux_dq3,dc2Vy_dq1,dc2Vy_dq2,dc2Vy_dq3,
     & dc2Wz_dq1,dc2Wz_dq2,dc2Wz_dq3,
     & dcVz_dq1,dcVz_dq2,dcVz_dq3,dcWy_dq1,dcWy_dq2,dcWy_dq3,
     & dc3Ux_dq1,dc3Ux_dq2,dc3Ux_dq3,dc3Vy_dq1,dcVy_dq2,dcVy_q3,
     & dc3Wz_dq1,dc3Wz_dq2,dc3Wz_dq3

      double precision
     & UU(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & VV(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & WW(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  U(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  V(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  W(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     
 
     & alambda
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & amu
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & ttoRhoJ
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),     

     & dq1_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),

     & A1(N_down_x:N_up_x,N_down_y:N_up_y),
     & A2(N_down_x:N_up_x,N_down_y:N_up_y),
     & A3(N_down_x:N_up_x,N_down_y:N_up_y),
     & B1(N_down_x:N_up_x,N_down_y:N_up_y),
     & B2(N_down_x:N_up_x,N_down_y:N_up_y),
     & B3(N_down_x:N_up_x,N_down_y:N_up_y),
     & C1(N_down_x:N_up_x,N_down_y:N_up_y),
     & C2(N_down_x:N_up_x,N_down_y:N_up_y),
     & C3(N_down_x:N_up_x,N_down_y:N_up_y),
     
     & R1,R2,R3,
     & R1_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_6(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_6(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_6(N_down_x:N_up_x,N_down_y:N_up_y),
     
     &dz_dq1_surf(N_down_x:N_up_x,N_down_y:N_up_y),
     &dz_dq2_surf(N_down_x:N_up_x,N_down_y:N_up_y), 

     & Znam1,Znam2,Znam3,Znam4,
     & dU_dq1,dU_dq2,dV_dq1,dV_dq2,dW_dq1,dW_dq2,
     & dU_dq3,dV_dq3,dW_dq3,

!--- For surface sending -----------------------------------
     & U_down_x(N_down_y:N_up_y),U_up_x(N_down_y:N_up_y),
     & U_down_y(N_down_x:N_up_x),U_up_y(N_down_x:N_up_x),
     & U_N_down_x(N_down_y:N_up_y),U_N_up_x(N_down_y:N_up_y),
     & U_N_down_y(N_down_x:N_up_x),U_N_up_y(N_down_x:N_up_x),
 
     & V_down_x(N_down_y:N_up_y),V_up_x(N_down_y:N_up_y),
     & V_down_y(N_down_x:N_up_x),V_up_y(N_down_x:N_up_x),
     & V_N_down_x(N_down_y:N_up_y),V_N_up_x(N_down_y:N_up_y),
     & V_N_down_y(N_down_x:N_up_x),V_N_up_y(N_down_x:N_up_x),

     & W_down_x(N_down_y:N_up_y),W_up_x(N_down_y:N_up_y),
     & W_down_y(N_down_x:N_up_x),W_up_y(N_down_x:N_up_x),
     & W_N_down_x(N_down_y:N_up_y),W_N_up_x(N_down_y:N_up_y),
     & W_N_down_y(N_down_x:N_up_x),W_N_up_y(N_down_x:N_up_x)

!=================================== Free surface =====================================
      if(id_z==0) then
      
      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x
      Znam1=(B3(i,j)*A1(i,j)-A3(i,j)*B1(i,j))*
     &      (C2(i,j)*A1(i,j)-A2(i,j)*C1(i,j)) -
     &      (C3(i,j)*A1(i,j)-A3(i,j)*C1(i,j))*
     &      (B2(i,j)*A1(i,j)-A2(i,j)*B1(i,j))

      Znam2= B2(i,j)*A1(i,j)-A2(i,j)*B1(i,j)

      Znam3= B2(i,j)*C3(i,j)-C2(i,j)*B3(i,j)

      Znam4= A1(i,j)*C3(i,j)-C1(i,j)*A3(i,j)


      dU_dq1= 0.5d0*rd*(1.5d0*(U(i+1,j,1)-U(i-1,j,1)) -
     &                  0.5d0*(U(i+1,j,2)-U(i-1,j,2)))
      dU_dq2= 0.5d0*rd*(1.5d0*(U(i,j+1,1)-U(i,j-1,1)) -
     &                  0.5d0*(U(i,j+1,2)-U(i,j-1,2)))

      dV_dq1= 0.5d0*rd*(1.5d0*(V(i+1,j,1)-V(i-1,j,1)) -
     &                  0.5d0*(V(i+1,j,2)-V(i-1,j,2)))
      dV_dq2= 0.5d0*rd*(1.5d0*(V(i,j+1,1)-V(i,j-1,1)) -
     &                  0.5d0*(V(i,j+1,2)-V(i,j-1,2)))

      dW_dq1= 0.5d0*rd*(1.5d0*(W(i+1,j,1)-W(i-1,j,1)) -
     &                  0.5d0*(W(i+1,j,2)-W(i-1,j,2)))
      dW_dq2= 0.5d0*rd*(1.5d0*(W(i,j+1,1)-W(i,j-1,1)) -
     &                  0.5d0*(W(i,j+1,2)-W(i,j-1,2)))
      
      R1= R1_1(i,j)*dU_dq1+R1_2(i,j)*dU_dq2+ 
     &    R1_3(i,j)*dV_dq1+R1_4(i,j)*dV_dq2+
     &    R1_5(i,j)*dW_dq1+R1_6(i,j)*dW_dq2

      R2= R2_1(i,j)*dU_dq1+R2_2(i,j)*dU_dq2+
     &    R2_3(i,j)*dV_dq1+R2_4(i,j)*dV_dq2+
     &    R2_5(i,j)*dW_dq1+R2_6(i,j)*dW_dq2

      R3= R3_1(i,j)*dU_dq1+R3_2(i,j)*dU_dq2+
     &    R3_3(i,j)*dV_dq1+R3_4(i,j)*dV_dq2+
     &    R3_5(i,j)*dW_dq1+R3_6(i,j)*dW_dq2



      if(dabs(dz_dq1_surf(i,j))>=0.000000001d0 .and. 
     &   dabs(dz_dq2_surf(i,j))>=0.000000001d0) then

      dW_dq3=-(1.0d0/Znam1)*((R2*A1(i,j)-R1*B1(i,j))*
     &                       (C2(i,j)*A1(i,j)-A2(i,j)*C1(i,j)) -
     &                       (R3*A1(i,j)-R1*C1(i,j))*
     &                       (B2(i,j)*A1(i,j)-A2(i,j)*B1(i,j)))
     
      dV_dq3= (1.0d0/Znam2)*
     &                      (R1*B1(i,j)-R2*A1(i,j)-
     &                 (B3(i,j)*A1(i,j)-A3(i,j)*B1(i,j))*dW_dq3)

      dU_dq3= -(1.0d0/A1(i,j))*(R1+A2(i,j)*dV_dq3+A3(i,j)*dW_dq3)
      
      end if

      
      if(dabs(dz_dq1_surf(i,j))<0.000000001d0 .and. 
     &   dabs(dz_dq2_surf(i,j))>=0.000000001d0) then

      dW_dq3= -(1.0d0/(-Znam3))*(R2*C2(i,j)-R3*B2(i,j))
     
      dV_dq3= -(1.0d0/Znam3)*(R2*C3(i,j)-R3*B3(i,j))

      dU_dq3= -R1/A1(i,j)

      end if


      if(dabs(dz_dq1_surf(i,j))>=0.000000001d0 .and. 
     &   dabs(dz_dq2_surf(i,j))<0.000000001d0) then

      dW_dq3= -(1.0d0/(-Znam4))*(R1*C1(i,j)-R3*A1(i,j))
     
      dV_dq3= -R2/B2(i,j)

      dU_dq3= -(1.0d0/Znam4)*(R1*C3(i,j)-R3*A3(i,j))

      end if

      
      if(dabs(dz_dq1_surf(i,j))<0.000000001d0 .and. 
     &   dabs(dz_dq2_surf(i,j))<0.000000001d0) then

      dW_dq3= -R3/C3(i,j)
     
      dV_dq3= -R2/B2(i,j)

      dU_dq3= -R1/A1(i,j)

      end if


      U(i,j,0)=U(i,j,1)-d*dU_dq3
      V(i,j,0)=V(i,j,1)-d*dV_dq3
      W(i,j,0)=W(i,j,1)-d*dW_dq3
      end do
      end do

      
!      print *,id,'U calc surf',abs(maxval(real(U))),maxloc(real(U))
!      print *,id,'V calc surf',abs(maxval(real(V))),maxloc(real(V))
!      print *,id,'W calc surf',abs(maxval(real(W))),maxloc(real(W))

!------------------
! MPI_Send, MPI_Recv U,V,W on surface
      U_N_down_x=U(N_down_x,N_down_y:N_up_y,0)
      U_N_up_x=U(N_up_x,N_down_y:N_up_y,0)
      U_N_down_y=U(N_down_x:N_up_x,N_down_y,0)
      U_N_up_y=U(N_down_x:N_up_x,N_up_y,0)

      V_N_down_x=V(N_down_x,N_down_y:N_up_y,0)
      V_N_up_x=V(N_up_x,N_down_y:N_up_y,0)
      V_N_down_y=V(N_down_x:N_up_x,N_down_y,0)
      V_N_up_y=V(N_down_x:N_up_x,N_up_y,0)
 
      W_N_down_x=W(N_down_x,N_down_y:N_up_y,0)
      W_N_up_x=W(N_up_x,N_down_y:N_up_y,0)
      W_N_down_y=W(N_down_x:N_up_x,N_down_y,0)
      W_N_up_y=W(N_down_x:N_up_x,N_up_y,0)

!      print *,'x',sizeof(U_N_down_x),sizeof(U_up_x)      
!      print *,'y',sizeof(U_N_down_y),sizeof(U_up_y)
 
      N_ud_x=N_up_x-N_down_x+1
      N_ud_y=N_up_y-N_down_y+1

      call MPI_Send(U_N_down_x,N_ud_y,MPI_DOUBLE_PRECISION,N_bot_x,
     &133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(U_up_x,N_ud_y,MPI_DOUBLE_PRECISION,N_top_x,
     &133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'0'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(U_N_up_x,N_ud_y,MPI_DOUBLE_PRECISION,N_top_x,
     &1+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(U_down_x,N_ud_y,MPI_DOUBLE_PRECISION,N_bot_x,
     &1+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'1'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(U_N_down_y,N_ud_x,MPI_DOUBLE_PRECISION,N_bot_y,
     &2+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(U_up_y,N_ud_x,MPI_DOUBLE_PRECISION,N_top_y,
     &2+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'2'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(U_N_up_y,N_ud_x,MPI_DOUBLE_PRECISION,N_top_y,
     &3+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(U_down_y,N_ud_x,MPI_DOUBLE_PRECISION,N_bot_y,
     &3+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'3'
!      call MPI_BARRIER(n_cartesian_comm,n_error)


      

      call MPI_Send(V_N_down_x,N_ud_y,MPI_DOUBLE_PRECISION,N_bot_x,
     &4+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(V_up_x,N_ud_y,MPI_DOUBLE_PRECISION,N_top_x,
     &4+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'4'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(V_N_up_x,N_ud_y,MPI_DOUBLE_PRECISION,N_top_x,
     &5+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(V_down_x,N_ud_y,MPI_DOUBLE_PRECISION,N_bot_x,
     &5+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'5'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(V_N_down_y,N_ud_x,MPI_DOUBLE_PRECISION,N_bot_y,
     &6+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(V_up_y,N_ud_x,MPI_DOUBLE_PRECISION,N_top_y,
     &6+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'6'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(V_N_up_y,N_ud_x,MPI_DOUBLE_PRECISION,N_top_y,
     &7+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(V_down_y,N_ud_x,MPI_DOUBLE_PRECISION,N_bot_y,
     &7+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'7'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

   
      call MPI_Send(W_N_down_x,N_ud_y,MPI_DOUBLE_PRECISION,N_bot_x,
     &8+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(W_up_x,N_ud_y,MPI_DOUBLE_PRECISION,N_top_x,
     &8+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'8'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(W_N_up_x,N_ud_y,MPI_DOUBLE_PRECISION,N_top_x,
     &9+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(W_down_x,N_ud_y,MPI_DOUBLE_PRECISION,N_bot_x,
     &9+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'9'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(W_N_down_y,N_ud_x,MPI_DOUBLE_PRECISION,N_bot_y,
     &10+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(W_up_y,N_ud_x,MPI_DOUBLE_PRECISION,N_top_y,
     &10+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'10'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(W_N_up_y,N_ud_x,MPI_DOUBLE_PRECISION,N_top_y,
     &11+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(W_down_y,N_ud_x,MPI_DOUBLE_PRECISION,N_bot_y,
     &11+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'11'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      if(id_x>0) then
      U(N_down_x-1,N_down_y:N_up_y,0)=U_down_x
      V(N_down_x-1,N_down_y:N_up_y,0)=V_down_x
      W(N_down_x-1,N_down_y:N_up_y,0)=W_down_x
      end if

      if(id_y>0) then
      U(N_down_x:N_up_x,N_down_y-1,0)=U_down_y
      V(N_down_x:N_up_x,N_down_y-1,0)=V_down_y
      W(N_down_x:N_up_x,N_down_y-1,0)=W_down_y
      end if

      if(id_x<n_dims(1)-1) then
      U(N_up_x+1,N_down_y:N_up_y,0)=U_up_x
      V(N_up_x+1,N_down_y:N_up_y,0)=V_up_x
      W(N_up_x+1,N_down_y:N_up_y,0)=W_up_x
      end if

      if(id_y<n_dims(2)-1) then
      U(N_down_x:N_up_x,N_up_y+1,0)=U_up_y
      V(N_down_x:N_up_x,N_up_y+1,0)=V_up_y
      W(N_down_x:N_up_x,N_up_y+1,0)=W_up_y
      end if

!      call MPI_BARRIER(n_cartesian_comm,n_error)


      call MPI_Send(U(N_down_x-1,N_up_y,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,12+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(U(N_down_x-1,N_down_y-1,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,12+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'12'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(U(N_down_x-1,N_down_y,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,13+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(U(N_down_x-1,N_up_y+1,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,13+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'13'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(U(N_up_x+1,N_up_y,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,14+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(U(N_up_x+1,N_down_y-1,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,14+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'14'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(U(N_up_x+1,N_down_y,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,15+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(U(N_up_x+1,N_up_y+1,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,15+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'15'
!      call MPI_BARRIER(n_cartesian_comm,n_error)


      call MPI_Send(V(N_down_x-1,N_up_y,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,16+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(V(N_down_x-1,N_down_y-1,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,16+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'16'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(V(N_down_x-1,N_down_y,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,17+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(V(N_down_x-1,N_up_y+1,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,17+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'17'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(V(N_up_x+1,N_up_y,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,18+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(V(N_up_x+1,N_down_y-1,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,18+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'18'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(V(N_up_x+1,N_down_y,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,19+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(V(N_up_x+1,N_up_y+1,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,19+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'19'
!      call MPI_BARRIER(n_cartesian_comm,n_error)


      call MPI_Send(W(N_down_x-1,N_up_y,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,20+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(W(N_down_x-1,N_down_y-1,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,20+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'20'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(W(N_down_x-1,N_down_y,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,21+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(W(N_down_x-1,N_up_y+1,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,21+133*num_procs,n_cartesian_comm,n_status,n_error)

!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'21'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(W(N_up_x+1,N_up_y,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,22+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(W(N_up_x+1,N_down_y-1,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,22+133*num_procs,n_cartesian_comm,n_status,n_error)      
            
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'22'
!      call MPI_BARRIER(n_cartesian_comm,n_error)

      call MPI_Send(W(N_up_x+1,N_down_y,0),1,MPI_DOUBLE_PRECISION,
     &N_bot_y,23+133*num_procs,n_cartesian_comm,n_error)     
      call MPI_Recv(W(N_up_x+1,N_up_y+1,0),1,MPI_DOUBLE_PRECISION,
     &N_top_y,23+133*num_procs,n_cartesian_comm,n_status,n_error)
  
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'23'
!      call MPI_BARRIER(n_cartesian_comm,n_error)


!------------------

      end if
!=======================================================================================
!      call MPI_BARRIER(n_cartesian_comm,n_error)
!      print *,id,'U send recv',abs(maxval(real(UU))),maxloc(real(UU))
!      print *,id,'V send recv',abs(maxval(real(VV))),maxloc(real(VV))
!      print *,id,'W send recv',abs(maxval(real(WW))),maxloc(real(WW))
!      call MPI_BARRIER(n_cartesian_comm,n_error)
 
!=================================== Edges =====================================
      k=N_down_z
      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x


! dSxx_dx ------------------------------------------------------------
      dc1Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +             
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc1Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc1Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc1Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc1Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc1Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc1Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc1Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc1Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSxx_dx=
     & dq1_dx(2*i,2*j,2*k)*(dc1Ux_dq1+dc1Vy_dq1+dc1Wz_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dc1Ux_dq2+dc1Vy_dq2+dc1Wz_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dc1Ux_dq3+dc1Vy_dq3+dc1Wz_dq3)
!-------------------------------------------------------------
       
! dSxy_dy ----------------------------------------------------
      dcUy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcVx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )
 
   
      dSxy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSxz_dz ----------------------------------------------------
      dcUz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcWx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSxz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------
!-------------------------------------------------------------

! dSxy_dx ----------------------------------------------------
      dSxy_dx=
     & dq1_dx(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dCUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSyy_dy ----------------------------------------------------
      dc2Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc2Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc2Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc2Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc2Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc2Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc2Wz_dq1 = dc1Wz_dq1 
     
      dc2Wz_dq2 = dc1Wz_dq2

      dc2Wz_dq3 = dc1Wz_dq3


      dSyy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dc2Ux_dq1+dc2Vy_dq1+dc2Wz_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dc2Ux_dq2+dc2Vy_dq2+dc2Wz_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dc2Ux_dq3+dc2Vy_dq3+dc2Wz_dq3) 
!------------------------------------------------------------- 

! dSyz_dz ----------------------------------------------------
      dcVz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )


      dcWy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSyz_dz =
     & dq1_dz(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!------------------------------------------------------------- 
!-------------------------------------------------------------

! dSxz_dx ----------------------------------------------------
      dSxz_dx = 
     & dq1_dx(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------

! dSyz_dy ----------------------------------------------------
      dSyz_dy =
     & dq1_dy(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!-------------------------------------------------------------

! dSzz_dz ----------------------------------------------------
      dc3Ux_dq1 = dc2Ux_dq1

      dc3Ux_dq2 = dc2Ux_dq2

      dc3Ux_dq3 = dc2Ux_dq3


      dc3Vy_dq1 = dc1Vy_dq1

      dc3Vy_dq1 = dc1Vy_dq2

      dc3Vy_dq3 = dc1Vy_dq3 


      dc3Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc3Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc3Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSzz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dc3Ux_dq1+dc3Vy_dq1+dc3Wz_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dc3Ux_dq2+dc3Vy_dq2+dc3Wz_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dc3Ux_dq3+dc3Vy_dq3+dc3Wz_dq3) 
!-------------------------------------------------------------
 !     call MPI_BARRIER(n_cartesian_comm,n_error)
 !      if(i==34 .and. j==1) then
 !      print *,'N_down_z dSxx_dx=',dSxx_dx,i,j
 !      print *,'N_down_z dSxy_dy=',dSxy_dy,i,j
 !      print *,'N_down_z dSxz_dz=',dSxz_dz,i,j
 !      print *,'N_down_z dSxy_dx=',dSxy_dx,i,j
 !      print *,'N_down_z dSyy_dy=',dSyy_dy,i,j
 !      print *,'N_down_z dSxz_dx=',dSxz_dx,i,j
 !      print *,'N_down_z dSyz_dy=',dSyz_dy,i,j
 !      print *,'N_down_z dSzz_dz=',dSzz_dz,i,j
 !      end if
 !      call MPI_BARRIER(n_cartesian_comm,n_error)
!-------------------------------------------------------------
   
      UU(i,j,k)=2.0d0*U(i,j,k)+ttoRhoJ(i,j,k)*(dSxx_dx+dSxy_dy+dSxz_dz)-
     &               UU(i,j,k)
      VV(i,j,k)=2.0d0*V(i,j,k)+ttoRhoJ(i,j,k)*(dSxy_dx+dSyy_dy+dSyz_dz)-
     &               VV(i,j,k)
      WW(i,j,k)=2.0d0*W(i,j,k)+ttoRhoJ(i,j,k)*(dSxz_dx+dSyz_dy+dSzz_dz)-
     &               WW(i,j,k)

!-------------------------------------------------------------
      end do
      end do





      k=N_up_z
      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x


! dSxx_dx ------------------------------------------------------------
      dc1Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +             
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc1Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc1Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc1Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc1Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc1Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc1Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc1Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc1Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSxx_dx=
     & dq1_dx(2*i,2*j,2*k)*(dc1Ux_dq1+dc1Vy_dq1+dc1Wz_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dc1Ux_dq2+dc1Vy_dq2+dc1Wz_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dc1Ux_dq3+dc1Vy_dq3+dc1Wz_dq3)
!-------------------------------------------------------------
       
! dSxy_dy ----------------------------------------------------
      dcUy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcVx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )
 
   
      dSxy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSxz_dz ----------------------------------------------------
      dcUz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcWx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSxz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------
!-------------------------------------------------------------

! dSxy_dx ----------------------------------------------------
      dSxy_dx=
     & dq1_dx(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dCUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSyy_dy ----------------------------------------------------
      dc2Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc2Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc2Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc2Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc2Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc2Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc2Wz_dq1 = dc1Wz_dq1 
     
      dc2Wz_dq2 = dc1Wz_dq2

      dc2Wz_dq3 = dc1Wz_dq3


      dSyy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dc2Ux_dq1+dc2Vy_dq1+dc2Wz_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dc2Ux_dq2+dc2Vy_dq2+dc2Wz_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dc2Ux_dq3+dc2Vy_dq3+dc2Wz_dq3) 
!------------------------------------------------------------- 

! dSyz_dz ----------------------------------------------------
      dcVz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )


      dcWy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSyz_dz =
     & dq1_dz(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!------------------------------------------------------------- 
!-------------------------------------------------------------

! dSxz_dx ----------------------------------------------------
      dSxz_dx = 
     & dq1_dx(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------

! dSyz_dy ----------------------------------------------------
      dSyz_dy =
     & dq1_dy(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!-------------------------------------------------------------

! dSzz_dz ----------------------------------------------------
      dc3Ux_dq1 = dc2Ux_dq1

      dc3Ux_dq2 = dc2Ux_dq2

      dc3Ux_dq3 = dc2Ux_dq3


      dc3Vy_dq1 = dc1Vy_dq1

      dc3Vy_dq1 = dc1Vy_dq2

      dc3Vy_dq3 = dc1Vy_dq3 


      dc3Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc3Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc3Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSzz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dc3Ux_dq1+dc3Vy_dq1+dc3Wz_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dc3Ux_dq2+dc3Vy_dq2+dc3Wz_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dc3Ux_dq3+dc3Vy_dq3+dc3Wz_dq3) 
!-------------------------------------------------------------
 !     call MPI_BARRIER(n_cartesian_comm,n_error)
 !      if(i==34 .and. j==1) then
 !      print *,'N_down_z dSxx_dx=',dSxx_dx,i,j
 !      print *,'N_down_z dSxy_dy=',dSxy_dy,i,j
 !      print *,'N_down_z dSxz_dz=',dSxz_dz,i,j
 !      print *,'N_down_z dSxy_dx=',dSxy_dx,i,j
 !      print *,'N_down_z dSyy_dy=',dSyy_dy,i,j
 !      print *,'N_down_z dSxz_dx=',dSxz_dx,i,j
 !      print *,'N_down_z dSyz_dy=',dSyz_dy,i,j
 !      print *,'N_down_z dSzz_dz=',dSzz_dz,i,j
 !      end if
 !      call MPI_BARRIER(n_cartesian_comm,n_error)
!-------------------------------------------------------------
   
      UU(i,j,k)=2.0d0*U(i,j,k)+ttoRhoJ(i,j,k)*(dSxx_dx+dSxy_dy+dSxz_dz)-
     &               UU(i,j,k)
      VV(i,j,k)=2.0d0*V(i,j,k)+ttoRhoJ(i,j,k)*(dSxy_dx+dSyy_dy+dSyz_dz)-
     &               VV(i,j,k)
      WW(i,j,k)=2.0d0*W(i,j,k)+ttoRhoJ(i,j,k)*(dSxz_dx+dSyz_dy+dSzz_dz)-
     &               WW(i,j,k)

!-------------------------------------------------------------
      end do
      end do





      j=N_down_y
      do k=N_down_z+1,N_up_z-1
      do i=N_down_x,N_up_x


! dSxx_dx ------------------------------------------------------------
      dc1Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +             
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc1Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc1Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc1Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc1Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc1Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc1Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc1Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc1Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSxx_dx=
     & dq1_dx(2*i,2*j,2*k)*(dc1Ux_dq1+dc1Vy_dq1+dc1Wz_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dc1Ux_dq2+dc1Vy_dq2+dc1Wz_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dc1Ux_dq3+dc1Vy_dq3+dc1Wz_dq3)
!-------------------------------------------------------------

! dSxy_dy ----------------------------------------------------
      dcUy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcVx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )
 
   
      dSxy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSxz_dz ----------------------------------------------------
      dcUz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcWx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSxz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------
!-------------------------------------------------------------

! dSxy_dx ----------------------------------------------------
      dSxy_dx=
     & dq1_dx(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSyy_dy ----------------------------------------------------
      dc2Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc2Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc2Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc2Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc2Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc2Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc2Wz_dq1 = dc1Wz_dq1 
     
      dc2Wz_dq2 = dc1Wz_dq2

      dc2Wz_dq3 = dc1Wz_dq3



      dSyy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dc2Ux_dq1+dc2Vy_dq1+dc2Wz_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dc2Ux_dq2+dc2Vy_dq2+dc2Wz_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dc2Ux_dq3+dc2Vy_dq3+dc2Wz_dq3) 
!------------------------------------------------------------- 

! dSyz_dz ----------------------------------------------------
      dcVz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )


      dcWy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSyz_dz =
     & dq1_dz(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!------------------------------------------------------------- 
!-------------------------------------------------------------

! dSxz_dx ----------------------------------------------------
      dSxz_dx = 
     & dq1_dx(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------

! dSyz_dy ----------------------------------------------------
      dSyz_dy =
     & dq1_dy(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!-------------------------------------------------------------

! dSzz_dz ----------------------------------------------------
      dc3Ux_dq1 = dc2Ux_dq1

      dc3Ux_dq2 = dc2Ux_dq2

      dc3Ux_dq3 = dc2Ux_dq3


      dc3Vy_dq1 = dc1Vy_dq1

      dc3Vy_dq1 = dc1Vy_dq2

      dc3Vy_dq3 = dc1Vy_dq3 


      dc3Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc3Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc3Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSzz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dc3Ux_dq1+dc3Vy_dq1+dc3Wz_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dc3Ux_dq2+dc3Vy_dq2+dc3Wz_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dc3Ux_dq3+dc3Vy_dq3+dc3Wz_dq3) 
!-------------------------------------------------------------

!-------------------------------------------------------------
   
      UU(i,j,k)=2.0d0*U(i,j,k)+ttoRhoJ(i,j,k)*(dSxx_dx+dSxy_dy+dSxz_dz)-
     &               UU(i,j,k)
      VV(i,j,k)=2.0d0*V(i,j,k)+ttoRhoJ(i,j,k)*(dSxy_dx+dSyy_dy+dSyz_dz)-
     &               VV(i,j,k)
      WW(i,j,k)=2.0d0*W(i,j,k)+ttoRhoJ(i,j,k)*(dSxz_dx+dSyz_dy+dSzz_dz)-
     &               WW(i,j,k)

!-------------------------------------------------------------
      end do
      end do





      j=N_up_y
      do k=N_down_z+1,N_up_z-1      
      do i=N_down_x,N_up_x


! dSxx_dx ------------------------------------------------------------
      dc1Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +             
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc1Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc1Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc1Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc1Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc1Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc1Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc1Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc1Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSxx_dx=
     & dq1_dx(2*i,2*j,2*k)*(dc1Ux_dq1+dc1Vy_dq1+dc1Wz_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dc1Ux_dq2+dc1Vy_dq2+dc1Wz_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dc1Ux_dq3+dc1Vy_dq3+dc1Wz_dq3)
!-------------------------------------------------------------

! dSxy_dy ----------------------------------------------------
      dcUy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcVx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )
 
   
      dSxy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSxz_dz ----------------------------------------------------
      dcUz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcWx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSxz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------
!-------------------------------------------------------------

! dSxy_dx ----------------------------------------------------
      dSxy_dx=
     & dq1_dx(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSyy_dy ----------------------------------------------------
      dc2Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc2Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc2Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc2Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc2Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc2Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc2Wz_dq1 = dc1Wz_dq1 
     
      dc2Wz_dq2 = dc1Wz_dq2

      dc2Wz_dq3 = dc1Wz_dq3



      dSyy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dc2Ux_dq1+dc2Vy_dq1+dc2Wz_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dc2Ux_dq2+dc2Vy_dq2+dc2Wz_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dc2Ux_dq3+dc2Vy_dq3+dc2Wz_dq3) 
!------------------------------------------------------------- 

! dSyz_dz ----------------------------------------------------
      dcVz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )


      dcWy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSyz_dz =
     & dq1_dz(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!------------------------------------------------------------- 
!-------------------------------------------------------------

! dSxz_dx ----------------------------------------------------
      dSxz_dx = 
     & dq1_dx(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------

! dSyz_dy ----------------------------------------------------
      dSyz_dy =
     & dq1_dy(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!-------------------------------------------------------------

! dSzz_dz ----------------------------------------------------
      dc3Ux_dq1 = dc2Ux_dq1

      dc3Ux_dq2 = dc2Ux_dq2

      dc3Ux_dq3 = dc2Ux_dq3


      dc3Vy_dq1 = dc1Vy_dq1

      dc3Vy_dq1 = dc1Vy_dq2

      dc3Vy_dq3 = dc1Vy_dq3 


      dc3Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc3Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc3Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSzz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dc3Ux_dq1+dc3Vy_dq1+dc3Wz_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dc3Ux_dq2+dc3Vy_dq2+dc3Wz_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dc3Ux_dq3+dc3Vy_dq3+dc3Wz_dq3) 
!-------------------------------------------------------------

!-------------------------------------------------------------
   
      UU(i,j,k)=2.0d0*U(i,j,k)+ttoRhoJ(i,j,k)*(dSxx_dx+dSxy_dy+dSxz_dz)-
     &               UU(i,j,k)
      VV(i,j,k)=2.0d0*V(i,j,k)+ttoRhoJ(i,j,k)*(dSxy_dx+dSyy_dy+dSyz_dz)-
     &               VV(i,j,k)
      WW(i,j,k)=2.0d0*W(i,j,k)+ttoRhoJ(i,j,k)*(dSxz_dx+dSyz_dy+dSzz_dz)-
     &               WW(i,j,k)

!-------------------------------------------------------------
      end do
      end do


 

      i=N_down_x
      do k=N_down_z+1,N_up_z-1
      do j=N_down_y+1,N_up_y-1
      


! dSxx_dx ------------------------------------------------------------
      dc1Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +             
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc1Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc1Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc1Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc1Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc1Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc1Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc1Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc1Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSxx_dx=
     & dq1_dx(2*i,2*j,2*k)*(dc1Ux_dq1+dc1Vy_dq1+dc1Wz_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dc1Ux_dq2+dc1Vy_dq2+dc1Wz_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dc1Ux_dq3+dc1Vy_dq3+dc1Wz_dq3)
!-------------------------------------------------------------

! dSxy_dy ----------------------------------------------------
      dcUy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcVx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )
 
   
      dSxy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSxz_dz ----------------------------------------------------
      dcUz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcWx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSxz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------
!-------------------------------------------------------------

! dSxy_dx ----------------------------------------------------
      dSxy_dx=
     & dq1_dx(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSyy_dy ----------------------------------------------------
      dc2Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc2Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc2Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc2Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc2Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc2Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc2Wz_dq1 = dc1Wz_dq1 
     
      dc2Wz_dq2 = dc1Wz_dq2

      dc2Wz_dq3 = dc1Wz_dq3



      dSyy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dc2Ux_dq1+dc2Vy_dq1+dc2Wz_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dc2Ux_dq2+dc2Vy_dq2+dc2Wz_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dc2Ux_dq3+dc2Vy_dq3+dc2Wz_dq3) 
!------------------------------------------------------------- 

! dSyz_dz ----------------------------------------------------
      dcVz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )


      dcWy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSyz_dz =
     & dq1_dz(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!------------------------------------------------------------- 
!-------------------------------------------------------------

! dSxz_dx ----------------------------------------------------
      dSxz_dx = 
     & dq1_dx(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------

! dSyz_dy ----------------------------------------------------
      dSyz_dy =
     & dq1_dy(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!-------------------------------------------------------------

! dSzz_dz ----------------------------------------------------
      dc3Ux_dq1 = dc2Ux_dq1

      dc3Ux_dq2 = dc2Ux_dq2

      dc3Ux_dq3 = dc2Ux_dq3


      dc3Vy_dq1 = dc1Vy_dq1

      dc3Vy_dq1 = dc1Vy_dq2

      dc3Vy_dq3 = dc1Vy_dq3 


      dc3Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc3Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc3Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSzz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dc3Ux_dq1+dc3Vy_dq1+dc3Wz_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dc3Ux_dq2+dc3Vy_dq2+dc3Wz_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dc3Ux_dq3+dc3Vy_dq3+dc3Wz_dq3) 
!-------------------------------------------------------------

!-------------------------------------------------------------
   
      UU(i,j,k)=2.0d0*U(i,j,k)+ttoRhoJ(i,j,k)*(dSxx_dx+dSxy_dy+dSxz_dz)-
     &               UU(i,j,k)
      VV(i,j,k)=2.0d0*V(i,j,k)+ttoRhoJ(i,j,k)*(dSxy_dx+dSyy_dy+dSyz_dz)-
     &               VV(i,j,k)
      WW(i,j,k)=2.0d0*W(i,j,k)+ttoRhoJ(i,j,k)*(dSxz_dx+dSyz_dy+dSzz_dz)-
     &               WW(i,j,k)

!-------------------------------------------------------------
      end do
      end do





      i=N_up_x
      do k=N_down_z+1,N_up_z-1
      do j=N_down_y+1,N_up_y-1
      


! dSxx_dx ------------------------------------------------------------
      dc1Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +             
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc1Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc1Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc1Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc1Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc1Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc1Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc1Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc1Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSxx_dx=
     & dq1_dx(2*i,2*j,2*k)*(dc1Ux_dq1+dc1Vy_dq1+dc1Wz_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dc1Ux_dq2+dc1Vy_dq2+dc1Wz_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dc1Ux_dq3+dc1Vy_dq3+dc1Wz_dq3)
!-------------------------------------------------------------

! dSxy_dy ----------------------------------------------------
      dcUy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcVx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )
 
   
      dSxy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSxz_dz ----------------------------------------------------
      dcUz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcWx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSxz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------
!-------------------------------------------------------------

! dSxy_dx ----------------------------------------------------
      dSxy_dx=
     & dq1_dx(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSyy_dy ----------------------------------------------------
      dc2Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc2Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc2Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc2Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc2Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc2Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc2Wz_dq1 = dc1Wz_dq1 
     
      dc2Wz_dq2 = dc1Wz_dq2

      dc2Wz_dq3 = dc1Wz_dq3



      dSyy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dc2Ux_dq1+dc2Vy_dq1+dc2Wz_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dc2Ux_dq2+dc2Vy_dq2+dc2Wz_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dc2Ux_dq3+dc2Vy_dq3+dc2Wz_dq3) 
!------------------------------------------------------------- 

! dSyz_dz ----------------------------------------------------
      dcVz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )


      dcWy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSyz_dz =
     & dq1_dz(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!------------------------------------------------------------- 
!-------------------------------------------------------------

! dSxz_dx ----------------------------------------------------
      dSxz_dx = 
     & dq1_dx(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------

! dSyz_dy ----------------------------------------------------
      dSyz_dy =
     & dq1_dy(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!-------------------------------------------------------------

! dSzz_dz ----------------------------------------------------
      dc3Ux_dq1 = dc2Ux_dq1

      dc3Ux_dq2 = dc2Ux_dq2

      dc3Ux_dq3 = dc2Ux_dq3


      dc3Vy_dq1 = dc1Vy_dq1

      dc3Vy_dq1 = dc1Vy_dq2

      dc3Vy_dq3 = dc1Vy_dq3 


      dc3Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc3Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc3Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSzz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dc3Ux_dq1+dc3Vy_dq1+dc3Wz_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dc3Ux_dq2+dc3Vy_dq2+dc3Wz_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dc3Ux_dq3+dc3Vy_dq3+dc3Wz_dq3) 
!-------------------------------------------------------------

!-------------------------------------------------------------
   
      UU(i,j,k)=2.0d0*U(i,j,k)+ttoRhoJ(i,j,k)*(dSxx_dx+dSxy_dy+dSxz_dz)-
     &               UU(i,j,k)
      VV(i,j,k)=2.0d0*V(i,j,k)+ttoRhoJ(i,j,k)*(dSxy_dx+dSyy_dy+dSyz_dz)-
     &               VV(i,j,k)
      WW(i,j,k)=2.0d0*W(i,j,k)+ttoRhoJ(i,j,k)*(dSxz_dx+dSyz_dy+dSzz_dz)-
     &               WW(i,j,k)

!-------------------------------------------------------------
      end do
      end do


!=====================================================================================================================      
         
      return
      end subroutine    
      
! Every process gets it's part of main domain to be calculated
      subroutine domain_decomposition(num_procs,N,id,N_down,N_up)

       integer N,N_down,N_up,N_wide,N_rest
       integer id
              
       N_wide=N/num_procs ! wide of line with ceiling deviding

! If the remain is equal 0, then every process gets line with width N/num_procs
! if the remain is equal N_rest, then first N_rest processes get line with width N/num_procs+1,
! and the rest of processes get line with width N/num_proc. Every process gets it's personal boundaries of domain
! N_down and N_up       
       
       if((N_wide*num_procs-N).eq.0) then
       N_down=1+N_wide*id  
       N_up=N_down+N_wide-1
       end if
       
       if((N_wide*num_procs-N).ne.0) then
       N_rest=N-N_wide*num_procs  
       
       if(id<N_rest) then
       N_down=1+N_wide*id+id
       N_up=N_down+N_wide
       end if
       
       if(id>=N_rest) then
       N_down=1+N_wide*id+N_rest
       N_up=N_down+N_wide-1
       end if      
       
       end if
             
      return
      end subroutine	
      
! Procedure of exchanges
      subroutine exchange1(
     &U,V,W,N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     &id,id_x,id_y,id_z,num_procs,n_cartesian_comm,n_dims,
     &A_N_down_x,A_N_up_x,A_down_x,A_up_x,
     &A_N_down_y,A_N_up_y,A_down_y,A_up_y,
     &A_N_down_z,A_N_up_z,A_down_z,A_up_z,n_request,n_statuses)
   
      
      include "mpif.h"
      
      EXTERNAL MPI_BARRIER,MPI_Send,MPI_Recv,MPI_Isend,MPI_Irecv
      
      integer id,id_x,id_y,id_z,num_procs,n_dims(3)
      integer i,j,k,m,l,i_c
      integer i_send_size,j_send_size,k_send_size
      integer N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z
      integer N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z
      integer n_cartesian_comm,n_error,n_status(MPI_STATUS_SIZE)
      integer n_statuses(MPI_STATUS_SIZE,1:12)
      integer n_request(1:12)
      
      
      double precision
     & U(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & V(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & W(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
         
     &       A_N_down_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),
     &         A_N_up_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),
     &         A_down_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),
     &           A_up_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),

     &       A_N_down_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),
     &         A_N_up_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),
     &         A_down_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),
     &           A_up_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),

     &       A_N_down_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)),
     &         A_N_up_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)),
     &         A_down_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)),
     &           A_up_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1))

!      print *,'max U before send',abs(maxval(real(U))), maxloc(real(U))            
!      print *,'max V before send',abs(maxval(real(V))), maxloc(real(V))
!      print *,'max W before send',abs(maxval(real(W))), maxloc(real(W))
!      print *,''


      i_c=1
      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      A_N_down_x(i_c)=U(N_down_x,j,k)
      A_N_up_x(i_c)=U(N_up_x,j,k)
      i_c=i_c+1 
      end do
      end do

      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      A_N_down_x(i_c)=V(N_down_x,j,k)
      A_N_up_x(i_c)=V(N_up_x,j,k)
      i_c=i_c+1 
      end do
      end do

      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      A_N_down_x(i_c)=W(N_down_x,j,k)
      A_N_up_x(i_c)=W(N_up_x,j,k)
      i_c=i_c+1 
      end do
      end do

!      print *,'N_down_x,N_up_x write  i_c=',i_c-1

      i_c=1

      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      A_N_down_y(i_c)=U(i,N_down_y,k)
      A_N_up_y(i_c)=U(i,N_up_y,k)
      i_c=i_c+1
      end do
      end do

      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      A_N_down_y(i_c)=V(i,N_down_y,k)
      A_N_up_y(i_c)=V(i,N_up_y,k)
      i_c=i_c+1
      end do
      end do
   
      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      A_N_down_y(i_c)=W(i,N_down_y,k)
      A_N_up_y(i_c)=W(i,N_up_y,k)
      i_c=i_c+1
      end do
      end do

!      print *,'N_down_y,N_up_y write  i_c=',i_c-1

      i_c=1

      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x
      A_N_down_z(i_c)=U(i,j,N_down_z)
      A_N_up_z(i_c)=U(i,j,N_up_z)
      i_c=i_c+1
      end do
      end do

      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x
      A_N_down_z(i_c)=V(i,j,N_down_z)
      A_N_up_z(i_c)=V(i,j,N_up_z)
      i_c=i_c+1
      end do
      end do

      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x
      A_N_down_z(i_c)=W(i,j,N_down_z)
      A_N_up_z(i_c)=W(i,j,N_up_z)
      i_c=i_c+1
      end do
      end do


      i_send_size=3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)
      j_send_size=3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)
      k_send_size=3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)


      call MPI_Isend(A_N_down_x,i_send_size,MPI_DOUBLE_PRECISION,
     &N_bot_x,0,n_cartesian_comm,n_request(1),n_error)     
      call MPI_Irecv(A_up_x,i_send_size,MPI_DOUBLE_PRECISION,
     &N_top_x,0,n_cartesian_comm,n_request(2),n_error)
            
      call MPI_Isend(A_N_up_x,i_send_size,MPI_DOUBLE_PRECISION,
     &N_top_x,1,n_cartesian_comm,n_request(3),n_error)     
      call MPI_Irecv(A_down_x,i_send_size,MPI_DOUBLE_PRECISION,
     &N_bot_x,1,n_cartesian_comm,n_request(4),n_error)
      

      call MPI_Isend(A_N_down_y,j_send_size,MPI_DOUBLE_PRECISION,
     &N_bot_y,2,n_cartesian_comm,n_request(5),n_error)     
      call MPI_Irecv(A_up_y,j_send_size,MPI_DOUBLE_PRECISION,
     &N_top_y,2,n_cartesian_comm,n_request(6),n_error)
            
      call MPI_Isend(A_N_up_y,j_send_size,MPI_DOUBLE_PRECISION,
     &N_top_y,3,n_cartesian_comm,n_request(7),n_error)     
      call MPI_Irecv(A_down_y,j_send_size,MPI_DOUBLE_PRECISION,
     &N_bot_y,3,n_cartesian_comm,n_request(8),n_error)


      call MPI_Isend(A_N_down_z,k_send_size,MPI_DOUBLE_PRECISION,
     &N_bot_z,4,n_cartesian_comm,n_request(9),n_error)     
      call MPI_Irecv(A_up_z,k_send_size,MPI_DOUBLE_PRECISION,
     &N_top_z,4,n_cartesian_comm,n_request(10),n_error)
            
      call MPI_Isend(A_N_up_z,k_send_size,MPI_DOUBLE_PRECISION,
     &N_top_z,5,n_cartesian_comm,n_request(11),n_error)     
      call MPI_Irecv(A_down_z,k_send_size,MPI_DOUBLE_PRECISION,
     &N_bot_z,5,n_cartesian_comm,n_request(12),n_error)
      

!      call MPI_Send(A_N_down_x,i_send_size,MPI_DOUBLE_PRECISION,
!     &N_bot_x,0,n_cartesian_comm,n_error)     
!      call MPI_Recv(A_up_x,i_send_size,MPI_DOUBLE_PRECISION,
!     &N_top_x,0,n_cartesian_comm,n_status,n_error)
            
!      call MPI_Send(A_N_up_x,i_send_size,MPI_DOUBLE_PRECISION,
!     &N_top_x,1,n_cartesian_comm,n_error)     
!      call MPI_Recv(A_down_x,i_send_size,MPI_DOUBLE_PRECISION,
!     &N_bot_x,1,n_cartesian_comm,n_status,n_error)
      

!      call MPI_Send(A_N_down_y,j_send_size,MPI_DOUBLE_PRECISION,
!     &N_bot_y,2,n_cartesian_comm,n_error)     
!      call MPI_Recv(A_up_y,j_send_size,MPI_DOUBLE_PRECISION,
!     &N_top_y,2,n_cartesian_comm,n_status,n_error)
            
!      call MPI_Send(A_N_up_y,j_send_size,MPI_DOUBLE_PRECISION,
!     &N_top_y,3,n_cartesian_comm,n_error)     
!      call MPI_Recv(A_down_y,j_send_size,MPI_DOUBLE_PRECISION,
!     &N_bot_y,3,n_cartesian_comm,n_status,n_error)


!      call MPI_Send(A_N_down_z,k_send_size,MPI_DOUBLE_PRECISION,
!     &N_bot_z,4,n_cartesian_comm,n_error)     
!      call MPI_Recv(A_up_z,k_send_size,MPI_DOUBLE_PRECISION,
!     &N_top_z,4,n_cartesian_comm,n_status,n_error)
            
!      call MPI_Send(A_N_up_z,k_send_size,MPI_DOUBLE_PRECISION,
!     &N_top_z,5,n_cartesian_comm,n_error)     
!      call MPI_Recv(A_down_z,k_send_size,MPI_DOUBLE_PRECISION,
!     &N_bot_z,5,n_cartesian_comm,n_status,n_error)
 
!----------------------------------------------------------------------

     

!======================================================================


!======================================================================

      return
      end subroutine
      
      

      subroutine exchange2(
     &U,V,W,N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     &N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z,
     &id,id_x,id_y,id_z,num_procs,n_cartesian_comm,n_dims,
     &A_N_down_x,A_N_up_x,A_down_x,A_up_x,
     &A_N_down_y,A_N_up_y,A_down_y,A_up_y,
     &A_N_down_z,A_N_up_z,A_down_z,A_up_z)
   
      
      include "mpif.h"
      
      EXTERNAL MPI_BARRIER,MPI_Send,MPI_Recv,MPI_Isend,MPI_Irecv
      
      integer id,id_x,id_y,id_z,num_procs,n_dims(3)
      integer i,j,k,m,l,i_c
      integer i_send_size,j_send_size,k_send_size
      integer N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z
      integer N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z
      integer n_cartesian_comm,n_error,n_status(MPI_STATUS_SIZE)
      !integer n_statuses(MPI_STATUS_SIZE,1:132)
      !integer n_request(1:132)
      
      
      double precision
     & U(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & V(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & W(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
         
     &       A_N_down_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),
     &         A_N_up_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),
     &         A_down_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),
     &           A_up_x(3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)),

     &       A_N_down_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),
     &         A_N_up_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),
     &         A_down_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),
     &           A_up_y(3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)),

     &       A_N_down_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)),
     &         A_N_up_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)),
     &         A_down_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)),
     &           A_up_z(3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1))



      i_send_size=3*(N_up_y-N_down_y+1)*(N_up_z-N_down_z+1)
      j_send_size=3*(N_up_x-N_down_x+1)*(N_up_z-N_down_z+1)
      k_send_size=3*(N_up_x-N_down_x+1)*(N_up_y-N_down_y+1)

 
 
      if(id_x>0) then
      i_c=1
      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      U(N_down_x-1,j,k)=A_down_x(i_c)
      i_c=i_c+1 
      end do
      end do

      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      V(N_down_x-1,j,k)=A_down_x(i_c)
      i_c=i_c+1 
      end do
      end do

      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      W(N_down_x-1,j,k)=A_down_x(i_c)
      i_c=i_c+1 
      end do
      end do

      end if

      if(id_x<n_dims(1)-1) then
      i_c=1
      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      U(N_up_x+1,j,k)=A_up_x(i_c)
      i_c=i_c+1 
      end do
      end do

      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      V(N_up_x+1,j,k)=A_up_x(i_c)
      i_c=i_c+1 
      end do
      end do

      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      W(N_up_x+1,j,k)=A_up_x(i_c)
      i_c=i_c+1 
      end do
      end do
      
      end if

!      print *,'N_down_x-1,N_up_x+1 read  i_c=',i_c-1


      if(id_y>0) then
      i_c=1

      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      U(i,N_down_y-1,k)=A_down_y(i_c)
      i_c=i_c+1
      end do
      end do

      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      V(i,N_down_y-1,k)=A_down_y(i_c)
      i_c=i_c+1
      end do
      end do
   
      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      W(i,N_down_y-1,k)=A_down_y(i_c)
      i_c=i_c+1
      end do
      end do

      end if

      if(id_y<n_dims(2)-1) then
      i_c=1

      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      U(i,N_up_y+1,k)=A_up_y(i_c)
      i_c=i_c+1
      end do
      end do

      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      V(i,N_up_y+1,k)=A_up_y(i_c)
      i_c=i_c+1
      end do
      end do
   
      do k=N_down_z,N_up_z
      do i=N_down_x,N_up_x
      W(i,N_up_y+1,k)=A_up_y(i_c)
      i_c=i_c+1
      end do
      end do

      end if

!      print *,'N_down_y-1,N_up_y+1 read  i_c=',i_c-1


      if(id_z>0) then
      i_c=1

      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x
      U(i,j,N_down_z-1)=A_down_z(i_c)
      i_c=i_c+1
      end do
      end do

      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x
      V(i,j,N_down_z-1)=A_down_z(i_c)
      i_c=i_c+1
      end do
      end do

      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x
      W(i,j,N_down_z-1)=A_down_z(i_c)
      i_c=i_c+1
      end do
      end do

!      print *,'N_down_z-1,N_up_z+1 read  i_c=',i_c-1

      end if

      if(id_z<n_dims(3)-1) then
      i_c=1

      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x
      U(i,j,N_up_z+1)=A_up_z(i_c)
      i_c=i_c+1
      end do
      end do

      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x
      V(i,j,N_up_z+1)=A_up_z(i_c)
      i_c=i_c+1
      end do
      end do

      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x
      W(i,j,N_up_z+1)=A_up_z(i_c)
      i_c=i_c+1
      end do
      end do
      
      end if


      N_ud_x=N_up_x-N_down_x+1
      N_ud_y=N_up_y-N_down_y+1
      N_ud_z=N_up_z-N_down_z+1



!=== N_down_x-1 =======================================================

!--- (N_down_x-1,:,N_down_z) ------------------------------------------
      call MPI_Send(U(N_down_x-1,N_down_y:N_up_y,N_down_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,6,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x-1,N_down_y:N_up_y,N_up_z+1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,6,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_down_x-1,N_down_y:N_up_y,N_down_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,7,n_cartesian_comm,
     &n_error)          
      call MPI_Recv(V(N_down_x-1,N_down_y:N_up_y,N_up_z+1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,7,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_down_x-1,N_down_y:N_up_y,N_down_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,8,n_cartesian_comm,
     &n_error)          
      call MPI_Recv(W(N_down_x-1,N_down_y:N_up_y,N_up_z+1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,8,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------   

!--- (N_down_x-1,:,N_up_z) ------------------------------------------
      call MPI_Send(U(N_down_x-1,N_down_y:N_up_y,N_up_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,9,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x-1,N_down_y:N_up_y,N_down_z-1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,9,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_down_x-1,N_down_y:N_up_y,N_up_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,10,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x-1,N_down_y:N_up_y,N_down_z-1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,10,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_down_x-1,N_down_y:N_up_y,N_up_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,11,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x-1,N_down_y:N_up_y,N_down_z-1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,11,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!--- (N_down_x-1,N_down_y,:) --------------------------------------------
      call MPI_Send(U(N_down_x-1,N_down_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,12,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x-1,N_up_y+1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,12,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_down_x-1,N_down_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,13,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x-1,N_up_y+1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,13,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_down_x-1,N_down_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,14,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x-1,N_up_y+1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,14,n_cartesian_comm,
     &n_status,n_error)     
!------------------------------------------------------------------------

!--- (N_down_x-1,N_up_y,:) --------------------------------------------
      call MPI_Send(U(N_down_x-1,N_up_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,15,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x-1,N_down_y-1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,15,n_cartesian_comm,
     &n_status,n_error)     
   
      call MPI_Send(V(N_down_x-1,N_up_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,16,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x-1,N_down_y-1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,16,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_down_x-1,N_up_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,17,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x-1,N_down_y-1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,17,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!======================================================================


!=== N_up_x+1 =======================================================

!--- (N_down_x-1,:,N_down_z) ------------------------------------------
      call MPI_Send(U(N_up_x+1,N_down_y:N_up_y,N_down_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,18,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_up_x+1,N_down_y:N_up_y,N_up_z+1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,18,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_up_x+1,N_down_y:N_up_y,N_down_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,19,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_up_x+1,N_down_y:N_up_y,N_up_z+1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,19,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_up_x+1,N_down_y:N_up_y,N_down_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,20,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_up_x+1,N_down_y:N_up_y,N_up_z+1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,20,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------   

!--- (N_down_x-1,:,N_up_z) ------------------------------------------
      call MPI_Send(U(N_up_x+1,N_down_y:N_up_y,N_up_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,21,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_up_x+1,N_down_y:N_up_y,N_down_z-1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,21,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_up_x+1,N_down_y:N_up_y,N_up_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,22,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_up_x+1,N_down_y:N_up_y,N_down_z-1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,22,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_up_x+1,N_down_y:N_up_y,N_up_z),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_top_z,23,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_up_x+1,N_down_y:N_up_y,N_down_z-1),N_ud_y,
     &MPI_DOUBLE_PRECISION,N_bot_z,23,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!--- (N_down_x-1,N_down_y,:) --------------------------------------------
      call MPI_Send(U(N_up_x+1,N_down_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,24,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_up_x+1,N_up_y+1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,24,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_up_x+1,N_down_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,25,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_up_x+1,N_up_y+1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,25,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_up_x+1,N_down_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,26,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_up_x+1,N_up_y+1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,26,n_cartesian_comm,
     &n_status,n_error)     
!------------------------------------------------------------------------

!--- (N_down_x-1,N_up_y,:) --------------------------------------------
      call MPI_Send(U(N_up_x+1,N_up_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,27,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_up_x+1,N_down_y-1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,27,n_cartesian_comm,
     &n_status,n_error)     
   
      call MPI_Send(V(N_up_x+1,N_up_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,28,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_up_x+1,N_down_y-1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,28,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_up_x+1,N_up_y,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_top_y,29,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_up_x+1,N_down_y-1,N_down_z:N_up_z),N_ud_z,
     &MPI_DOUBLE_PRECISION,N_bot_y,29,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!======================================================================


!=== N_down_y-1 =======================================================

!--- (:,N_down_y-1,N_down_z) ------------------------------------------
      call MPI_Send(U(N_down_x:N_up_x,N_down_y-1,N_down_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,30,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x:N_up_x,N_down_y-1,N_up_z+1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,30,n_cartesian_comm,
     &n_status,n_error)     
      
      call MPI_Send(V(N_down_x:N_up_x,N_down_y-1,N_down_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,31,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x:N_up_x,N_down_y-1,N_up_z+1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,31,n_cartesian_comm,
     &n_status,n_error)     
 
      call MPI_Send(W(N_down_x:N_up_x,N_down_y-1,N_down_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,32,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x:N_up_x,N_down_y-1,N_up_z+1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,32,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!--- (:,N_down_y-1,N_up_z) --------------------------------------------
      call MPI_Send(U(N_down_x:N_up_x,N_down_y-1,N_up_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,33,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x:N_up_x,N_down_y-1,N_down_z-1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,33,n_cartesian_comm,
     &n_status,n_error)     
      
      call MPI_Send(V(N_down_x:N_up_x,N_down_y-1,N_up_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,34,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x:N_up_x,N_down_y-1,N_down_z-1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,34,n_cartesian_comm,
     &n_status,n_error)     
      
      call MPI_Send(W(N_down_x:N_up_x,N_down_y-1,N_up_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,35,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x:N_up_x,N_down_y-1,N_down_z-1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,35,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!======================================================================


!=== N_up_y+1 =======================================================

!--- (:,N_up_y+1,N_down_z) ------------------------------------------
      call MPI_Send(U(N_down_x:N_up_x,N_up_y+1,N_down_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,36,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x:N_up_x,N_up_y+1,N_up_z+1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,36,n_cartesian_comm,
     &n_status,n_error)     
      
      call MPI_Send(V(N_down_x:N_up_x,N_up_y+1,N_down_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,37,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x:N_up_x,N_up_y+1,N_up_z+1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,37,n_cartesian_comm,
     &n_status,n_error)     
 
      call MPI_Send(W(N_down_x:N_up_x,N_up_y+1,N_down_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,38,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x:N_up_x,N_up_y+1,N_up_z+1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,38,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!--- (:,N_up_y+1,N_up_z) --------------------------------------------
      call MPI_Send(U(N_down_x:N_up_x,N_up_y+1,N_up_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,39,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x:N_up_x,N_up_y+1,N_down_z-1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,39,n_cartesian_comm,
     &n_status,n_error)     
      
      call MPI_Send(V(N_down_x:N_up_x,N_up_y+1,N_up_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,40,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x:N_up_x,N_up_y+1,N_down_z-1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,40,n_cartesian_comm,
     &n_status,n_error)     
      
      call MPI_Send(W(N_down_x:N_up_x,N_up_y+1,N_up_z),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_top_z,41,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x:N_up_x,N_up_y+1,N_down_z-1),N_ud_x,
     &MPI_DOUBLE_PRECISION,N_bot_z,41,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!======================================================================

!=== Corners ==========================================================

!--- (N_down_x-1,N_down_y-1,N_down_z) ---------------------------------
      call MPI_Send(U(N_down_x-1,N_down_y-1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,42,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x-1,N_down_y-1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,42,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_down_x-1,N_down_y-1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,43,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x-1,N_down_y-1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,43,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_down_x-1,N_down_y-1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,44,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x-1,N_down_y-1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,44,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------
!--- (N_down_x-1,N_down_y-1,N_up_z) ---------------------------------
      call MPI_Send(U(N_down_x-1,N_down_y-1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,45,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x-1,N_down_y-1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,45,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_down_x-1,N_down_y-1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,46,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x-1,N_down_y-1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,46,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_down_x-1,N_down_y-1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,47,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x-1,N_down_y-1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,47,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!--- (N_down_x-1,N_up_y+1,N_down_z) ---------------------------------
      call MPI_Send(U(N_down_x-1,N_up_y+1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,48,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x-1,N_up_y+1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,48,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_down_x-1,N_up_y+1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,49,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x-1,N_up_y+1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,49,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_down_x-1,N_up_y+1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,50,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x-1,N_up_y+1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,50,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------
!--- (N_down_x-1,N_up_y+1,N_up_z) ---------------------------------
      call MPI_Send(U(N_down_x-1,N_up_y+1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,51,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_down_x-1,N_up_y+1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,51,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_down_x-1,N_up_y+1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,52,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_down_x-1,N_up_y+1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,52,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_down_x-1,N_up_y+1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,53,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_down_x-1,N_up_y+1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,53,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!--- (N_up_x+1,N_down_y-1,N_down_z) ---------------------------------
      call MPI_Send(U(N_up_x+1,N_down_y-1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,54,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_up_x+1,N_down_y-1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,54,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_up_x+1,N_down_y-1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,55,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_up_x+1,N_down_y-1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,55,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_up_x+1,N_down_y-1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,56,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_up_x+1,N_down_y-1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,56,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------
!--- (N_up_x+1,N_down_y-1,N_up_z) ---------------------------------
      call MPI_Send(U(N_up_x+1,N_down_y-1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,57,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_up_x+1,N_down_y-1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,57,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_up_x+1,N_down_y-1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,58,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_up_x+1,N_down_y-1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,58,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_up_x+1,N_down_y-1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,59,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_up_x+1,N_down_y-1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,59,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------

!--- (N_up_x+1,N_up_y+1,N_down_z) ---------------------------------
      call MPI_Send(U(N_up_x+1,N_up_y+1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,60,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_up_x+1,N_up_y+1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,60,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_up_x+1,N_up_y+1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,61,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_up_x+1,N_up_y+1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,61,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_up_x+1,N_up_y+1,N_down_z),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,62,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_up_x+1,N_up_y+1,N_up_z+1),1,
     &MPI_DOUBLE_PRECISION,N_top_z,62,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------
!--- (N_down_x+1,N_down_y+1,N_up_z) ---------------------------------
      call MPI_Send(U(N_up_x+1,N_up_y+1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,63,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(U(N_up_x+1,N_up_y+1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,63,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(V(N_up_x+1,N_up_y+1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,64,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(V(N_up_x+1,N_up_y+1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,64,n_cartesian_comm,
     &n_status,n_error)     

      call MPI_Send(W(N_up_x+1,N_up_y+1,N_up_z),1,
     &MPI_DOUBLE_PRECISION,N_top_z,65,n_cartesian_comm,
     &n_error)     
      call MPI_Recv(W(N_up_x+1,N_up_y+1,N_down_z-1),1,
     &MPI_DOUBLE_PRECISION,N_bot_z,65,n_cartesian_comm,
     &n_status,n_error)     
!----------------------------------------------------------------------
!======================================================================

      return
      end subroutine
      

      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
        