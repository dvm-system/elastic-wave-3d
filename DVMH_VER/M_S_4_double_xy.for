
      implicit double precision(A-H,R-Z)
      implicit character(O)
       
      
!      include "mpif.h" ! MPI
    
    
      double precision, allocatable::                  
     &                       X(:,:,:),Y(:,:,:),Z(:,:,:),
     &                     a1_10(:), a1_20(:), a1_11(:), a1_21(:),
     &                     a2_10(:), a2_20(:), a2_11(:), a2_21(:),
     &                     a3_10(:), a3_20(:), a3_11(:), a3_21(:),  
     &                        surf_Z(:,:),
     &                     surf2_X(:,:),surf2_Y(:,:),surf2_Z(:,:),
     &      dX_dq3_surf2(:,:),dY_dq3_surf2(:,:),dZ_dq3_surf2(:,:),
     & Y_x_0(:,:),Z_x_0(:,:),Y_x_aW(:,:),Z_x_aW(:,:),
     & X_y_0(:,:),Z_y_0(:,:),X_y_aL(:,:),Z_y_aL(:,:),
    
     & F1x_y_0(:,:),F1x_y_aL(:,:),
     & F1y_y_0(:,:),F1y_y_aL(:,:),
     & F1z_y_0(:,:),F1z_y_aL(:,:),
    
     & F2x_z_surf2(:,:),F2x_z_aH(:,:),
     & F2y_z_surf2(:,:),F2y_z_aH(:,:),
     & F2z_z_surf2(:,:),F2z_z_aH(:,:), 

     & X_y_0_z_surf2(:),X_y_aL_z_surf2(:),
     & Y_x_0_z_surf2(:),Y_x_aW_z_surf2(:),
     & Z_x_0_z_surf2(:),Z_x_aW_z_surf2(:),
     & Z_y_0_z_surf2(:),Z_y_aL_z_surf2(:),
     &                           U1(:,:,:),U(:,:,:),V(:,:,:),W(:,:,:),
     &                                  UU(:,:,:),VV(:,:,:),WW(:,:,:),
     &                                      alambda(:,:,:),amu(:,:,:),
     &                                      ttoRhoJ(:,:,:),Rho(:,:,:),
     &                                                           F(:),
     &                      dq1_dx(:,:,:),dq2_dx(:,:,:),dq3_dx(:,:,:),
     &                      dq1_dy(:,:,:),dq2_dy(:,:,:),dq3_dy(:,:,:),
     &                      dq1_dz(:,:,:),dq2_dz(:,:,:),dq3_dz(:,:,:),
     &                                        A1(:,:),A2(:,:),A3(:,:),
     &                                        B1(:,:),B2(:,:),B3(:,:),
     &                                        C1(:,:),C2(:,:),C3(:,:),
     &    R1_1(:,:),R1_2(:,:),R1_3(:,:),R1_4(:,:),R1_5(:,:),R1_6(:,:),
     &    R2_1(:,:),R2_2(:,:),R2_3(:,:),R2_4(:,:),R2_5(:,:),R2_6(:,:),
     &    R3_1(:,:),R3_2(:,:),R3_3(:,:),R3_4(:,:),R3_5(:,:),R3_6(:,:),
     &                              dz_dq1_surf(:,:),dz_dq2_surf(:,:),
     &                                                      amaxes(:),
     &                                        tr_x(:),tr_y(:),tr_z(:),
     &                         trace_U(:,:),trace_V(:,:),trace_W(:,:)

      integer, allocatable::         i_tr(:),j_tr(:),k_tr(:),id_tr(:),
     & n_request(:),n_statuses(:,:)
      double precision dvtime,F_curr, F_next, tmpSources(3)

!DVM$ DISTRIBUTE dq1_dx(BLOCK, BLOCK, BLOCK)
!DVM$ ALIGN (i,j,k) WITH dq1_dx(i,j,k)::dq2_dx, dq3_dx, dq1_dy, dq2_dy
!DVM$ ALIGN (i,j,k) WITH dq1_dx(i,j,k)::dq3_dy, dq1_dz, dq2_dz, dq3_dz
!DVM$ ALIGN (i,j,k) WITH dq1_dx(i,j,k)::X,Y,Z

!DVM$ ALIGN (i,j,k) WITH dq1_dx(2*i,2*j,2*k)::alambda,amu,Rho,ttoRhoJ
!DVM$ ALIGN (i,j,k) WITH dq1_dx(2*i,2*j,2*k)::U,V,W,UU,VV,WW

!DVM$ ALIGN (i,j) WITH dq1_dx(2*i,2*j,*)::A1, A2, A3, B1, B2, B3
!DVM$ ALIGN (i,j) WITH dq1_dx(2*i,2*j,*)::C1,C2,C3,R1_1,R1_2,R1_3
!DVM$ ALIGN (i,j) WITH dq1_dx(2*i,2*j,*)::R1_4,R1_5,R1_6,R2_1
!DVM$ ALIGN (i,j) WITH dq1_dx(2*i,2*j,*)::R2_2,R2_3,R2_4,R2_5,R2_6
!DVM$ ALIGN (i,j) WITH dq1_dx(2*i,2*j,*)::R3_1,R3_2,R3_3,R3_4,R3_5
!DVM$ ALIGN (i,j) WITH dq1_dx(2*i,2*j,*)::R3_6,dz_dq1_surf,dz_dq2_surf

!DVM$ SHADOW(1:1, 1:1, 1:2) :: U,V,W
!DVM$ SHADOW(2:2, 2:2, 2:2) :: dq1_dx,dq2_dx, dq3_dx, dq1_dy, dq2_dy
!DVM$ SHADOW(2:2, 2:2, 2:2) :: dq3_dy, dq1_dz, dq2_dz, dq3_dz,X,Y,Z

      integer i, j, k, M, L, N, mult, N_rest, N_wide,newSources(3)
      double precision pi, speed, d, aW, aL, aH, 
     & a_length, a_width, a_height,
     & coef, a_nx, a_ny, a_nz, rd, aH_level,
     & dZ_dq1, dZ_dq2, sqr,
     & F1x,F1y,F1z,
     & dF1x_dq2,dF1y_dq2,dF1z_dq2,
     & F2x,F2y,F2z,
     & dF1x_dq3,dF1y_dq3,dF1z_dq3,
     & dF1x_dq2dq3,dF1y_dq2dq3,dF1z_dq2dq3,
     & dF2x_dq3,dF2y_dq3,dF2z_dq3


      
      character(len=50)::o_filename1,o_filename2
      character(len=50)::o_numb_str
      character(len=50)::o_step 
      character(len=50)::o_i_tr     
      character(len=50)::o_j_tr
      character(len=50)::o_k_tr
      character(len=50)::o_id_x
      character(len=50)::o_id_y
      character(len=50)::o_id_z
      character(len=50)::o_l1

!=============================================================================================== 
       open(unit=31,file='!_Speed.txt',form='formatted') 
       read(31,*) s
       close(31)

      speed=dble(s) ! Vp

      open(unit=31,file='!_d_coef.txt',form='formatted') 
      read(31,*) d_coef
      close(31)
      
      d=speed/dble(d_coef) ! Discrete step
      
      open(unit=31,file='!_Medium_size.txt',form='formatted') 
      read(31,*) a_w
      read(31,*) a_l
      read(31,*) a_h
      close(31)      
      
      a_width=dble(a_w)  ! width of area
      a_length=dble(a_l) ! length of area
      a_height=dble(a_h)  ! height of area

!====================== Time parameters ======================

      open(unit=31,file='!_dt_coef.txt',form='formatted') 
      read(31,*) dt_coef
      close(31)
     
      dt=dble(dt_coef)*d ! time discrete step

      t=2.0d0 ! time of source work

      
      open(unit=31,file='!_Explicite_time.txt',form='formatted') 
      read(31,*) explicite_time
      close(31)

      expl_time=dble(explicite_time) ! time of wave moving after source ended it's work

      iTT=floor(t/dt)+1
      iTime=floor((t+expl_time)/dt)+1

      open(unit=31,file='!_Snapshot_frequency.txt',form='formatted') 
      read(31,*) i_snap
      close(31)

      iTT = 20
      iTime = 40
      write(*,'(a,f7.4)') 'dt= ',dt
      write(*,'(a,i6)') 'iTT= ',iTT
      write(*,'(a,i6)') 'iTime= ',iTime
      print *,''
     
!=============================================================
!====================== Source position ======================
      
      open(unit=31,file='!_Source.txt',form='formatted') 
      read(31,*) s_x
      read(31,*) s_y
      read(31,*) s_z
      close(31)

      x0=dble(s_x)
      y0=dble(s_y)
      z0=dble(s_z)
!=============================================================

      aH_level=-1.0d0

      M=ceiling(a_width/d)  ! Number of dots at X axis
      L=ceiling(a_length/d) ! Number of dots at Y axis 
      N=ceiling(a_height/d)  ! Number of dots at Z axis
      
      a_width=d*(M)
      a_length=d*(L)
      a_height=d*(N)
      
      aW=a_width
      aL=a_length
      aH=a_height

      pi=dacos(-1.0d0)

      rdd=1.0d0/(d*d)
      rd=1.0d0/d


      write(*,'(a,f6.3,a,i6)') 'width(X)=  ',a_width,'  M=',M
      write(*,'(a,f6.3,a,i6)') 'length(Y)= ',a_length,'  L=',L
      write(*,'(a,f6.3,a,i6)') 'height(Z)= ',a_height,'  N=',N
      print *,''
      write(*,'(a,f7.4)') 'd= ',d
      print *,''
  

!====================== Traces ===============================
      num_tr=10
!=============================================================

!====================== domain decomposition =================      
      N_down_x=2
      N_up_x=M-1
      N_down_y=2
      N_up_y=L-1
      N_up_z=N-1
      N_down_z=1
!=============================================================




!===========================================================================================================================================================
!======================================================================================GRID=================================================================
!===========================================================================================================================================================
      allocate (
     & dq1_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     &  2*N_down_z-3:2*N_up_z+3),
     & X(2*N_down_x-3:2*N_up_x+3,
     &   2*N_down_y-3:2*N_up_y+3,
     &   2*N_down_z-3:2*N_up_z+3),
     & Y(2*N_down_x-3:2*N_up_x+3,
     &   2*N_down_y-3:2*N_up_y+3,
     &   2*N_down_z-3:2*N_up_z+3),

     & Z(2*N_down_x-3:2*N_up_x+3,
     &   2*N_down_y-3:2*N_up_y+3,
     &   2*N_down_z-3:2*N_up_z+3),

     &   a1_10(2*M+1),a1_20(2*M+1),a1_11(2*M+1),a1_21(2*M+1),
     &   a2_10(2*L+1),a2_20(2*L+1),a2_11(2*L+1),a2_21(2*L+1),
     &   a3_10(3:2*N+1),a3_20(3:2*N+1),a3_11(3:2*N+1),a3_21(3:2*N+1),

     & surf_Z(0:2*M+2,0:2*L+2),
     & surf2_X(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & surf2_Y(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & surf2_Z(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),

     & dX_dq3_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & dY_dq3_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & dZ_dq3_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
    
     & Y_x_0(2*N_down_y-4:2*N_up_y+4,2*N_down_z-3:2*N_up_z+3),
     & Z_x_0(2*N_down_y-4:2*N_up_y+4,2*N_down_z-3:2*N_up_z+3),
     & Y_x_aW(2*N_down_y-4:2*N_up_y+4,2*N_down_z-3:2*N_up_z+3),
     & Z_x_aW(2*N_down_y-4:2*N_up_y+4,2*N_down_z-3:2*N_up_z+3),
  
     & X_y_0(2*N_down_x-4:2*N_up_x+4,2*N_down_z-3:2*N_up_z+3),
     & Z_y_0(2*N_down_x-4:2*N_up_x+4,2*N_down_z-3:2*N_up_z+3),
     & X_y_aL(2*N_down_x-4:2*N_up_x+4,2*N_down_z-3:2*N_up_z+3),
     & Z_y_aL(2*N_down_x-4:2*N_up_x+4,2*N_down_z-3:2*N_up_z+3),


     & F1x_y_0(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
     & F1x_y_aL(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
     & F1y_y_0(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
     & F1y_y_aL(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
     & F1z_y_0(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
     & F1z_y_aL(2*N_down_x-3:2*N_up_x+3,2*N_down_z-3:2*N_up_z+3),
    
     & F2x_z_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & F2x_z_aH(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & F2y_z_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & F2y_z_aH(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & F2z_z_surf2(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),
     & F2z_z_aH(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3),

     & X_y_0_z_surf2(1:2*M+1),X_y_aL_z_surf2(1:2*M+1),
     & Y_x_0_z_surf2(1:2*L+1),Y_x_aW_z_surf2(1:2*L+1),
     & Z_x_0_z_surf2(1:2*L+1),Z_x_aW_z_surf2(1:2*L+1),
     & Z_y_0_z_surf2(1:2*M+1),Z_y_aL_z_surf2(1:2*M+1)) 

      X=0.0d0
      Y=0.0d0
      Z=0.0d0
      
      a1_10=0.0d0
      a1_20=0.0d0
      a1_11=0.0d0
      a1_21=0.0d0
      a2_10=0.0d0
      a2_20=0.0d0
      a2_11=0.0d0
      a2_21=0.0d0
      a3_10=0.0d0
      a3_20=0.0d0
      a3_11=0.0d0
      a3_21=0.0d0

      surf_Z=0.0d0
      surf2_X=0.0d0
      surf2_Y=0.0d0
      surf2_Z=0.0d0

      dX_dq3_surf2=0.0d0
      dY_dq3_surf2=0.0d0
      dZ_dq3_surf2=0.0d0

      Y_x_0=0.0d0
      Z_x_0=0.0d0
      Y_x_aW=0.0d0
      Z_x_aW=0.0d0
      X_y_0=0.0d0
      Z_y_0=0.0d0
      X_y_aL=0.0d0
      Z_y_aL=0.0d0

      F1x_y_0=0.0d0
      F1x_y_aL=0.0d0
      F1y_y_0=0.0d0
      F1y_y_aL=0.0d0
      F1z_y_0=0.0d0
      F1z_y_aL=0.0d0
    
      F2x_z_surf2=0.0d0
      F2x_z_aH=0.0d0
      F2y_z_surf2=0.0d0
      F2y_z_aH=0.0d0
      F2z_z_surf2=0.0d0
      F2z_z_aH=0.0d0

      X_y_0_z_surf2=0.0d0
      X_y_aL_z_surf2=0.0d0
      Y_x_0_z_surf2=0.0d0
      Y_x_aW_z_surf2=0.0d0
      Z_x_0_z_surf2=0.0d0
      Z_x_aW_z_surf2=0.0d0
      Z_y_0_z_surf2=0.0d0
      Z_y_aL_z_surf2=0.0d0
     
 

      print *,'allocated and zeroed'

	  start = dvtime()
      open(unit=31,file='!_A_surf.txt',form='formatted') 
      read(31,*) a_s_1
      read(31,*) a_s_2
      close(31)

      A_surf1=-dble(a_s_1)
      A_surf2=-dble(a_s_2)

      open(unit=31,file='!_Surf_sign.txt',form='formatted') 
      read(31,*) surf_sign1
      close(31)

      surf_sign=dble(surf_sign1)

       open(unit=31,file='!_hx1_hx2_hx3_hx4.txt',form='formatted') 
       read(31,*) hx_1
       read(31,*) hx_2
       read(31,*) hx_3
       read(31,*) hx_4
       close(31)
!       hx_1=1.0d0
!       hx_2=5.0d0
!       hx_3=1.0d0
!       hx_4=5.0d0	   
      

      hx1=dble(hx_1)
      hx2=dble(hx_2)
      hx3=dble(hx_3)
      hx4=dble(hx_4)

      aaa1=hx1+0.5d0*(hx2-hx1)
      aaa2=hx3+0.5d0*(hx4-hx3)

      i2= floor((aaa1-0.5*(hx2-hx1))/(0.5*d))+1
      i3= floor((aaa1+0.5*(hx2-hx1))/(0.5*d))+1
      j2= floor((aaa2-0.5*(hx4-hx3))/(0.5*d))+1
      j3= floor((aaa2+0.5*(hx4-hx3))/(0.5*d))+1  
!      i2=1
!     i3=2*M+1
      
      hx1=0.5d0*d*(i2-1)
      hx2=0.5d0*d*(i3-1)
      aaa1=hx1+0.5d0*(hx2-hx1)

      hx3=0.5d0*d*(j2-1)
      hx4=0.5d0*d*(j3-1)
      aaa2=hx3+0.5d0*(hx4-hx3)


       write(*,*) j2, ' ', j3,' ', i2, ' ', i3
      write(*,*) 2*M+2,' ' ,2*L+2
      do j=j2-160,j3-160 ! CHANGE Z FOR REAL FORMULA
      do i=i2-160,i3-160


      surf_Z(i,j)=
     &surf_sign*( (-A_surf1+   A_surf1*
     &((3.0/4.0)*cos((2.0*pi/(hx2-hx1))*((i-1)*0.5*d-aaa1))
     &+(1.0/4.0)*cos((2.0*3.0*pi/(hx2-hx1))*((i-1)*0.5*d-aaa1))))*
     &            (-A_surf2+   A_surf2*
     &((3.0/4.0)*cos((2.0*pi/(hx4-hx3))*((j-1)*0.5*d-aaa2))
     &+(1.0/4.0)*cos((2.0*3.0*pi/(hx4-hx3))*((j-1)*0.5*d-aaa2)))))


      end do
      end do

      do j=1,2*L+1
      do i=1,i2-1
      surf_Z(i,j)=surf_Z(i2,j)
      end do

      do i=i3+1,2*M+1
      surf_Z(i,j)=surf_Z(i3,j)
      end do
      end do

      do i=1,2*M+1
      do j=1,j2-1
      surf_Z(i,j)=surf_Z(i,j2)
      end do

      do j=j3+1,2*L+1
      surf_Z(i,j)=surf_Z(i,j3)
      end do
      end do

      surf_Z(1,:)=surf_Z(2,:)
      surf_Z(2*M+1,:)=surf_Z(2*M,:)
      surf_Z(:,1)=surf_Z(:,2)
      surf_Z(:,2*L+1)=surf_Z(:,2*L)
      
      surf_Z(0,:)=surf_Z(1,:)
      surf_Z(2*M+2,:)=surf_Z(2*M+1,:)
      surf_Z(:,0)=surf_Z(:,1)
      surf_Z(:,2*L+2)=surf_Z(:,2*L+1)


      open(35,file='Surface_Mountain.sct',form='binary',
     &status='unknown')
      do j=1,2*L+1
      do i=1,2*M+1
      write(35) real(surf_Z(i,j))
      end do
      end do
      close(35)


      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3
      
      dZ_dq1=rd*(surf_Z(i+1,j)-surf_Z(i-1,j))
      dZ_dq2=rd*(surf_Z(i,j+1)-surf_Z(i,j-1))
      
      sqr=sqrt(dZ_dq1*dZ_dq1+dZ_dq2*dZ_dq2+1.0d0)
      
      a_nx=-dZ_dq1/sqr
      a_ny=-dZ_dq2/sqr
      a_nz=1.0d0/sqr

      
      surf2_X(i,j)=(i-1)*0.5d0*d+d*a_nx
      surf2_Y(i,j)=(j-1)*0.5d0*d+d*a_ny
!      surf2_Z(i,j)=surf_Z(i,j)+d*a_nz*(aH-surf_Z(i,j))/aH
      surf2_Z(i,j)=surf_Z(i,j)+d*a_nz 
      end do
      end do

!DVM$ REGION targets(host)
!DVM$ PARALLEL (k,j,i) ON X(i,j,k)    
      do k=1,1
      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3
      X(i,j,1)=(i-1)*0.5d0*d
      Y(i,j,1)=(j-1)*0.5d0*d
      Z(i,j,1)=surf_Z(i,j)
      end do
      end do
      end do
!DVM$ END REGION

!-- Surface ----------------------------------------------------------- 
     
      

      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3
      
      dZ_dq1=rd*(surf_Z(i+1,j)-surf_Z(i-1,j))
      dZ_dq2=rd*(surf_Z(i,j+1)-surf_Z(i,j-1))
      
      sqr=sqrt(dZ_dq1*dZ_dq1+dZ_dq2*dZ_dq2+1.0d0)
      
      a_nx=-dZ_dq1/sqr
      a_ny=-dZ_dq2/sqr
      a_nz=1.0d0/sqr

      surf2_X(i,j)=(i-1)*0.5d0*d+d*a_nx
      surf2_Y(i,j)=(j-1)*0.5d0*d+d*a_ny
      surf2_Z(i,j)=surf_Z(i,j)+d*a_nz

! Derivatives on 3rd layer
      dX_dq3_surf2(i,j)=rd*(3.0d0*surf2_X(i,j)-
     &4.0d0*(0.5d0*(i-1)*0.5d0*d+0.5d0*surf2_X(i,j))+(i-1)*0.5d0*d)
      dY_dq3_surf2(i,j)=rd*(3.0d0*surf2_Y(i,j)-
     &4.0d0*(0.5d0*(j-1)*0.5d0*d+0.5d0*surf2_Y(i,j))+(j-1)*0.5d0*d)
      dZ_dq3_surf2(i,j)=rd*(3.0d0*surf2_Z(i,j)-
     &4.0d0*(0.5d0*surf_Z(i,j)+0.5d0*surf2_Z(i,j))+surf_Z(i,j))
     
      end do
      end do

!DVM$ REGION targets(host)
!DVM$ PARALLEL (k,j,i) ON X(i,j,k),
!DVM$& PRIVATE(dz_dq1, dz_dq2, sqr, a_nx, a_ny, a_nz)
      do k=-1,3
      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3
      
      dZ_dq1=rd*(surf_Z(i+1,j)-surf_Z(i-1,j))
      dZ_dq2=rd*(surf_Z(i,j+1)-surf_Z(i,j-1))
      
      sqr=sqrt(dZ_dq1*dZ_dq1+dZ_dq2*dZ_dq2+1.0d0)
      
      a_nx=-dZ_dq1/sqr
      a_ny=-dZ_dq2/sqr
      a_nz=1.0d0/sqr
      if(k .eq. 2) then
      X(i,j,2)=(i-1)*0.5d0*d+0.5d0*d*a_nx
      Y(i,j,2)=(j-1)*0.5d0*d+0.5d0*d*a_ny
      Z(i,j,2)=surf_Z(i,j)+0.5d0*d*a_nz
      endif

      if (k .eq. 0) then
      X(i,j,0)=(i-1)*0.5d0*d-0.5d0*d*a_nx
      Y(i,j,0)=(j-1)*0.5d0*d-0.5d0*d*a_ny
      Z(i,j,0)=surf_Z(i,j)-0.5d0*d*a_nz 
      endif

      if (k .eq. -1) then
      X(i,j,-1)=((i-1)*0.5d0*d-0.5d0*d*a_nx)-0.5d0*d*a_nx
      Y(i,j,-1)=((j-1)*0.5d0*d-0.5d0*d*a_ny)-0.5d0*d*a_ny
      Z(i,j,-1)=(surf_Z(i,j)-0.5d0*d*a_nz)-0.5d0*d*a_nz 
      endif

      if(k .eq. 3) then
      X(i,j,3)=(i-1)*0.5d0*d+d*a_nx
      Y(i,j,3)=(j-1)*0.5d0*d+d*a_ny
      Z(i,j,3)=surf_Z(i,j)+d*a_nz
      endif
      end do
      end do
      end do
!DVM$ END REGION

      print *,'surface and 2 layers'
     
    
!----------------------------------------------------------------------

!-- Edges of cube -----------------------------------------------------

!DVM$ REGION targets(host)
! x=const
!DVM$ PARALLEL (k,j,i) ON X(i,j,k)
      do k = 2 * N_down_z - 3, 2 * N_up_z + 3
         do j = 2 * N_down_y - 3, 2 * N_up_y + 3
         do i=1,1
           X(1,j,k)=0.0d0
           Y(1,j,k)=0.5d0*d*(j-1)
           if(k > 3) then
              Z(1,j,k)=surf_Z(1,j)+0.5d0*d*(k-1)*(aH-surf_Z(1,j))/aH    ! change depanding on height
           end if
         end do
         end do
      end do

!DVM$ PARALLEL (k,j,i) ON X(i,j,k)
      do k=2 * N_down_z - 3, 2 * N_up_z + 3
         do j=2 * N_down_y - 3, 2 * N_up_y + 3
         do i=2*M+1,2*M+1
            X(2*M+1,j,k)=aW
            Y(2*M+1,j,k)=0.5d0*d*(j-1)
            if(k>3) then
               Z(2*M+1,j,k)=surf_Z(2*M+1,j)+0.5d0*d*(k-1)*
     &                      (aH-surf_Z(2*M+1,j))/aH  ! change depanding on height
            end if
         end do
         end do
      end do

!------
! y=const
!DVM$ PARALLEL (k,j,i) ON X(i,j,k)
      do k=2*N_down_z-3,2*N_up_z+3
         do j=1,1
         do i=2*N_down_x-3,2*N_up_x+3
            X(i,1,k)=0.5d0*d*(i-1)
            Y(i,1,k)=0.0d0
            if(k>3) then
               Z(i,1,k)=surf_Z(i,1)+0.5d0*d*(k-1)*(aH-surf_Z(i,1))/aH     ! change depanding on height
            end if
         end do
         end do
      end do

!DVM$ PARALLEL (k,j,i) ON X(i,j,k)
      do k=2*N_down_z-3,2*N_up_z+3
        do j=2*L+1,2*L+1
         do i=2*N_down_x-3,2*N_up_x+3
            X(i,2*L+1,k)=0.5d0*d*(i-1)
            Y(i,2*L+1,k)=aL
            if(k>3) then
              Z(i,2*L+1,k)=surf_Z(i,2*L+1)+0.5d0*d*(k-1)*
     &                     (aH-surf_Z(i,2*L+1))/aH     ! change depanding on height
            end if
         end do
         end do
      end do

!-------
! z=aH
!DVM$ PARALLEL (k,j,i) ON X(i,j,k)
      do k=2*N+1,2*N+1
      do j=2*N_down_y-3,2*N_up_y+3
        do i=2*N_down_x-3,2*N_up_x+3
           X(i,j,2*N+1)=(i-1)*0.5d0*d
           Y(i,j,2*N+1)=(j-1)*0.5d0*d
           Z(i,j,2*N+1)=aH
         end do 
      end do
      end do
!DVM$ END REGION
!-------
!----------------------------------------------------------------------
      coef=aH-d

      open(unit=31,file='!_a3_11_a3_21_degree.txt',form='formatted') 
      read(31,*) i_deg
      close(31)

      do i=1,2*M+1
      a1_10(i)=(1.0d0+2.0d0*dble(i-1)/dble(2*M))*
     &             (dble(i-1)/dble(2*M)-1.0d0)**2
      a1_20(i)=1.0d0-a1_10(i)
      a1_11(i)=dble(i-1)/dble(2*M)*
     &             (1.0d0-dble(i-1)/dble(2*M))**2
      a1_21(i)=(dble(i-1)/dble(2*M)-1.0d0)*
     &                   (dble(i-1)/dble(2*M))**2
      end do

      do j=1,2*L+1
      a2_10(j)=(1.0d0+2.0d0*dble(j-1)/dble(2*L))*
     &             (dble(j-1)/dble(2*L)-1.0d0)**2
      a2_20(j)=1.0d0-a2_10(j)
      a2_11(j)=dble(j-1)/dble(2*L)*
     &             (1.0d0-dble(j-1)/dble(2*L))**2
      a2_21(j)=(dble(j-1)/dble(2*L)-1.0d0)*
     &                   (dble(j-1)/dble(2*L))**2
      end do

      do k=3,2*N+1
      a3_10(k)=(1.0d0+2.0d0*dble(k-3)/dble(2*N-2))*
     &             (dble(k-3)/dble(2*N-2)-1.0d0)**2
      a3_20(k)=1.0d0-a3_10(k)
      a3_11(k)=dble(k-3)/dble(2*N-2)*
     &         (1.0d0-dble(k-3)/dble(2*N-2))**i_deg*coef
      a3_21(k)=(dble(k-3)/dble(2*N-2)-1.0d0)*
     &         (dble(k-3)/dble(2*N-2))**i_deg*coef
      end do

!- Essential formula ---------------------------------------------------

      do k=2*N_down_z-3,2*N_up_z+3
      do j=2*N_down_y-4,2*N_up_y+4
      Y_x_0(j,k)=0.5d0*d*(j-1)
      Z_x_0(j,k)=surf_Z(1,j)+0.5d0*d*(k-1)*(aH-surf_Z(1,j))/aH
      end do
      end do
      
      do k=2*N_down_z-3,2*N_up_z+3
      do j=2*N_down_y-4,2*N_up_y+4
      Y_x_aW(j,k)=0.5d0*d*(j-1)
      Z_x_aW(j,k)=surf_Z(2*M+1,j)+0.5d0*d*(k-1)*(aH-surf_Z(2*M+1,j))/aH
      end do
      end do

      do j=1,2*L+1
      Y_x_0_z_surf2(j)=0.5d0*d*(j-1)
      Y_x_aW_z_surf2(j)=0.5d0*d*(j-1)
      Z_x_0_z_surf2(j)=surf_Z(1,j)+0.5d0*d*(3-1)*(aH-surf_Z(1,j))/aH
      Z_x_aW_z_surf2(j)=surf_Z(2*M+1,j)+0.5d0*d*(3-1)*
     &                  (aH-surf_Z(2*M+1,j))/aH
      end do

      do k=2*N_down_z-3,2*N_up_z+3
      do i=2*N_down_x-4,2*N_up_x+4
      X_y_0(i,k)=0.5d0*d*(i-1)
      Z_y_0(i,k)=surf_Z(i,1)+0.5d0*d*(k-1)*(aH-surf_Z(i,1))/aH
      end do
      end do

      do k=2*N_down_z-3,2*N_up_z+3
      do i=2*N_down_x-4,2*N_up_x+4
      X_y_aL(i,k)=0.5d0*d*(i-1)
      Z_y_aL(i,k)=surf_Z(i,2*L+1)+0.5d0*d*(k-1)*(aH-surf_Z(i,2*L+1))/aH
      end do
      end do

      do i=1,2*M+1
      X_y_0_z_surf2(i)=0.5d0*d*(i-1)
      X_y_aL_z_surf2(i)=0.5d0*d*(i-1)
      Z_y_0_z_surf2(i)=surf_Z(i,1)+0.5d0*d*(3-1)*(aH-surf_Z(i,1))/aH
      Z_y_aL_z_surf2(i)=surf_Z(i,2*L+1)+0.5d0*d*(3-1)*
     &                  (aH-surf_Z(i,2*L+1))/aH
      end do

      do k=2*N_down_z-3,2*N_up_z+3
      do i=2*N_down_x-3,2*N_up_x+3
      F1x_y_0(i,k)=a1_10(i)*0.0d0+a1_11(i)*1.0d0+
     &             a1_20(i)*aW+a1_21(i)*1.0d0
      F1x_y_aL(i,k)=a1_10(i)*0.0d0+a1_11(i)*1.0d0+
     &             a1_20(i)*aW+a1_21(i)*1.0d0

      F1y_y_0(i,k)=a1_10(i)*0.0d0+a1_11(i)*0.0d0+
     &             a1_20(i)*0.0d0+a1_21(i)*0.0d0
      F1y_y_aL(i,k)=a1_10(i)*aL+a1_11(i)*0.0d0+
     &              a1_20(i)*aL+a1_21(i)*0.0d0

      F1z_y_0(i,k)=a1_10(i)*
     &            (surf_Z(1,1)+0.5d0*d*(k-1)*(aH-surf_Z(1,1))/aH)+
     &             a1_11(i)*0.0d0+                ! Change depanding on height
     &             a1_20(i)*
     &          (surf_Z(2*M+1,1)+0.5d0*d*(k-1)*(aH-surf_Z(2*M+1,1))/aH)+
     &             a1_21(i)*0.0d0
      F1z_y_aL(i,k)=a1_10(i)*
     &          (surf_Z(1,2*L+1)+0.5d0*d*(k-1)*(aH-surf_Z(1,2*L+1))/aH)+
     &              a1_11(i)*0.0d0+                ! Change depanding on height
     &              a1_20(i)*
     &  (surf_Z(2*M+1,2*L+1)+0.5d0*d*(k-1)*(aH-surf_Z(2*M+1,2*L+1))/aH)+
     &              a1_21(i)*0.0d0                 ! Change depanding on height
      end do
      end do

      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3
      
      F2x_z_surf2(i,j)=(a1_11(i)+a1_20(i)*aW+a1_21(i))+           !F1x surf2_Z
     & a2_10(j)*(X_y_0_z_surf2(i)-(a1_11(i)+a1_20(i)*aW+a1_21(i)))+
     & a2_11(j)*0.0d0+
     & a2_20(j)*(X_y_aL_z_surf2(i)-(a1_11(i)+a1_20(i)*aW+a1_21(i)))+
     & a2_21(j)*0.0d0
      
      F2x_z_aH(i,j)=(a1_11(i)+a1_20(i)*aW+a1_21(i))+           !F1x aH
     & a2_10(j)*((i-1)*0.5d0*d-(a1_11(i)+a1_20(i)*aW+a1_21(i)))+
     & a2_11(j)*0.0d0+
     & a2_20(j)*((i-1)*0.5d0*d-(a1_11(i)+a1_20(i)*aW+a1_21(i)))+
     & a2_21(j)*0.0d0

      F2y_z_surf2(i,j)=(a1_10(i)*Y_x_0_z_surf2(j)+
     &                  a1_20(i)*Y_x_aW_z_surf2(j)) !F1y surf2_Z
     &+a2_10(j)*(0.0d0-(a1_10(i)*Y_x_0_z_surf2(1)+
     &                a1_20(i)*Y_x_aW_z_surf2(1)))+
     & a2_11(j)*0.0d0+
     & a2_20(j)*
     & (aL-(a1_10(i)*Y_x_0_z_surf2(2*L+1)+
     &      a1_20(i)*Y_x_aW_z_surf2(2*L+1)))+
     & a2_21(j)*0.0d0

      F2y_z_aH(i,j)=(a1_10(i)*0.5d0*d*(j-1)+a1_20(i)*0.5d0*d*(j-1))          !F1y aH
     &+a2_10(j)*(0.0d0-(a1_10(i)*0.5d0*d*(1-1)+a1_20(i)*0.5d0*d*(1-1)))+
     & a2_11(j)*0.0d0+
     & a2_20(j)*
     & (aL-(a1_10(i)*0.5d0*d*(2*L+1-1)+a1_20(i)*0.5d0*d*(2*L+1-1)))+
     & a2_21(j)*0.0d0

      F2z_z_surf2(i,j)=(a1_10(i)*Z_x_0_z_surf2(j)+
     &                  a1_20(i)*Z_x_aW_z_surf2(j))
     &+a2_10(j)*
     & (Z_y_0_z_surf2(i)-(a1_10(i)*Z_x_0_z_surf2(1)+
     &                    a1_20(i)*Z_x_aW_z_surf2(1)))+
     & a2_11(j)*0.0d0+                ! Change depanding on height
     & a2_20(j)*
     & (Z_y_aL_z_surf2(i)-
     & (a1_10(i)*Z_x_0_z_surf2(2*L+1)+a1_20(i)*Z_x_aW_z_surf2(2*L+1)))+
     & a2_21(j)*0.0d0 

      F2z_z_aH(i,j)=(a1_10(i)*aH+a1_20(i)*aH)
     &+a2_10(j)*
     & (aH-(a1_10(i)*aH+a1_20(i)*aH))+
     & a2_11(j)*0.0d0+                ! Change depanding on height
     & a2_20(j)*
     & (aH-(a1_10(i)*aH+a1_20(i)*aH))+
     & a2_21(j)*0.0d0                 ! Change depanding on height
      end do
      end do

!=======================================================================

!DVM$ REGION targets(host)
!DVM$ PARALLEL (k,j,i) ON X(i,j,k), PRIVATE(f1x, f1y, f1z, df1x_dq2, 
!DVM$& df1y_dq2, df1z_dq2, f2x, f2y, f2z, df1x_dq3, df1y_dq3, df1z_dq3,
!DVM$& df1x_dq2dq3, df1y_dq2dq3, df1z_dq2dq3, df2x_dq3, df2y_dq3, 
!DVM$& df2z_dq3)
      do k=4,2*N_up_z+3
      do j=2*N_down_y-3,2*N_up_y+3
      do i=2*N_down_x-3,2*N_up_x+3

      F1x=a1_10(i)*0.0d0+a1_11(i)*1.0d0+a1_20(i)*aW+a1_21(i)*1.0d0
      F1y=a1_10(i)*Y_x_0(j,k)+a1_11(i)*0.0d0+
     &    a1_20(i)*Y_x_aW(j,k)+a1_21(i)*0.0d0 
      F1z=a1_10(i)*Z_x_0(j,k)+a1_11(i)*0.0d0+                ! Change depanding on height
     &    a1_20(i)*Z_x_aW(j,k)+a1_21(i)*0.0d0                 ! Change depanding on height
 
      dF1x_dq2=0.0d0
      dF1y_dq2=a1_10(i)*rd*(Y_x_0(j+1,k)-Y_x_0(j-1,k))+
     &         a1_11(i)*0.0d0+
     &         a1_20(i)*rd*(Y_x_aW(j+1,k)-Y_x_aW(j-1,k))+
     &         a1_21(i)*0.0d0
      dF1z_dq2=a1_10(i)*rd*(Z_x_0(j+1,k)-Z_x_0(j-1,k))+
     &         a1_11(i)*0.0d0+                                          ! Change depanding on height
     &         a1_20(i)*rd*(Z_x_aW(j+1,k)-Z_x_aW(j-1,k))+
     &         a1_21(i)*0.0d0
  
      F2x=F1x+a2_10(j)*(X_y_0(i,k)-F1x_y_0(i,k))+a2_11(j)*0.0d0+
     &        a2_20(j)*(X_y_aL(i,k)-F1x_y_aL(i,k))+a2_21(j)*0.0d0

      F2y=F1y+a2_10(j)*(0.0d0-F1y_y_0(i,k))+a2_11(j)*(1.0d0-1.0d0)+
     &        a2_20(j)*(aL-F1y_y_aL(i,k))+a2_21(j)*(1.0d0-1.0d0)

      F2z=F1z+a2_10(j)*(Z_y_0(i,k)-F1z_y_0(i,k))+a2_11(j)*(0.0d0-0.0d0)+  ! Change depanding on height
     &       a2_20(j)*(Z_y_aL(i,k)-F1z_y_aL(i,k))+a2_21(j)*(0.0d0-0.0d0) ! Change depanding on height

      dF1x_dq3=0.0d0
      dF1y_dq3=0.0d0
      dF1z_dq3=a1_10(i)*(aH-surf_Z(1,j))/aH+a1_11(i)*0.0d0+
     &         a1_20(i)*(aH-surf_Z(2*M+1,j))/aH+a1_21(i)*0.0d0            ! Change depanding on height   
 
      dF1x_dq2dq3=0.0d0
      dF1y_dq2dq3=0.0d0
      dF1z_dq2dq3=a1_10(i)*(-(surf_Z(1,j+1)-surf_Z(1,j-1))/aH)+
     &            a1_20(i)*(-(surf_Z(2*M+1,j+1)-surf_Z(2*M+1,j-1))/aH)       ! Change depanding on height

      dF2x_dq3=dF1x_dq3+a2_10(j)*(0.0d0-dF1x_dq3)+a2_11(j)*0.0d0+
     &                  a2_20(j)*(0.0d0-dF1x_dq3)+a2_21(j)*0.0d0
      dF2y_dq3=dF1y_dq3+a2_10(j)*(0.0d0-dF1y_dq3)+a2_11(j)*0.0d0+
     &                  a2_20(j)*(0.0d0-dF1y_dq3)+a2_21(j)*0.0d0
      dF2z_dq3=dF1z_dq3+a2_10(j)*((aH-surf_Z(i,1))/aH-dF1z_dq3)+
     &                  a2_11(j)*0.0d0+         ! Change depanding on height
     &                  a2_20(j)*((aH-surf_Z(i,2*L+1))/aH-1.0d0)+
     &                  a2_21(j)*0.0d0


      X(i,j,k)=F2x+a3_10(k)*(surf2_X(i,j)-F2x)+
     &             a3_11(k)*(dX_dq3_surf2(i,j)-dF2x_dq3)+
     &            a3_20(k)*((i-1)*0.5d0*d-F2x)+a3_21(k)*(0.0d0-dF2x_dq3)   !X(i,j,2*N+1)
      Y(i,j,k)=F2y+a3_10(k)*(surf2_Y(i,j)-F2y)+
     &             a3_11(k)*(dY_dq3_surf2(i,j)-dF2y_dq3)+
     &            a3_20(k)*((j-1)*0.5d0*d-F2y)+a3_21(k)*(0.0d0-dF2y_dq3)   !Y(i,j,2*N+1)
      Z(i,j,k)=F2z+a3_10(k)*(surf2_Z(i,j)-F2z_z_surf2(i,j))+                 ! !!!!!!!!!!!!!!!!!!!!!!!!
     &             a3_11(k)*(dZ_dq3_surf2(i,j)-dF2z_dq3)+
     &             a3_20(k)*(aH-F2z_z_aH(i,j))+a3_21(k)*(1.0d0-1.0d0)            !Z(i,j,2*N+1) 
      end do
      end do
      end do
!DVM$ END REGION

!=======================================================================

      print *,'Grid is calculated'

      finish = dvtime()
      print '("Grid calc time =",f8.1," seconds")',finish-start

!===========================================================================================================
!===========================================================================================================












!===========================================================================================================

      allocate(
     & alambda
     & (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & amu
     & (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & Rho
     & (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & ttoRhoJ
     & (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),     

     & dq2_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),

     & A1(N_down_x:N_up_x,N_down_y:N_up_y),
     & A2(N_down_x:N_up_x,N_down_y:N_up_y),
     & A3(N_down_x:N_up_x,N_down_y:N_up_y),
     & B1(N_down_x:N_up_x,N_down_y:N_up_y),
     & B2(N_down_x:N_up_x,N_down_y:N_up_y),
     & B3(N_down_x:N_up_x,N_down_y:N_up_y),
     & C1(N_down_x:N_up_x,N_down_y:N_up_y),
     & C2(N_down_x:N_up_x,N_down_y:N_up_y),
     & C3(N_down_x:N_up_x,N_down_y:N_up_y),
    
     & R1_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_6(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_6(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_6(N_down_x:N_up_x,N_down_y:N_up_y),

     & dz_dq1_surf(N_down_x:N_up_x,N_down_y:N_up_y),
     & dz_dq2_surf(N_down_x:N_up_x,N_down_y:N_up_y)) 


      alambda=0.0d0 ! lambda
      amu=0.0d0     ! mu
      Rho=0.0d0     ! density

      ttoRhoJ=0.0d0 ! Jacobian

      dq1_dx=1.0d0  ! Partial direvatives
      dq2_dx=0.0d0
      dq3_dx=0.0d0
      dq1_dy=0.0d0
      dq2_dy=1.0d0
      dq3_dy=0.0d0
      dq1_dz=0.0d0
      dq2_dz=0.0d0
      dq3_dz=1.0d0

      A1=0.0d0
      A2=0.0d0
      A3=0.0d0
      B1=0.0d0
      B2=0.0d0
      B3=0.0d0
      C1=0.0d0
      C2=0.0d0
      C3=0.0d0

      R1_1=0.0d0
      R1_2=0.0d0
      R1_3=0.0d0
      R1_4=0.0d0
      R1_5=0.0d0
      R1_6=0.0d0
      R2_1=0.0d0
      R2_2=0.0d0
      R2_3=0.0d0
      R2_4=0.0d0
      R2_5=0.0d0
      R2_6=0.0d0
      R3_1=0.0d0
      R3_2=0.0d0
      R3_3=0.0d0
      R3_4=0.0d0
      R3_5=0.0d0
      R3_6=0.0d0

      dz_dq1_surf=0.0d0
      dz_dq2_surf=0.0d0
      
!================================================================================
         ! reading from file X,Y,Z

         !Cartesian grid
!         do k=2*N_down_z-3,2*N_up_z+3
!         do j=2*N_down_y-3,2*N_up_y+3
!         do i=2*N_down_x-3,2*N_up_x+3
!         X(i,j,k)=(i-1)*d*0.5d0
!         Y(i,j,k)=(j-1)*d*0.5d0
!         Z(i,j,k)=(k-1)*d*0.5d0
!         end do
!         end do
!         end do


!================= Source position ====================                         
      

      print *,'finding x0,y0,z0'
      write(*,'(a,f6.3)') 'x0= ',x0
      write(*,'(a,f6.3)') 'y0= ',y0
      write(*,'(a,f6.3)') 'z0= ',z0
      print *,''

      amax=1.0d0
      newSources=0
!DVM$ REGION targets(host)
!DVM$ PARALLEL (k,j,i) ON X(2*i,2*j,2*k),
!DVM$& REDUCTION(MINLOC(amax,newSources,3))
      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x

      if(((X(2*i,2*j,2*k)-x0)**2 +
     &    (Y(2*i,2*j,2*k)-y0)**2 +
     &    (Z(2*i,2*j,2*k)-z0)**2 < amax)) then

      amax = (X(2*i,2*j,2*k)-x0)**2 +
     &       (Y(2*i,2*j,2*k)-y0)**2 +
     &       (Z(2*i,2*j,2*k)-z0)**2

      newSources(1)=i
      newSources(2)=j
      newSources(3)=k

      end if

      end do
      end do
      end do
!DVM$ END REGION
      i_source = newSources(1)
      j_source = newSources(2)
      k_source = newSources(3)
      if(i_source==N_down_x)  i_source=i_source+2	  
      if(i_source==N_down_x+1)i_source=i_source+1      	  
      if(i_source==N_up_x)    i_source=i_source-2      
      if(i_source==N_up_x-1)  i_source=i_source-1      
      if(j_source==N_down_y)  j_source=j_source+2      
      if(j_source==N_down_y+1)j_source=j_source+1      
      if(j_source==N_up_y)    j_source=j_source-2      
      if(j_source==N_up_y-1)  j_source=j_source-1      
      if(k_source==N_down_z)  k_source=k_source+2      
      if(k_source==N_down_z+1)k_source=k_source+1      
      if(k_source==N_up_z)    k_source=k_source-2      
      if(k_source==N_up_z-1)  k_source=k_source-1      

      print *,'new source coordinates' 
!DVM$ REMOTE_ACCESS(X(2*i_source,2*j_source,2*k_source))	  
      tmpSources(1) = X(2*i_source,2*j_source,2*k_source)
!DVM$ REMOTE_ACCESS(Y(2*i_source,2*j_source,2*k_source))
      tmpSources(2) = Y(2*i_source,2*j_source,2*k_source)
!DVM$ REMOTE_ACCESS(Z(2*i_source,2*j_source,2*k_source)	  )
      tmpSources(3) = Z(2*i_source,2*j_source,2*k_source)	  
      write(*,'(a,f7.4)') 'x0= ', tmpSources(1)
      write(*,'(a,f7.4)') 'y0= ', tmpSources(2)
      write(*,'(a,f7.4)') 'z0= ', tmpSources(3)
      print *,''
      print *,'i,j,k source=',i_source,j_source,k_source
	  
!===========================================================================================================================================================
!===========================================================================================================================================================
!================= Medium parameters ==================
      
!DVM$ REGION targets(host)
!DVM$ PARALLEL (k,j,i) on amu(i,j,k)  	  
      do k=N_down_z-1,N_up_z+1
      do j=N_down_y-1,N_up_y+1
      do i=N_down_x-1,N_up_x+1
        if(k>=N/3) then
           alambda(i,j,k)=0.5d0
           amu(i,j,k)=0.25d0
           Rho(i,j,k)=1.0d0
        else
           alambda(i,j,k)=0.5d0
           amu(i,j,k)=0.25d0
           Rho(i,j,k)=1.0d0
        end if
      end do
      end do
      end do
!DVM$ END REGION
  
      print *,'lambda, mu, rho are done'
      print *,''
       
!======================================================
!================= Metrical coefficients ==============


!--------------------------- Jacobian -----------------

! private(dx_dq1,
! DVM$& dx_dq2,dx_dq3,dy_dq1,dy_dq2,dy_dq3,dz_dq1,dz_dq2,
! DVM$& dz_dq3,aJac),

!DVM$ REGION targets(host)

!DVM$ PARALLEL (k,j,i) on Rho(i,j,k), SHADOW_RENEW(X,Y,Z),
!DVM$& PRIVATE(dx_dq1,dx_dq2,dx_dq3,dy_dq1,dy_dq2,dy_dq3,
!DVM$& dz_dq1,dz_dq2,dz_dq3,aJac)
      do k=N_down_z,N_up_z
      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x	  
        dx_dq1=rd*(X(2*i+1,2*j,2*k)-X(2*i-1,2*j,2*k))
        dx_dq2=rd*(X(2*i,2*j+1,2*k)-X(2*i,2*j-1,2*k))
        dx_dq3=rd*(X(2*i,2*j,2*k+1)-X(2*i,2*j,2*k-1))

        dy_dq1=rd*(Y(2*i+1,2*j,2*k)-Y(2*i-1,2*j,2*k))
        dy_dq2=rd*(Y(2*i,2*j+1,2*k)-Y(2*i,2*j-1,2*k))
        dy_dq3=rd*(Y(2*i,2*j,2*k+1)-Y(2*i,2*j,2*k-1))

        dz_dq1=rd*(Z(2*i+1,2*j,2*k)-Z(2*i-1,2*j,2*k))
        dz_dq2=rd*(Z(2*i,2*j+1,2*k)-Z(2*i,2*j-1,2*k))
        dz_dq3=rd*(Z(2*i,2*j,2*k+1)-Z(2*i,2*j,2*k-1))
!     WARN: not used
        aJac=dx_dq1*dy_dq2*dz_dq3+
     &     dx_dq2*dy_dq3*dz_dq1+
     &     dx_dq3*dy_dq1*dz_dq2-
     &     dx_dq3*dy_dq2*dz_dq1-
     &     dx_dq2*dy_dq1*dz_dq3-
     &     dx_dq1*dy_dq3*dz_dq2

        ttoRhoJ(i,j,k)=dt*dt/(Rho(i,j,k))

      end do
      end do
      end do

!------------------------------------------------------
!-------------------------- Partials ------------------
!DVM$ PARALLEL (k,j,i) on dq1_dx(i,j,k), private(dx_dq1,
!DVM$& dx_dq2,dx_dq3,dy_dq1,dy_dq2,dy_dq3,dz_dq1,dz_dq2,
!DVM$& dz_dq3,aJac),SHADOW_RENEW(X,Y,Z)
      do k=2*N_down_z-2,2*N_up_z+2
      do j=2*N_down_y-2,2*N_up_y+2
      do i=2*N_down_x-2,2*N_up_x+2	  
      dx_dq1=rd*(X(i+1,j,k)-X(i-1,j,k))
      dx_dq2=rd*(X(i,j+1,k)-X(i,j-1,k))
      dx_dq3=rd*(X(i,j,k+1)-X(i,j,k-1))

      dy_dq1=rd*(Y(i+1,j,k)-Y(i-1,j,k))
      dy_dq2=rd*(Y(i,j+1,k)-Y(i,j-1,k))
      dy_dq3=rd*(Y(i,j,k+1)-Y(i,j,k-1))

      dz_dq1=rd*(Z(i+1,j,k)-Z(i-1,j,k))
      dz_dq2=rd*(Z(i,j+1,k)-Z(i,j-1,k))
      dz_dq3=rd*(Z(i,j,k+1)-Z(i,j,k-1))

      aJac=dx_dq1*dy_dq2*dz_dq3+
     &     dx_dq2*dy_dq3*dz_dq1+
     &     dx_dq3*dy_dq1*dz_dq2-
     &     dx_dq3*dy_dq2*dz_dq1-
     &     dx_dq2*dy_dq1*dz_dq3-
     &     dx_dq1*dy_dq3*dz_dq2


      dq1_dx(i,j,k)=(1.0d0/aJac)*(dy_dq2*dz_dq3-dy_dq3*dz_dq2)
      dq2_dx(i,j,k)=(1.0d0/aJac)*(dy_dq3*dz_dq1-dy_dq1*dz_dq3)
      dq3_dx(i,j,k)=(1.0d0/aJac)*(dy_dq1*dz_dq2-dy_dq2*dz_dq1)
      
      dq1_dy(i,j,k)=(1.0d0/aJac)*(dz_dq2*dx_dq3-dz_dq3*dx_dq2)
      dq2_dy(i,j,k)=(1.0d0/aJac)*(dz_dq3*dx_dq1-dz_dq1*dx_dq3)
      dq3_dy(i,j,k)=(1.0d0/aJac)*(dz_dq1*dx_dq2-dz_dq2*dx_dq1)

      dq1_dz(i,j,k)=(1.0d0/aJac)*(dx_dq2*dy_dq3-dx_dq3*dy_dq2)
      dq2_dz(i,j,k)=(1.0d0/aJac)*(dx_dq3*dy_dq1-dx_dq1*dy_dq3)
      dq3_dz(i,j,k)=(1.0d0/aJac)*(dx_dq1*dy_dq2-dx_dq2*dy_dq1)
      end do
      end do
      end do
      

!------------------------- Free surface coefficients -----------

!DVM$ PARALLEL (k,j,i) on amu(i,j,k), private(a_nx,a_ny,a_nz),
!DVM$& SHADOW_RENEW(alambda(corner),amu(corner),ttoRhoJ(corner),
!DVM$& dq1_dx(corner),dq2_dx(corner),dq3_dx(corner),
!DVM$& dq1_dy(corner),dq2_dy(corner),dq3_dy(corner),
!DVM$& dq1_dz(corner),dq2_dz(corner),dq3_dz(corner))
      do k=1,1
      do j=N_down_y,N_up_y
      do i=N_down_x,N_up_x	  

      a_nx=-rd*(Z(2*i+1,2*j,k)-Z(2*i-1,2*j,k))
      a_ny=-rd*(Z(2*i,2*j+1,k)-Z(2*i,2*j-1,k))
      a_nz=1.0d0
      
      A1(i,j)=a_nx*(alambda(i,j,k)+2.0d0*amu(i,j,k))*dq3_dx(2*i,2*j,k)+    
     &        a_ny*amu(i,j,k)*dq3_dy(2*i,2*j,k)+
     &        a_nz*amu(i,j,k)*dq3_dz(2*i,2*j,k)

      A2(i,j)=a_nx*alambda(i,j,k)*dq3_dy(2*i,2*j,k)+
     &        a_ny*amu(i,j,k)*dq3_dx(2*i,2*j,k) 

      A3(i,j)=a_nx*alambda(i,j,k)*dq3_dz(2*i,2*j,k)+
     &        a_nz*amu(i,j,k)*dq3_dx(2*i,2*j,k)


      B1(i,j)=a_nx*amu(i,j,k)*dq3_dy(2*i,2*j,k)+
     &        a_ny*alambda(i,j,k)*dq3_dx(2*i,2*j,k)

      B2(i,j)=a_nx*amu(i,j,k)*dq3_dx(2*i,2*j,k)+    
     &        a_ny*(alambda(i,j,k)+2.0d0*amu(i,j,k))*dq3_dy(2*i,2*j,k)+
     &        a_nz*amu(i,j,k)*dq3_dz(2*i,2*j,k)

      B3(i,j)=a_ny*alambda(i,j,k)*dq3_dz(2*i,2*j,k)+
     &        a_nz*amu(i,j,k)*dq3_dy(2*i,2*j,k)


      C1(i,j)=a_nx*amu(i,j,k)*dq3_dz(2*i,2*j,k)+
     &        a_ny*alambda(i,j,k)*dq3_dx(2*i,2*j,k)

      C2(i,j)=a_ny*amu(i,j,k)*dq3_dz(2*i,2*j,k)+
     &        a_nz*alambda(i,j,k)*dq3_dy(2*i,2*j,k)

      C3(i,j)=a_nx*amu(i,j,k)*dq3_dx(2*i,2*j,k)+    
     &        a_ny*amu(i,j,k)*dq3_dy(2*i,2*j,k)+
     &        a_nz*(alambda(i,j,k)+2.0d0*amu(i,j,k))*dq3_dz(2*i,2*j,k)


      R1_1(i,j)=a_nx*(alambda(i,j,k)+2.0d0*amu(i,j,k))*dq1_dx(2*i,2*j,k)
     &         +a_ny*amu(i,j,k)*dq1_dy(2*i,2*j,k)+
     &          a_nz*amu(i,j,k)*dq1_dz(2*i,2*j,k)

      R1_2(i,j)=a_nx*(alambda(i,j,k)+2.0d0*amu(i,j,k))*dq2_dx(2*i,2*j,k)
     &         +a_ny*amu(i,j,k)*dq2_dy(2*i,2*j,k)+
     &          a_nz*amu(i,j,k)*dq2_dz(2*i,2*j,k)

      R1_3(i,j)=a_nx*alambda(i,j,k)*dq1_dy(2*i,2*j,k)+
     &          a_ny*amu(i,j,k)*dq1_dx(2*i,2*j,k)

      R1_4(i,j)=a_nx*alambda(i,j,k)*dq2_dy(2*i,2*j,k)+
     &          a_ny*amu(i,j,k)*dq2_dx(2*i,2*j,k)

      R1_5(i,j)=a_nx*alambda(i,j,k)*dq1_dz(2*i,2*j,k)+
     &          a_nz*amu(i,j,k)*dq1_dx(2*i,2*j,k)

      R1_6(i,j)=a_nx*alambda(i,j,k)*dq2_dz(2*i,2*j,k)+
     &          a_nz*amu(i,j,k)*dq2_dx(2*i,2*j,k)


      R2_1(i,j)=a_nx*amu(i,j,k)*dq1_dy(2*i,2*j,k)+
     &          a_ny*alambda(i,j,k)*dq1_dx(2*i,2*j,k)

      R2_2(i,j)=a_nx*amu(i,j,k)*dq2_dy(2*i,2*j,k)+
     &          a_ny*alambda(i,j,k)*dq2_dx(2*i,2*j,k)

      R2_3(i,j)=a_nx*amu(i,j,k)*dq1_dx(2*i,2*j,k)+
     &          a_ny*(alambda(i,j,k)+2.0d0*amu(i,j,k))*dq1_dy(2*i,2*j,k)
     &         +a_nz*amu(i,j,k)*dq1_dz(2*i,2*j,k)

      R2_4(i,j)=a_nx*amu(i,j,k)*dq2_dx(2*i,2*j,k)+
     &          a_ny*(alambda(i,j,k)+2.0d0*amu(i,j,k))*dq2_dy(2*i,2*j,k)
     &         +a_nz*amu(i,j,k)*dq2_dz(2*i,2*j,k)

      R2_5(i,j)=a_ny*alambda(i,j,k)*dq1_dz(2*i,2*j,k)+
     &          a_nz*amu(i,j,k)*dq1_dy(2*i,2*j,k)

      R2_6(i,j)=a_ny*alambda(i,j,k)*dq2_dz(2*i,2*j,k)+
     &          a_nz*amu(i,j,k)*dq2_dy(2*i,2*j,k)


      R3_1(i,j)=a_nx*amu(i,j,k)*dq1_dz(2*i,2*j,k)+
     &          a_nz*alambda(i,j,k)*dq1_dx(2*i,2*j,k)

      R3_2(i,j)=a_nx*amu(i,j,k)*dq2_dz(2*i,2*j,k)+
     &          a_nz*alambda(i,j,k)*dq2_dx(2*i,2*j,k)

      R3_3(i,j)=a_ny*amu(i,j,k)*dq1_dz(2*i,2*j,k)+
     &          a_nz*alambda(i,j,k)*dq1_dy(2*i,2*j,k)

      R3_4(i,j)=a_ny*amu(i,j,k)*dq2_dz(2*i,2*j,k)+
     &          a_nz*alambda(i,j,k)*dq2_dy(2*i,2*j,k)

      R3_5(i,j)=a_nx*amu(i,j,k)*dq1_dx(2*i,2*j,k)+
     &          a_ny*amu(i,j,k)*dq1_dy(2*i,2*j,k)+
     &          a_nz*(alambda(i,j,k)+2.0d0*amu(i,j,k))*dq1_dz(2*i,2*j,k)

      R3_6(i,j)=a_nx*amu(i,j,k)*dq2_dx(2*i,2*j,k)+
     &          a_ny*amu(i,j,k)*dq2_dy(2*i,2*j,k)+
     &          a_nz*(alambda(i,j,k)+2.0d0*amu(i,j,k))*dq2_dz(2*i,2*j,k)

      end do      
      end do
      end do
!DVM$ END REGION

!      OPEN (3, FILE='ARR_X.DAT', FORM='FORMATTED', STATUS='UNKNOWN')
!      WRITE (3,*) X
!      CLOSE (3)
	 
!      OPEN (3, FILE='ARR_Y.DAT', FORM='FORMATTED', STATUS='UNKNOWN')
!      WRITE (3,*) Y
!      CLOSE (3)	 
 
!      OPEN (3, FILE='ARR_Z.DAT', FORM='FORMATTED', STATUS='UNKNOWN')
!      WRITE (3,*) Z
!      CLOSE (3)	 
	
!======================================================

      deallocate(X,Y,Z)
     
!------------------------------------------------------

      

!======================================================

      allocate(
     &  U(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & U1(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  V(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  W(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & UU(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & VV(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & WW(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1), 
     & F(0:iTT)
     &)
    
     
      U=0.0d0
      UU=0.0d0
      V=0.0d0
      VV=0.0d0
      W=0.0d0
      WW=0.0d0
      F=0.0d0

!================= F =================================================     
      p_s=0.0d0 
       
!DVM$ REMOTE_ACCESS(ttoRhoJ(i_source,j_source,k_source))
      p_s = 0.125d0*ttoRhoJ(i_source,j_source,k_source)/(d*d*d*d)   ! dt*dt/(Rho*J*S_pyramid)
      
      do k=1,iTT-2                                    ! f(t)
           F(k) = p_s*(dsin(pi*(k*dt-1.0D0)) + 0.8D0 * 
     &            dsin(2.0D0*pi*(k*dt-1.0D0)) + 0.2D0 *
     &            dsin(3.0D0*pi*(k*dt-1.0D0)))
      end do

      open(35,file='F.txt',form='formatted')
      write(35,*) real(F)
      close(35)
!=====================================================================

      print *,''
      print *,'Ready'
!==================================================================
!========================= Explicit ainside ========================
!==================================================================
     
      print *,''
      print *,'Start'
 
!DVM$ actual(UU,VV,WW,U,V,W,alambda,amu,ttoRhoJ,
!DVM$& dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy
!DVM$& dq3_dy,dq1_dz,dq2_dz,dq3_dz
!DVM$& A1,A2,A3,B1,B2,B3,C1,C2,C3
!DVM$& R1_1,R1_2,R1_3,R1_4,R1_5,R1_6
!DVM$& R2_1,R2_2,R2_3,R2_4,R2_5,R2_6
!DVM$& R3_1,R3_2,R3_3,R3_4,R3_5,R3_6
!DVM$& dz_dq1_surf,dz_dq2_surf)

      start = dvtime()
	  
!DVM$ interval 222
      do i_time = 0, iTT, 2
	  
        if (i_time .ge. 1 .and. i_time .le. iTT-2) then
         F_curr = p_s*(dsin(pi*(i_time*dt-1.0D0)) + 0.8D0 * 
     &            dsin(2.0D0*pi*(i_time*dt-1.0D0)) + 0.2D0 *
     &            dsin(3.0D0*pi*(i_time*dt-1.0D0)))
        else
		 F_curr = 0.0d0
        endif
	 
        if (i_time+1 .ge. 1 .and. i_time+1 .le. iTT-2) then	 
           F_next = p_s*(dsin(pi*((i_time+1)*dt-1.0D0)) + 0.8D0 * 
     &            dsin(2.0D0*pi*((i_time+1)*dt-1.0D0)) + 0.2D0 *
     &            dsin(3.0D0*pi*((i_time+1)*dt-1.0D0)))
        else
		   F_next = 0.0d0
	    endif
	
!DVM$ REGION
	
!DVM$ PARALLEL (k,j,i) on U(i,j,k)	 
       do k=k_source,k_source
	   do j=j_source,j_source
	   do i=i_source-1,i_source+1, 2
	      if (i .eq. i_source-1) then
            U(i,j,k) = U(i,j,k) + F_curr * dq1_dx(2*i,2*j,2*k)
			V(i,j,k) = V(i,j,k) + F_curr * dq1_dy(2*i,2*j,2*k)
			W(i,j,k) = W(i,j,k) + F_curr * dq1_dz(2*i,2*j,2*k)
		  else 
		    U(i,j,k) = U(i,j,k) - F_curr * dq1_dx(2*i,2*j,2*k)
			V(i,j,k) = V(i,j,k) - F_curr * dq1_dy(2*i,2*j,2*k)
			W(i,j,k) = W(i,j,k) - F_curr * dq1_dz(2*i,2*j,2*k)
		  endif
	   enddo
	   enddo
	   enddo

!DVM$ PARALLEL (k,j,i) on U(i,j,k)	 
       do k=k_source,k_source
	   do j=j_source-1,j_source+1, 2
	   do i=i_source,i_source
	      if (j .eq. j_source-1) then
            U(i,j,k) = U(i,j,k) + F_curr * dq2_dx(2*i,2*j,2*k)
			V(i,j,k) = V(i,j,k) + F_curr * dq2_dy(2*i,2*j,2*k)
			W(i,j,k) = W(i,j,k) + F_curr * dq2_dz(2*i,2*j,2*k)
		  else 
		    U(i,j,k) = U(i,j,k) - F_curr * dq2_dx(2*i,2*j,2*k)
			V(i,j,k) = V(i,j,k) - F_curr * dq2_dy(2*i,2*j,2*k)
			W(i,j,k) = W(i,j,k) - F_curr * dq2_dz(2*i,2*j,2*k)
		  endif
	   enddo
	   enddo
	   enddo

!DVM$ PARALLEL (k,j,i) on U(i,j,k)	 
       do k=k_source-1,k_source+1,2
	   do j=j_source,j_source
	   do i=i_source,i_source
	      if (k .eq. k_source-1) then
            U(i,j,k) = U(i,j,k) + F_curr * dq3_dx(2*i,2*j,2*k)
			V(i,j,k) = V(i,j,k) + F_curr * dq3_dy(2*i,2*j,2*k)
			W(i,j,k) = W(i,j,k) + F_curr * dq3_dz(2*i,2*j,2*k)
		  else 
		    U(i,j,k) = U(i,j,k) - F_curr * dq3_dx(2*i,2*j,2*k)
			V(i,j,k) = V(i,j,k) - F_curr * dq3_dy(2*i,2*j,2*k)
			W(i,j,k) = W(i,j,k) - F_curr * dq3_dz(2*i,2*j,2*k)
		  endif
	   enddo
	   enddo
	   enddo
!DVM$ END REGION

      call edges(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)

      call ainside(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,     
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)

      print *,'i_time= ',i_time
   
!DVM$ REGION   

!DVM$ PARALLEL (k,j,i) on UU(i,j,k)	 
       do k=k_source,k_source
	   do j=j_source,j_source
	   do i=i_source-1,i_source+1, 2
	      if (i .eq. i_source-1) then
            UU(i,j,k) = UU(i,j,k) + F_next * dq1_dx(2*i,2*j,2*k)
			VV(i,j,k) = VV(i,j,k) + F_next * dq1_dy(2*i,2*j,2*k)
			WW(i,j,k) = WW(i,j,k) + F_next * dq1_dz(2*i,2*j,2*k)
		  else 
		    UU(i,j,k) = UU(i,j,k) - F_next * dq1_dx(2*i,2*j,2*k)
			VV(i,j,k) = VV(i,j,k) - F_next * dq1_dy(2*i,2*j,2*k)
			WW(i,j,k) = WW(i,j,k) - F_next * dq1_dz(2*i,2*j,2*k)
		  endif
	   enddo
	   enddo
	   enddo

!DVM$ PARALLEL (k,j,i) on U(i,j,k)	 
       do k=k_source,k_source
	   do j=j_source-1,j_source+1, 2
	   do i=i_source,i_source
	      if (j .eq. j_source-1) then
            UU(i,j,k) = UU(i,j,k) + F_next * dq2_dx(2*i,2*j,2*k)
			VV(i,j,k) = VV(i,j,k) + F_next * dq2_dy(2*i,2*j,2*k)
			WW(i,j,k) = WW(i,j,k) + F_next * dq2_dz(2*i,2*j,2*k)
		  else 
		    UU(i,j,k) = UU(i,j,k) - F_next * dq2_dx(2*i,2*j,2*k)
			VV(i,j,k) = VV(i,j,k) - F_next * dq2_dy(2*i,2*j,2*k)
			WW(i,j,k) = WW(i,j,k) - F_next * dq2_dz(2*i,2*j,2*k)
		  endif
	   enddo
	   enddo
	   enddo

!DVM$ PARALLEL (k,j,i) on U(i,j,k)	 
       do k=k_source-1,k_source+1,2
	   do j=j_source,j_source
	   do i=i_source,i_source
	      if (k .eq. k_source-1) then
            UU(i,j,k) = UU(i,j,k) + F_next * dq3_dx(2*i,2*j,2*k)
			VV(i,j,k) = VV(i,j,k) + F_next * dq3_dy(2*i,2*j,2*k)
			WW(i,j,k) = WW(i,j,k) + F_next * dq3_dz(2*i,2*j,2*k)
		  else 
		    UU(i,j,k) = UU(i,j,k) - F_next * dq3_dx(2*i,2*j,2*k)
			VV(i,j,k) = VV(i,j,k) - F_next * dq3_dy(2*i,2*j,2*k)
			WW(i,j,k) = WW(i,j,k) - F_next * dq3_dz(2*i,2*j,2*k)
		  endif
	   enddo
	   enddo
	   enddo
!DVM$ END REGION

       call edges(U,V,W,UU,VV,WW,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)
	 
      call ainside(U,V,W,UU,VV,WW,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)
 
      print *,'i_time= ',i_time+1
      end do !i_time   
      
!---------------------------------------------------------------

      do i_time = i_time,iTime,2

      call edges(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)


      call ainside(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)

      print *,'i_time 2 =',i_time

      call edges(U,V,W,UU,VV,WW,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)


      call ainside(U,V,W,UU,VV,WW,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)
      

!      print *,''
      print *,'i_time 2 =',i_time+1
!     print *,''


      end do !i_time   
!DVM$ end interval 
!------------------------ Sending and writing traces -----------------------

      finish = dvtime()
      print '("Time =",f8.1," seconds")',finish-start

! DVM$ GET_ACTUAL(U,V,W)
!      OPEN (3, FILE='ARR_U.DAT', FORM='FORMATTED', STATUS='UNKNOWN')
!      U1 = U
!      WRITE (3,*) U1
!      CLOSE (3)
	 
!      OPEN (3, FILE='ARR_V.DAT', FORM='FORMATTED', STATUS='UNKNOWN')
!      U1 = V
!      WRITE (3,*) U1
!      CLOSE (3)	 
 
!      OPEN (3, FILE='ARR_W.DAT', FORM='FORMATTED', STATUS='UNKNOWN')
!      U1 = W
!      WRITE (3,*) U1
!      CLOSE (3)	 
	  
!      deallocate(U1,U,UU,V,VV,W,WW,alambda,amu,Rho,
!     &dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz)
	 

!________________________________________________________________________
!============================================================================================================================
      
!============================================================================================================================      

      stop
      end 
    
      subroutine compute(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d,k_UP,k_DOWN,j_UP,j_DOWN,i_UP,i_DOWN)

!DVM$ INHERIT UU,VV,WW,U,V,W,alambda,amu,ttoRhoJ
!DVM$ INHERIT dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy
!DVM$ INHERIT dq1_dz,dq2_dz,dq3_dz
	   integer i,j,k,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & N_ud_x, N_ud_y,
     & k_UP,k_DOWN,j_UP,j_DOWN,i_UP,i_DOWN

      double precision rd,rdd,d 
      double precision dSxx_dx,dSxy_dy,dSxz_dz,
     &                 dSxy_dx,dSyy_dy,dSyz_dz,
     &                 dSxz_dx,dSyz_dy,dSzz_dz,
     & dc1Ux_dq1,dc1Ux_dq2,dc1Ux_dq3,dc1Vy_dq1,dc1Vy_dq2,dc1Vy_dq3,
     & dc1Wz_dq1,dc1Wz_dq2,dc1Wz_dq3,
     & dcUy_dq1,dcUy_dq2,dcUy_dq3,dcVx_dq1,dcVx_dq2,dcVx_dq3,
     & dcUz_dq1,dcUz_dq2,dcUz_dq3,dcWx_dq1,dcWx_dq2,dcWx_dq3,
     & dc2Ux_dq1,dc2Ux_dq2,dc2Ux_dq3,dc2Vy_dq1,dc2Vy_dq2,dc2Vy_dq3,
     & dc2Wz_dq1,dc2Wz_dq2,dc2Wz_dq3,
     & dcVz_dq1,dcVz_dq2,dcVz_dq3,dcWy_dq1,dcWy_dq2,dcWy_dq3,
     & dc3Ux_dq1,dc3Ux_dq2,dc3Ux_dq3,dc3Vy_dq1,dcVy_dq2,dcVy_q3,
     & dc3Wz_dq1,dc3Wz_dq2,dc3Wz_dq3

      double precision
     & UU(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & VV(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & WW(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  U(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  V(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  W(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
 
     & alambda
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & amu
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & ttoRhoJ
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),     

     & dq1_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3)
	 
!=================================== Inside medium =====================================
!DVM$ REGION

!DVM$ PARALLEL (k,j,i) on amu(i,j,k), SHADOW_RENEW(U(CORNER),
!DVM$& V(CORNER),W(CORNER)), PRIVATE(
!DVM$& dc1Ux_dq1,dc1Ux_dq2,dc1Ux_dq3,dc1Vy_dq1,dc1Vy_dq2,dc1Vy_dq3,
!DVM$& dc1Wz_dq1,dc1Wz_dq2,dc1Wz_dq3,dSxx_dx,dcUy_dq1,dcUy_dq2,
!DVM$& dcUy_dq3,dcVx_dq1,dcVx_dq2,dcVx_dq3,dSxy_dy,dcUz_dq1,dcUz_dq2,
!DVM$& dcUz_dq3,dcWx_dq1,dcWx_dq2,dcWx_dq3,dSxz_dz,dSxy_dx,dc2Ux_dq1,
!DVM$& dc2Ux_dq2,dc2Ux_dq3,dc2Vy_dq1,dc2Vy_dq2,dc2Vy_dq3,dc2Wz_dq1,
!DVM$& dc2Wz_dq2,dc2Wz_dq3,dSyy_dy,dcVz_dq1,dcVz_dq2,dcVz_dq3,
!DVM$& dcWy_dq1,dcWy_dq2,dcWy_dq3,dSyz_dz,dSxz_dx,dSyz_dy,dc3Ux_dq1,
!DVM$& dc3Ux_dq2,dc3Ux_dq3,dc3Vy_dq1,dc3Vy_dq3,dc3Wz_dq1,
!DVM$& dc3Wz_dq2,dc3Wz_dq3,dSzz_dz)
      do k = k_DOWN, k_UP
      do j = j_DOWN, j_UP
      do i = i_DOWN, i_UP

! dSxx_dx ------------------------------------------------------------
      dc1Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+                        !
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +             
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*               
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*               
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc1Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*               
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+                        
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*               
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc1Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*               
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+                        
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc1Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+                                     
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*                                  !
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -              
     &              (alambda(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc1Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*                                  
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+                                     
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*                                  
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc1Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*                                  
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*                                  !!!                                  
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+                                     
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc1Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+                                     
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*                                  
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*                                  !
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc1Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*                                  
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+                                     
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*                                  !!
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc1Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*									 
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*                                  
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSxx_dx=
     & dq1_dx(2*i,2*j,2*k)*(dc1Ux_dq1+dc1Vy_dq1+dc1Wz_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dc1Ux_dq2+dc1Vy_dq2+dc1Wz_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dc1Ux_dq3+dc1Vy_dq3+dc1Wz_dq3)
!-------------------------------------------------------------
            
! dSxy_dy ----------------------------------------------------
      dcUy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+                                      
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*                                      !!
     &  dq2_dy(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUy_dq2 =                                                         
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+                                         !
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcVx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+                                         !!
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*                                      !
     &  dq1_dx(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+                                         
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )
 
 
      dSxy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSxz_dz ----------------------------------------------------
      dcUz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*                                      !!!
     &  dq3_dz(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dcUz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dcUz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+                                         !
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dcWx_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+                                         !!!
     &       0.5d0*amu(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWx_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dx(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWx_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*                                      !
     &  dq1_dx(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSxz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------
!-------------------------------------------------------------

! dSxy_dx ----------------------------------------------------
      dSxy_dx=
     & dq1_dx(2*i,2*j,2*k)*(dcUy_dq1+dcVx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUy_dq2+dcVx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUy_dq3+dcVx_dq3)
!-------------------------------------------------------------

! dSyy_dy ----------------------------------------------------
      dc2Ux_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+                                     !!!
     &       0.5d0*alambda(i,j,k))*
     &        dq1_dx(2*i+1,2*j,2*k)*(U(i+1,j,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i-1,j,k))*
     &        dq1_dx(2*i-1,2*j,2*k)*(U(i,j,k)-U(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq2_dx(2*i+2,2*j,2*k)*(U(i+1,j+1,k)-U(i+1,j-1,k)) -
     &              (alambda(i-1,j,k))*
     &  dq2_dx(2*i-2,2*j,2*k)*(U(i-1,j+1,k)-U(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k))*
     &  dq3_dx(2*i+2,2*j,2*k)*(U(i+1,j,k+1)-U(i+1,j,k-1)) -
     &              (alambda(i-1,j,k))*
     &  dq3_dx(2*i-2,2*j,2*k)*(U(i-1,j,k+1)-U(i-1,j,k-1)) ) 


      dc2Ux_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k))*                                  !!
     &  dq1_dx(2*i,2*j+2,2*k)*(U(i+1,j+1,k)-U(i-1,j+1,k)) -
     &              (alambda(i,j-1,k))*
     &  dq1_dx(2*i,2*j-2,2*k)*(U(i+1,j-1,k)-U(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+
     &       0.5d0*alambda(i,j,k))*
     &        dq2_dx(2*i,2*j+1,2*k)*(U(i,j+1,k)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j-1,k))*
     &        dq2_dx(2*i,2*j-1,2*k)*(U(i,j,k)-U(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k))*
     &  dq3_dx(2*i,2*j+2,2*k)*(U(i,j+1,k+1)-U(i,j+1,k-1)) -
     &              (alambda(i,j-1,k))*
     &  dq3_dx(2*i,2*j-2,2*k)*(U(i,j-1,k+1)-U(i,j-1,k-1)) ) 


      dc2Ux_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq1_dx(2*i,2*j,2*k+2)*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq1_dx(2*i,2*j,2*k-2)*(U(i+1,j,k-1)-U(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1))*
     &  dq2_dx(2*i,2*j,2*k+2)*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &              (alambda(i,j,k-1))*
     &  dq2_dx(2*i,2*j,2*k-2)*(U(i,j+1,k-1)-U(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+
     &       0.5d0*alambda(i,j,k))*
     &        dq3_dx(2*i,2*j,2*k+1)*(U(i,j,k+1)-U(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+
     &       0.5d0*alambda(i,j,k-1))*
     &        dq3_dx(2*i,2*j,2*k-1)*(U(i,j,k)-U(i,j,k-1)) )


      dc2Vy_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dc2Vy_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+                        !!
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dc2Vy_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dy(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) ) 


      dc2Wz_dq1 = dc1Wz_dq1      
      dc2Wz_dq2 = dc1Wz_dq2
      dc2Wz_dq3 = dc1Wz_dq3

      dSyy_dy=
     & dq1_dy(2*i,2*j,2*k)*(dc2Ux_dq1+dc2Vy_dq1+dc2Wz_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dc2Ux_dq2+dc2Vy_dq2+dc2Wz_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dc2Ux_dq3+dc2Vy_dq3+dc2Wz_dq3) 
!------------------------------------------------------------- 

! dSyz_dz ----------------------------------------------------
      dcVz_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(V(i+1,j,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(V(i,j,k)-V(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(V(i+1,j+1,k)-V(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(V(i-1,j+1,k)-V(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(V(i+1,j,k+1)-V(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(V(i-1,j,k+1)-V(i-1,j,k-1)) ) 


      dcVz_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(V(i+1,j+1,k)-V(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(V(i+1,j-1,k)-V(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(V(i,j+1,k)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(V(i,j,k)-V(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*                                      !!!
     &  dq3_dz(2*i,2*j+2,2*k)*(V(i,j+1,k+1)-V(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(V(i,j-1,k+1)-V(i,j-1,k-1)) ) 


      dcVz_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(V(i+1,j,k-1)-V(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(V(i,j+1,k-1)-V(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+                                         !!
     &       0.5d0*amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(V(i,j,k+1)-V(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(V(i,j,k)-V(i,j,k-1)) )


      dcWy_dq1 = 
     & rdd*((0.5d0*amu(i+1,j,k)+
     &       0.5d0*amu(i,j,k))*
     &        dq1_dy(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i-1,j,k))*
     &        dq1_dy(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq2_dy(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (amu(i-1,j,k))*
     &  dq2_dy(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (amu(i+1,j,k))*
     &  dq3_dy(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (amu(i-1,j,k))*
     &  dq3_dy(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dcWy_dq2 =
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq1_dy(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (amu(i,j-1,k))*
     &  dq1_dy(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*amu(i,j+1,k)+                                         
     &       0.5d0*amu(i,j,k))*
     &        dq2_dy(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -              
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j-1,k))*
     &        dq2_dy(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (amu(i,j+1,k))*
     &  dq3_dy(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (amu(i,j-1,k))*
     &  dq3_dy(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dcWy_dq3 =
     & 0.25d0*rdd*( (amu(i,j,k+1))*
     &  dq1_dy(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq1_dy(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (amu(i,j,k+1))*                                      !!
     &  dq2_dy(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (amu(i,j,k-1))*
     &  dq2_dy(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*amu(i,j,k+1)+
     &       0.5d0*amu(i,j,k))*
     &        dq3_dy(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*amu(i,j,k)+
     &       0.5d0*amu(i,j,k-1))*
     &        dq3_dy(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )
 
   
      dSyz_dz =
     & dq1_dz(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!------------------------------------------------------------- 
!-------------------------------------------------------------

! dSxz_dx ----------------------------------------------------
      dSxz_dx = 
     & dq1_dx(2*i,2*j,2*k)*(dcUz_dq1+dcWx_dq1)+
     & dq2_dx(2*i,2*j,2*k)*(dcUz_dq2+dcWx_dq2)+
     & dq3_dx(2*i,2*j,2*k)*(dcUz_dq3+dcWx_dq3)
!-------------------------------------------------------------

! dSyz_dy ----------------------------------------------------
      dSyz_dy =
     & dq1_dy(2*i,2*j,2*k)*(dcVz_dq1+dcWy_dq1)+
     & dq2_dy(2*i,2*j,2*k)*(dcVz_dq2+dcWy_dq2)+
     & dq3_dy(2*i,2*j,2*k)*(dcVz_dq3+dcWy_dq3)
!-------------------------------------------------------------

! dSzz_dz ----------------------------------------------------
      dc3Ux_dq1 = dc2Ux_dq1
      dc3Ux_dq2 = dc2Ux_dq2
      dc3Ux_dq3 = dc2Ux_dq3

      dc3Vy_dq1 = dc1Vy_dq1
      dc3Vy_dq1 = dc1Vy_dq2
      dc3Vy_dq3 = dc1Vy_dq3 


      dc3Wz_dq1 = 
     & rdd*((0.5d0*alambda(i+1,j,k)+amu(i+1,j,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq1_dz(2*i+1,2*j,2*k)*(W(i+1,j,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i-1,j,k)+amu(i-1,j,k))*
     &        dq1_dz(2*i-1,2*j,2*k)*(W(i,j,k)-W(i-1,j,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq2_dz(2*i+2,2*j,2*k)*(W(i+1,j+1,k)-W(i+1,j-1,k)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq2_dz(2*i-2,2*j,2*k)*(W(i-1,j+1,k)-W(i-1,j-1,k)) ) +

     & 0.25d0*rdd*( (alambda(i+1,j,k)+2.0d0*amu(i+1,j,k))*
     &  dq3_dz(2*i+2,2*j,2*k)*(W(i+1,j,k+1)-W(i+1,j,k-1)) -
     &              (alambda(i-1,j,k)+2.0d0*amu(i-1,j,k))*
     &  dq3_dz(2*i-2,2*j,2*k)*(W(i-1,j,k+1)-W(i-1,j,k-1)) ) 


      dc3Wz_dq2 =
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq1_dz(2*i,2*j+2,2*k)*(W(i+1,j+1,k)-W(i-1,j+1,k)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq1_dz(2*i,2*j-2,2*k)*(W(i+1,j-1,k)-W(i-1,j-1,k)) ) +

     & rdd*((0.5d0*alambda(i,j+1,k)+amu(i,j+1,k)+
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq2_dz(2*i,2*j+1,2*k)*(W(i,j+1,k)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j-1,k)+amu(i,j-1,k))*
     &        dq2_dz(2*i,2*j-1,2*k)*(W(i,j,k)-W(i,j-1,k)) ) +
     
     & 0.25d0*rdd*( (alambda(i,j+1,k)+2.0d0*amu(i,j+1,k))*
     &  dq3_dz(2*i,2*j+2,2*k)*(W(i,j+1,k+1)-W(i,j+1,k-1)) -
     &              (alambda(i,j-1,k)+2.0d0*amu(i,j-1,k))*
     &  dq3_dz(2*i,2*j-2,2*k)*(W(i,j-1,k+1)-W(i,j-1,k-1)) ) 


      dc3Wz_dq3 =
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq1_dz(2*i,2*j,2*k+2)*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq1_dz(2*i,2*j,2*k-2)*(W(i+1,j,k-1)-W(i-1,j,k-1)) ) +
  
     & 0.25d0*rdd*( (alambda(i,j,k+1)+2.0d0*amu(i,j,k+1))*
     &  dq2_dz(2*i,2*j,2*k+2)*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &              (alambda(i,j,k-1)+2.0d0*amu(i,j,k-1))*
     &  dq2_dz(2*i,2*j,2*k-2)*(W(i,j+1,k-1)-W(i,j-1,k-1)) ) +

     & rdd*((0.5d0*alambda(i,j,k+1)+amu(i,j,k+1)+                        !!!
     &       0.5d0*alambda(i,j,k)+amu(i,j,k))*
     &        dq3_dz(2*i,2*j,2*k+1)*(W(i,j,k+1)-W(i,j,k)) -
     &      (0.5d0*alambda(i,j,k)+amu(i,j,k)+
     &       0.5d0*alambda(i,j,k-1)+amu(i,j,k-1))*
     &        dq3_dz(2*i,2*j,2*k-1)*(W(i,j,k)-W(i,j,k-1)) )


      dSzz_dz=
     & dq1_dz(2*i,2*j,2*k)*(dc3Ux_dq1+dc3Vy_dq1+dc3Wz_dq1)+
     & dq2_dz(2*i,2*j,2*k)*(dc3Ux_dq2+dc3Vy_dq2+dc3Wz_dq2)+
     & dq3_dz(2*i,2*j,2*k)*(dc3Ux_dq3+dc3Vy_dq3+dc3Wz_dq3) 
!-------------------------------------------------------------

!-------------------------------------------------------------
   
      UU(i,j,k)=2.0d0*U(i,j,k)+ttoRhoJ(i,j,k)*(dSxx_dx+dSxy_dy+dSxz_dz)-
     &               UU(i,j,k)
      VV(i,j,k)=2.0d0*V(i,j,k)+ttoRhoJ(i,j,k)*(dSxy_dx+dSyy_dy+dSyz_dz)-
     &               VV(i,j,k)
      WW(i,j,k)=2.0d0*W(i,j,k)+ttoRhoJ(i,j,k)*(dSxz_dx+dSyz_dy+dSzz_dz)-
     &               WW(i,j,k)

!-------------------------------------------------------------
      end do
      end do
      end do
	  
!DVM$ END REGION
!=====================================================================================================================      
         
      return
      end subroutine 
	  
	 
      !Essential
        subroutine ainside(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)
!DVM$ INHERIT UU,VV,WW,U,V,W,alambda,amu,ttoRhoJ
!DVM$ INHERIT dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy
!DVM$ INHERIT dq3_dy,dq1_dz,dq2_dz,dq3_dz
      integer i,j,k,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & N_ud_x, N_ud_y

      double precision rd,rdd,d 
      double precision dSxx_dx,dSxy_dy,dSxz_dz,
     &                 dSxy_dx,dSyy_dy,dSyz_dz,
     &                 dSxz_dx,dSyz_dy,dSzz_dz,
     & dc1Ux_dq1,dc1Ux_dq2,dc1Ux_dq3,dc1Vy_dq1,dc1Vy_dq2,dc1Vy_dq3,
     & dc1Wz_dq1,dc1Wz_dq2,dc1Wz_dq3,
     & dcUy_dq1,dcUy_dq2,dcUy_dq3,dcVx_dq1,dcVx_dq2,dcVx_dq3,
     & dcUz_dq1,dcUz_dq2,dcUz_dq3,dcWx_dq1,dcWx_dq2,dcWx_dq3,
     & dc2Ux_dq1,dc2Ux_dq2,dc2Ux_dq3,dc2Vy_dq1,dc2Vy_dq2,dc2Vy_dq3,
     & dc2Wz_dq1,dc2Wz_dq2,dc2Wz_dq3,
     & dcVz_dq1,dcVz_dq2,dcVz_dq3,dcWy_dq1,dcWy_dq2,dcWy_dq3,
     & dc3Ux_dq1,dc3Ux_dq2,dc3Ux_dq3,dc3Vy_dq1,dcVy_dq2,dcVy_q3,
     & dc3Wz_dq1,dc3Wz_dq2,dc3Wz_dq3

      double precision
     & UU(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & VV(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & WW(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  U(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  V(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  W(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     
 
     & alambda
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & amu
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & ttoRhoJ
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),     

     & dq1_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3)

	 call compute(UU,VV,WW,U,V,W, alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d,
     & N_up_z,N_down_z,N_up_y,N_down_y,N_up_x,N_down_x)

!=====================================================================================================================               
      return
      end subroutine 


      subroutine edges(UU,VV,WW,U,V,W,
     & alambda,amu,ttoRhoJ,
     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
     & A1,A2,A3,B1,B2,B3,C1,C2,C3,
     & R1_1,R1_2,R1_3,R1_4,R1_5,R1_6,
     & R2_1,R2_2,R2_3,R2_4,R2_5,R2_6,
     & R3_1,R3_2,R3_3,R3_4,R3_5,R3_6,
     & dz_dq1_surf,dz_dq2_surf,
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & rd,rdd,d)

!DVM$ INHERIT UU,VV,WW,U,V,W,alambda,amu,ttoRhoJ
!DVM$ INHERIT dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy
!DVM$ INHERIT dq3_dy,dq1_dz,dq2_dz,dq3_dz
!DVM$ INHERIT A1,A2,A3,B1,B2,B3,C1,C2,C3
!DVM$ INHERIT R1_1,R1_2,R1_3,R1_4,R1_5,R1_6
!DVM$ INHERIT R2_1,R2_2,R2_3,R2_4,R2_5,R2_6
!DVM$ INHERIT R3_1,R3_2,R3_3,R3_4,R3_5,R3_6
!DVM$ INHERIT dz_dq1_surf,dz_dq2_surf

      integer i,j,k, 
     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
     & N_ud_x, N_ud_y
 
      integer N_bot_x,N_top_x,N_bot_y,N_top_y,N_bot_z,N_top_z

      double precision rd,rdd,d 
      double precision dSxx_dx,dSxy_dy,dSxz_dz,
     &                 dSxy_dx,dSyy_dy,dSyz_dz,
     &                 dSxz_dx,dSyz_dy,dSzz_dz,
     & dc1Ux_dq1,dc1Ux_dq2,dc1Ux_dq3,dc1Vy_dq1,dc1Vy_dq2,dc1Vy_dq3,
     & dc1Wz_dq1,dc1Wz_dq2,dc1Wz_dq3,
     & dcUy_dq1,dcUy_dq2,dcUy_dq3,dcVx_dq1,dcVx_dq2,dcVx_dq3,
     & dcUz_dq1,dcUz_dq2,dcUz_dq3,dcWx_dq1,dcWx_dq2,dcWx_dq3,
     & dc2Ux_dq1,dc2Ux_dq2,dc2Ux_dq3,dc2Vy_dq1,dc2Vy_dq2,dc2Vy_dq3,
     & dc2Wz_dq1,dc2Wz_dq2,dc2Wz_dq3,
     & dcVz_dq1,dcVz_dq2,dcVz_dq3,dcWy_dq1,dcWy_dq2,dcWy_dq3,
     & dc3Ux_dq1,dc3Ux_dq2,dc3Ux_dq3,dc3Vy_dq1,dcVy_dq2,dcVy_q3,
     & dc3Wz_dq1,dc3Wz_dq2,dc3Wz_dq3

      double precision
     & UU(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & VV(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & WW(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  U(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  V(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     &  W(N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     
 
     & alambda
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & amu
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),
     & ttoRhoJ
     &   (N_down_x-1:N_up_x+1,N_down_y-1:N_up_y+1,N_down_z-1:N_up_z+1),     

     & dq1_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dx(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dy(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq1_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq2_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),
     & dq3_dz(2*N_down_x-3:2*N_up_x+3,2*N_down_y-3:2*N_up_y+3,
     & 2*N_down_z-3:2*N_up_z+3),

     & A1(N_down_x:N_up_x,N_down_y:N_up_y),
     & A2(N_down_x:N_up_x,N_down_y:N_up_y),
     & A3(N_down_x:N_up_x,N_down_y:N_up_y),
     & B1(N_down_x:N_up_x,N_down_y:N_up_y),
     & B2(N_down_x:N_up_x,N_down_y:N_up_y),
     & B3(N_down_x:N_up_x,N_down_y:N_up_y),
     & C1(N_down_x:N_up_x,N_down_y:N_up_y),
     & C2(N_down_x:N_up_x,N_down_y:N_up_y),
     & C3(N_down_x:N_up_x,N_down_y:N_up_y),
     
     & R1,R2,R3,
     & R1_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R1_6(N_down_x:N_up_x,N_down_y:N_up_y),
	 
     & R2_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R2_6(N_down_x:N_up_x,N_down_y:N_up_y),
	 
     & R3_1(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_2(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_3(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_4(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_5(N_down_x:N_up_x,N_down_y:N_up_y),
     & R3_6(N_down_x:N_up_x,N_down_y:N_up_y),
     
     &dz_dq1_surf(N_down_x:N_up_x,N_down_y:N_up_y),
     &dz_dq2_surf(N_down_x:N_up_x,N_down_y:N_up_y), 

     & Znam1,Znam2,Znam3,Znam4,
     & dU_dq1,dU_dq2,dV_dq1,dV_dq2,dW_dq1,dW_dq2,
     & dU_dq3,dV_dq3,dW_dq3

!=================================== Free surface =====================================
  !    if(id_z==0) then
      
!DVM$ REGION
!DVM$ PARALLEL (j,k,i) on U(i,j,k), SHADOW_RENEW(U(CORNER),
!DVM$& V(CORNER),W(CORNER)), PRIVATE(
!DVM$& Znam1,Znam2,Znam3,Znam4,dU_dq1,dU_dq2,dV_dq1,dV_dq2,dW_dq1,
!DVM$& dW_dq2, R1,R2,R3,dW_dq3,dV_dq3,dU_dq3)
      do j = N_down_y, N_up_y
       do k=0,0
        do i = N_down_x, N_up_x
           Znam1=(B3(i,j)*A1(i,j)-A3(i,j)*B1(i,j))*
     &           (C2(i,j)*A1(i,j)-A2(i,j)*C1(i,j)) -
     &           (C3(i,j)*A1(i,j)-A3(i,j)*C1(i,j))*
     &           (B2(i,j)*A1(i,j)-A2(i,j)*B1(i,j))

           Znam2 = B2(i,j)*A1(i,j)-A2(i,j)*B1(i,j)
           Znam3 = B2(i,j)*C3(i,j)-C2(i,j)*B3(i,j)
           Znam4 = A1(i,j)*C3(i,j)-C1(i,j)*A3(i,j)


           dU_dq1= 0.5d0*rd*(1.5d0*(U(i+1,j,k+1)-U(i-1,j,k+1)) -
     &             0.5d0*(U(i+1,j,k+2)-U(i-1,j,k+2)))
           dU_dq2= 0.5d0*rd*(1.5d0*(U(i,j+1,k+1)-U(i,j-1,k+1)) -
     &             0.5d0*(U(i,j+1,k+2)-U(i,j-1,k+2)))

           dV_dq1= 0.5d0*rd*(1.5d0*(V(i+1,j,k+1)-V(i-1,j,k+1)) -
     &             0.5d0*(V(i+1,j,k+2)-V(i-1,j,k+2)))
           dV_dq2= 0.5d0*rd*(1.5d0*(V(i,j+1,k+1)-V(i,j-1,k+1)) -
     &             0.5d0*(V(i,j+1,k+2)-V(i,j-1,k+2)))

           dW_dq1= 0.5d0*rd*(1.5d0*(W(i+1,j,k+1)-W(i-1,j,k+1)) -
     &             0.5d0*(W(i+1,j,k+2)-W(i-1,j,k+2)))
           dW_dq2= 0.5d0*rd*(1.5d0*(W(i,j+1,k+1)-W(i,j-1,k+1)) -
     &             0.5d0*(W(i,j+1,k+2)-W(i,j-1,k+2)))
      
           R1 = R1_1(i,j)*dU_dq1+R1_2(i,j)*dU_dq2+ 
     &          R1_3(i,j)*dV_dq1+R1_4(i,j)*dV_dq2+
     &          R1_5(i,j)*dW_dq1+R1_6(i,j)*dW_dq2

           R2 = R2_1(i,j)*dU_dq1+R2_2(i,j)*dU_dq2+
     &          R2_3(i,j)*dV_dq1+R2_4(i,j)*dV_dq2+
     &          R2_5(i,j)*dW_dq1+R2_6(i,j)*dW_dq2

           R3 = R3_1(i,j)*dU_dq1+R3_2(i,j)*dU_dq2+
     &          R3_3(i,j)*dV_dq1+R3_4(i,j)*dV_dq2+
     &          R3_5(i,j)*dW_dq1+R3_6(i,j)*dW_dq2


           if(dabs(dz_dq1_surf(i,j))>=0.000000001d0 .and. 
     &        dabs(dz_dq2_surf(i,j))>=0.000000001d0) then
              dW_dq3 = -(1.0d0/Znam1)*((R2*A1(i,j)-R1*B1(i,j))*
     &                 (C2(i,j)*A1(i,j)-A2(i,j)*C1(i,j)) -
     &                 (R3*A1(i,j)-R1*C1(i,j))*
     &                 (B2(i,j)*A1(i,j)-A2(i,j)*B1(i,j)))
     
              dV_dq3 = (1.0d0/Znam2)*
     &                 (R1*B1(i,j)-R2*A1(i,j)-
     &                 (B3(i,j)*A1(i,j)-A3(i,j)*B1(i,j))*dW_dq3)

              dU_dq3 = -(1.0d0/A1(i,j))*(R1+A2(i,j)*dV_dq3+
     &                 A3(i,j)*dW_dq3)      
           end if

      
           if(dabs(dz_dq1_surf(i,j))<0.000000001d0 .and. 
     &        dabs(dz_dq2_surf(i,j))>=0.000000001d0) then
              dW_dq3 = -(1.0d0/(-Znam3))*(R2*C2(i,j)-R3*B2(i,j))     
              dV_dq3 = -(1.0d0/Znam3)*(R2*C3(i,j)-R3*B3(i,j))
              dU_dq3 = -R1/A1(i,j)
           end if


           if(dabs(dz_dq1_surf(i,j))>=0.000000001d0 .and. 
     &        dabs(dz_dq2_surf(i,j))<0.000000001d0) then
              dW_dq3 = -(1.0d0/(-Znam4))*(R1*C1(i,j)-R3*A1(i,j))     
              dV_dq3 = -R2/B2(i,j)
              dU_dq3 = -(1.0d0/Znam4)*(R1*C3(i,j)-R3*A3(i,j))
           end if

      
           if(dabs(dz_dq1_surf(i,j))<0.000000001d0 .and. 
     &        dabs(dz_dq2_surf(i,j))<0.000000001d0) then
              dW_dq3 = -R3/C3(i,j)     
              dV_dq3 = -R2/B2(i,j)
              dU_dq3 = -R1/A1(i,j)
           end if

           U(i,j,k)=U(i,j,k+1)-d*dU_dq3
           V(i,j,k)=V(i,j,k+1)-d*dV_dq3
           W(i,j,k)=W(i,j,k+1)-d*dW_dq3
         end do
      end do
      end do	  
	  
!DVM$ END REGION 
!=================================== Edges =====================================

!    fixed k = N_down_z
!       call compute(UU,VV,WW,U,V,W, alambda,amu,ttoRhoJ,
!     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
!     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
!     & rd,rdd,d,
!     & N_down_z,N_down_z,N_up_y,N_down_y,N_up_x,N_down_x)

!    fixed k = N_up_z
!       call compute(UU,VV,WW,U,V,W, alambda,amu,ttoRhoJ,
!     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
!     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
!     & rd,rdd,d,
!     & N_up_z,N_up_z,N_up_y,N_down_y,N_up_x,N_down_x)

!    fixed j = N_down_y
!       call compute(UU,VV,WW,U,V,W, alambda,amu,ttoRhoJ,
!     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
!     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
!     & rd,rdd,d,
!     & N_up_z-1,N_down_z+1,N_down_y,N_down_y,N_up_x,N_down_x)

!    fixed j = N_up_y
!       call compute(UU,VV,WW,U,V,W, alambda,amu,ttoRhoJ,
!     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
!     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
!     & rd,rdd,d,
!     & N_up_z-1,N_down_z+1,N_up_y,N_up_y,N_up_x,N_down_x)	 

!    fixed i = N_down_x
!       call compute(UU,VV,WW,U,V,W, alambda,amu,ttoRhoJ,
!     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
!     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
!     & rd,rdd,d,
!     & N_up_z-1,N_down_z+1,N_up_y-1,N_down_y+1,N_down_x,N_down_x)	 

!    fixed i = N_up_x
!       call compute(UU,VV,WW,U,V,W, alambda,amu,ttoRhoJ,
!     & dq1_dx,dq2_dx,dq3_dx,dq1_dy,dq2_dy,dq3_dy,dq1_dz,dq2_dz,dq3_dz,
!     & N_down_x,N_up_x,N_down_y,N_up_y,N_down_z,N_up_z,
!     & rd,rdd,d,
!     & N_up_z-1,N_down_z+1,N_up_y-1,N_down_y+1,N_up_x,N_up_x)	 
!=====================================================================================================================               
      return
      end subroutine    
